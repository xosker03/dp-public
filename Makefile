##### NASTAVENÍ #####
.PHONY: all debug run clean fullclean prepare copytogpu zip doxygen format format2

NUM_CORES = $(shell nproc)
# MAKEFLAGS := --jobs=${NUM_CORES}
#MAKEFLAGS += --output-sync=target

CC = gcc
CXX = g++

NVCC = nvcc


##### BINÁRKA #####
EXECUTABLE_NAME = prog
STATIC_LIB_NAME = liblibs.a

##### ZDROJÁKY Hlavního programu #####
#$(wildcard */*.h) #$(wildcard */*.cpp)
SOURCEDIR = Sources
LIBDIR = Libs
DIRS = zaklad GUI $(shell ( cd ${SOURCEDIR}; find SimImplementation -type d))

HEADERS = \
	$(wildcard ${SOURCEDIR}/*.h) \
	$(foreach dir, ${DIRS}, $(wildcard ${SOURCEDIR}/${dir}/*.h))
SOURCES = \
	$(wildcard ${SOURCEDIR}/*.cpp) \
	$(foreach dir, ${DIRS}, $(wildcard ${SOURCEDIR}/${dir}/*.cpp))
SOURCES_CUDA = \
	$(wildcard ${SOURCEDIR}/*.cu) \
	$(foreach dir, ${DIRS}, $(wildcard ${SOURCEDIR}/${dir}/*.cu))



##### ZDROJÁKY Knihoven, které je jinak možné přímo kompilovat s hlavním programem #####
LIB_SOURCES = \
	$(wildcard ${LIBDIR}/imgui/*.cpp) \
	$(wildcard ${LIBDIR}/imgui/misc/cpp/imgui_stdlib/*.cpp) \
	${LIBDIR}/imgui/backends/imgui_impl_glfw.cpp \
	${LIBDIR}/imgui/backends/imgui_impl_opengl3.cpp \
	${LIBDIR}/implot/implot.cpp \
	${LIBDIR}/implot/implot_items.cpp

LIB_SOURCES_C = \
	${LIBDIR}/glad/src/glad.c


##### Objekty a knihovny #####
OBJECTS = $(SOURCES_CUDA:.cu=.o) $(SOURCES:.cpp=.o) $(SOURCES_C:.c=.o)
LIB_OBJECTS = $(LIB_SOURCES:.cpp=.o) $(LIB_SOURCES_C:.c=.o)
LIBS = ${LIBDIR}/glfw/build/src/libglfw3.a ${LIBDIR}/glm/build/glm/libglm.a ${STATIC_LIB_NAME}


##### FLAGY #####
INCLUDES = -I${LIBDIR}/imgui -I${LIBDIR}/glad/include -I${LIBDIR}/glfw/include -I${LIBDIR}/glm -I${LIBDIR}/json/include

CFLAGS = -O3 ${INCLUDES}
CPPFLAGS = -std=c++2a -fpermissive -rdynamic
GCCFLAGS = -march=native -ftree-vectorize -pipe -fopenmp -foffload=-misa=sm_53 #-foffload=nvptx-none -foffload="-O3" #-fdiagnostics-color=always #-fopt-info-vec

CUDA_FLAGS = --device-c --restrict -Xcompiler #-allow-unsupported-compiler


##### LINKOVÁNÍ #####
LDFLAGS = -lpthread -lm -lGL -lhdf5 -lhdf5_cpp -L ${LIBDIR}/glfw/build/src -l glfw3 -L ${LIBDIR}/glm/build/glm -l glm -L. -llibs


##### KOMPILACE #####
COMMAND = -o $(EXECUTABLE_NAME) $(OBJECTS)









##### CÍLE #####
all: $(OBJECTS) $(LIBS)
# 	$(CXX) $(CFLAGS) $(CPPFLAGS) $(GCCFLAGS) $(COMMAND) $(LDFLAGS)
	$(NVCC) $(CFLAGS) -std=c++17 -Xcompiler -fopenmp $(COMMAND) $(LDFLAGS)

debug: CFLAGS = -g ${INCLUDES}
debug: $(OBJECTS) $(LIBS)
# 	$(CXX) $(CFLAGS) $(CPPFLAGS) $(GCCFLAGS) $(COMMAND) $(LDFLAGS)
	$(NVCC) $(CFLAGS) -std=c++17 -Xcompiler -fopenmp $(COMMAND) $(LDFLAGS)

gcconly: $(SOURCES:.cpp=.o) $(SOURCES_C:.c=.o) $(LIBS)
	$(CXX) $(CFLAGS) $(CPPFLAGS) $(GCCFLAGS) -o $(EXECUTABLE_NAME) $(SOURCES:.cpp=.o) $(SOURCES_C:.c=.o) $(LDFLAGS)

run:
	./$(EXECUTABLE_NAME)

clean:
	rm -f $(EXECUTABLE_NAME)
	rm -f $(OBJECTS)
	rm -rf ./Doxygen

fullclean: clean
	rm -f $(LIBS)
	rm -f $(LIB_OBJECTS)
	rm -rf ./Libs/glfw/build
	rm -rf ./Libs/glm/build

zip:
	zip -r xosker03.zip Makefile *.c *.cpp *.h *.hpp

doxygen:
	doxygen ./Doxyfile

format:
	astyle -v -A1 --indent=spaces=4 --indent-classes --indent-preproc-block --indent-preproc-cond --indent-preproc-define --pad-oper --pad-comma --pad-header --unpad-paren --break-closing-braces --break-one-line-headers --add-braces --convert-tabs -r -n "Sources/*.cpp" "Sources/*.h" "Sources/*.glsl" "Sources/*.cu"

format2:
	astyle -v -A1 --indent=spaces=2 --indent-classes --indent-preproc-block --indent-preproc-cond --indent-preproc-define --pad-oper --pad-comma --pad-header --unpad-paren --break-closing-braces --break-one-line-headers --add-braces --convert-tabs -r -n "Sources/*.cpp" "Sources/*.h" "Sources/*.glsl" "Sources/*.cu"




##### Objektové soubory a knihovny #####

$(OBJECTS): $(HEADERS)


${STATIC_LIB_NAME}: $(LIB_OBJECTS)
	ar rcs $@ $^

${LIBDIR}/glfw/build/src/libglfw3.a:
	(cd ${LIBDIR}/glfw; mkdir -p build; cd build; cmake .. -D GLFW_BUILD_WAYLAND=0 -D GLFW_BUILD_DOCS=0 -D GLFW_BUILD_EXAMPLES=0 -D GLFW_BUILD_TESTS=0; make)

${LIBDIR}/glm/build/glm/libglm.a:
	(cd ${LIBDIR}/glm; mkdir -p build; cd build; cmake ..; make)


${SOURCEDIR}/SimImplementation/GSharedTC-Opt.o : ${SOURCEDIR}/SimImplementation/GSharedTC-Opt.cu
	$(NVCC) $(CFLAGS) $(CUDA_FLAGS) -fopenmp $(CUDA_ARCH) -prec-div=false -prec-sqrt=false -o $@ -c $<

${SOURCEDIR}/SimImplementation/GSharedTC-Fast-Opt.o : ${SOURCEDIR}/SimImplementation/GSharedTC-Fast-Opt.cu
	$(NVCC) $(CFLAGS) $(CUDA_FLAGS) -fopenmp $(CUDA_ARCH) -prec-div=false -prec-sqrt=false -o $@ -c $<


%.o: %.cpp
	$(CXX) $(CFLAGS) $(CPPFLAGS) $(GCCFLAGS) -o $@ -c $<

%.o: %.c
	$(CC) $(CFLAGS) $(GCCFLAGS) -o $@ -c $<

%.o: %.cu
	$(NVCC) $(CFLAGS) $(CUDA_FLAGS) -fopenmp $(CUDA_ARCH) -o $@ -c $<



#-Wno-writable-strings
