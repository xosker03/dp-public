/**
 * @file      SimUser4.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Soubor implementující třídu Simulation.
 *
 * @version   BS4GC 0.1
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include "SimUser4.h"


//FIXME: Volitelné: Pokud nechcete používat jmenný prostor, tak musíte přejmenovat SimClass na něco unikátního. Jednoduše třeba takto #define SimClass MyAwesomeSimulation
// namespace { // NAMESPACE_BEGIN

//FIXME: Volitelné: Pojmenuj tuto implementaci v GUI: __FILE_NAME__ nahraď za jakýkoliv řetězec, třeba "MojeTřída"
#define SimClassStr "SimUser4"


/**
 * @brief Samostnaný běh simulace.
 */
void SimUser4::userRun()
{
   /// Singalizuje začátek simulace.
    userSignalStart();

    // Zde je možné inicializovat simulaci. Alolovat paměť atd.


    while (mTime < mInitSett.mEndTime)
    {
        /// Singalizuje začátek kroku simulace.
        userSignalStepBegin(); 
        
        // Zde je také možné provést nějaké operace před každým krokem simulace.

        userStep();

        // Zde je také možné provést nějaké operace po každém kroku simulace.

        /// Singalizuje konec kroku simulace.
        userSignalStepEnd();
        /// Kontrola zda-li GUI nežádalo o zastavení simulace.
        if(userStopRequested())
        {
            break;
        }
    }
    /// Singalizuje konec simulace.
    userSignalFinish();
}


/**
 * @brief Provede jeden krok simulace.
 * @return True pokud je možné udělat další krok, tzn. aktuální čas simulace je menší než konečný.
 */
bool SimUser4::userStep()
{
    // Zde je místo pro provedení jednoho kroku simulace.

    mTime += mLiveSett.mDT;
    return mTime < mInitSett.mEndTime;
}








//FIXME: Pokud se změnil název třídy ze SimClass jinak něž pomocí #define, tak je třeba upravit toto "volání" makra.
//FIXME: Volitelné: Pokud chcete skrýt tuto implementaci v GUI, tak zakomentujte REGISTER_USER_SIMULATIUON().
REGISTER_USER_SIMULATIUON(SimUser4, SimClassStr, "");
// } //NAMESPACE_END

