/**
 * @file      SimUser3.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Soubor implementující třídu Simulation.
 *
 * @version   BS4GC 0.1
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */


#include "../Simulation.h"
#include "../Registrator.h"


//FIXME: Volitelné: Pokud nechcete používat jmenný prostor, tak musíte přejmenovat SimClass na něco unikátního. Jednoduše třeba takto #define SimClass MyAwesomeSimulation
// namespace { // NAMESPACE_BEGIN

//FIXME: Volitelné: Pojmenuj tuto implementaci v GUI: __FILE_NAME__ nahraď za jakýkoliv řetězec, třeba "MojeTřída"
#define SimClassStr "SimUser3"



/**
 * @class SimClass
 * @brief Třída reprezentující simulaci.
 *
 * Tato třída simuluje měkou kostku tvořenou částicemi pomocí gradientního sestupu.
 */
class SimUser3 : public Simulation
{
    public:
        /// Konstruktor.
        SimUser3() : Simulation() {};
        /// Destruktor.
        ~SimUser3() {};

        /// Kopírovací konstruktor. Není povolen.
        SimUser3(const SimUser3& other) = delete;
        /// Přesouvací konstruktor. Není povolen.
        SimUser3(SimUser3&& other) noexcept = delete;

        /// Opeátor přiřazení. Není povolen.
        SimUser3& operator=(const SimUser3& other) = delete;
        /// Operátor přiřazení pomocí přesunutí. Není povolen.
        SimUser3& operator=(SimUser3&& other) noexcept = delete;

        //spuštění simulace
        virtual void userRun() override;
        //krok simulace
        virtual bool userStep() override;

    private:
        //funkce pro ošetření hranic simulačního prostoru
        void handleBorders(Particle & a);
};

////////////////////////////////////////////////////////////////////////////////////////

/**
 * @brief Samostnaný běh simulace.
 */
void SimUser3::userRun()
{
    /// Singalizuje začátek simulace.
    userSignalStart();

    // Zde je možné inicializovat simulaci. Alolovat paměť atd.


    while (mTime < mInitSett.mEndTime)
    {
        /// Singalizuje začátek kroku simulace.
        userSignalStepBegin(); 
        
        // Zde je také možné provést nějaké operace před každým krokem simulace.

        userStep();

        // Zde je také možné provést nějaké operace po každém kroku simulace.

        /// Singalizuje konec kroku simulace.
        userSignalStepEnd();
        /// Kontrola zda-li GUI nežádalo o zastavení simulace.
        if(userStopRequested())
        {
            break;
        }
    }
    /// Singalizuje konec simulace.
    userSignalFinish();
}


/**
 * @brief Provede jeden krok simulace.
 * @return True pokud je možné udělat další krok, tzn. aktuální čas simulace je menší než konečný.
 */
bool SimUser3::userStep()
{
    // Zde je místo pro provedení jednoho kroku simulace.

    mTime += mLiveSett.mDT;
    return mTime < mInitSett.mEndTime;
}








//FIXME: Pokud se změnil název třídy ze SimClass jinak něž pomocí #define, tak je třeba upravit toto "volání" makra.
//FIXME: Volitelné: Pokud chcete skrýt tuto implementaci v GUI, tak zakomentujte REGISTER_USER_SIMULATIUON().
REGISTER_USER_SIMULATIUON(SimUser3, SimClassStr, "");
// } //NAMESPACE_END