#ifndef SIMUSER4_H
#define SIMUSER4_H

#include "../Simulation.h"
#include "../Registrator.h"

/**
 * @class SimClass
 * @brief Třída reprezentující simulaci.
 *
 * Tato třída simuluje měkou kostku tvořenou částicemi pomocí gradientního sestupu.
 */

class SimUser4 : public Simulation
{
    public:
        /// Konstruktor.
        SimUser4() : Simulation() {};
        /// Destruktor.
        ~SimUser4() {};

        /// Kopírovací konstruktor. Není povolen.
        SimUser4(const SimUser4& other) = delete;
        /// Přesouvací konstruktor. Není povolen.
        SimUser4(SimUser4&& other) noexcept = delete;

        /// Opeátor přiřazení. Není povolen.
        SimUser4& operator=(const SimUser4& other) = delete;
        /// Operátor přiřazení pomocí přesunutí. Není povolen.
        SimUser4& operator=(SimUser4&& other) noexcept = delete;

        //spuštění simulace
        virtual void userRun() override;
        //krok simulace
        virtual bool userStep() override;

    private:
        //funkce pro ošetření hranic simulačního prostoru
        void handleBorders(Particle & a);
};

////////////////////////////////////////////////////////////////////////////////////////


#endif

