/**
 * @file      Simulation.h
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Hlavičkový soubor definující třídu Simulation.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SIMULATION_H
#define SIMULATION_H

#include <vector>
#include <tuple>
#include <mutex>
#include <thread>
#include <condition_variable>

#include "zaklad/Timer.h"

#include "Particle.h"
#include "ParticleArray.h"

#include "SimSettings.h"

#include "Storage.h"

/**
 * @class Simulation
 * @brief Třída reprezentující simulaci.
 *
 * Tato třída simuluje měkou kostku tvořenou částicemi pomocí gradientního sestupu.
 */
class Simulation
{
    public:
        enum simType : int
        {
            eSimPlayer = 0,
            eSimEmpty = 1,
            eSimCPU = 2,
            eSimMultiCPU = 3
        };

        /// Konstruktor.
        Simulation();
        /// Destruktor.
        virtual ~Simulation();

        /// Kopírovací konstruktor. Není povolen.
        Simulation(const Simulation& other) = delete;
        /// Přesouvací konstruktor. Není povolen.
        Simulation(Simulation&& other) noexcept = delete;

        /// Opeátor přiřazení. Není povolen.
        Simulation& operator=(const Simulation& other) = delete;
        /// Operátor přiřazení pomocí přesunutí. Není povolen.
        Simulation& operator=(Simulation&& other) noexcept = delete;

        virtual std::string simName()
        {
            return "Simulation";
        };

        //inicializace simulace
        virtual void init();
        void init(InitSettings initSett, LiveSettings liveSett);
        virtual void init(InitSettings initSett, LiveSettings liveSett, std::string name, bool readFlag, bool benchFlag, std::string fileName);
        //nastavení živých parametrů simulace
        void set(LiveSettings liveSett);

        //ovládání z GUI
        void asyncStart(bool catchAll = false);
        void asyncStop();
        void asyncPause();
        void asyncResume();

        void loadFrame(int frameNum);

        //kontrola simulace
        std::thread* mSimThread = NULL;

        //signalizace do simluace
        std::mutex mPauseMutex;
        volatile int mPauseOnFrame = -1;
        volatile bool mPauseEveryFrame = false;
        volatile bool mStopRequestedFlag = false;

        //signalizace ze simluace
        volatile bool mThreadIsRunning = false;
        volatile bool mSimIsRunning = false;

        //indikátor stavu simluace
        volatile float mTime = 0;
        volatile int mFrame = 0;

        //Vypočtené parametry
        int mCubeSize;
        FloatV mCenter;

        //Výstup simulace
        ParticleArray* mParticles = NULL;
        Storage* mStorage = NULL;
        double mEnergy = 0;
        double mVolume = 0;

        float mStepTimeMS = 0;

    protected:
        std::vector<Particle> mAltParticles;

        //Nastavení simulace
        struct LiveSettings mLiveSett;
        struct InitSettings mInitSett;

        //spuštění simulace
        void userNoExceptRun();
        virtual void userRun() {};
        //krok simulace
        virtual bool userStep()
        {
            return false;
        };

        void userSignalStart();
        void userSignalStepBegin();
        void userSignalStepEnd();
        bool userStopRequested();
        void userSignalFinish();


        void pauseIfRequested();

    private:
        bool mBenchFlag = false;
        bool mFirstStep = true;

        //Časovač pro měření čas kroku simulace
        Timer mSimTimer;

        void storeFrame();
        void closeStorage();

};

#endif // SIMULATION_H
