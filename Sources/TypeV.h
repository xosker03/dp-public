/**
 * @file      floatV.h
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Hlavičkový soubor, který definuje a implementuje strukturu FloatV.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TYPEV_H
#define TYPEV_H

#include <math.h>
#include <stdint.h>
#include <string>
#include <ostream>

#include "zaklad/zaklad.h"
#include "zaklad/TError.h"

/**
 * @brief Struktura reprezentující vektor tří reálných čísel.
 */
template <typename TYPE>
struct TypeV
{
    //inline FloatV(float x) = delete; //NEPOUŽÍVAT

    /// Konstruktor. Nuluje vektor
    #ifdef __CUDACC__
    __host__ __device__
    #endif
    __always_inline TypeV()
    {
        mX = 0;
        mY = 0;
        mZ = 0;
    }

    /// Konstruktor.
    #ifdef __CUDACC__
    __host__ __device__
    #endif
    __always_inline TypeV(TYPE x, TYPE y, TYPE z = 0.0)
    {
        mX = x;
        mY = y;
        mZ = z;
    }

    /// Kopírovací konstruktor.
    #ifdef __CUDACC__
    __host__ __device__
    #endif
    __always_inline TypeV(const TypeV& other)
    {
        mX = other.mX;
        mY = other.mY;
        mZ = other.mZ;
    }

    // float* operator*()(int pos) {
    //     return (this->data);
    // }
    // const float* operator*()(int pos) const {
    //     return (this->data);
    // }

    /// Přístup k prvkům.
    __always_inline float& operator[](int pos)
    {
        switch (pos)
        {
        case 0:
            return mX;
        case 1:
            return mY;
        case 2:
            return mZ;
        default:
            throw QTError("TypeV::operator[]: přístup mimo TypeV");
        }
    }

    /// Přístup k prvkům.
    __always_inline const float& operator[](int pos) const
    {
        switch (pos)
        {
        case 0:
            return mX;
        case 1:
            return mY;
        case 2:
            return mZ;
        default:
            throw QTError("TypeV::operator[]: přístup mimo TypeV");
        }
    }

    /// Porovnání.
    #ifdef __CUDACC__
    __host__ __device__
    #endif
    __always_inline bool operator==(const TypeV& right) const
    {
        return mX == right.mX && mY == right.mY && mZ == right.mZ;
    }

    /// Přiřazení.
    #ifdef __CUDACC__
    __host__ __device__
    #endif
    __always_inline TypeV& operator=(const TypeV& source)
    {
        mX = source.mX;
        mY = source.mY;
        mZ = source.mZ;
        return *this;
    }

    /// Vektorové sčítání.
    #ifdef __CUDACC__
    __host__ __device__
    #endif
    __always_inline TypeV operator+(const TypeV& right) const
    {
        return TypeV(mX + right.mX, mY + right.mY, mZ + right.mZ);
    }

    /// Vektorové sčítání.
    #ifdef __CUDACC__
    __host__ __device__
    #endif
    __always_inline TypeV& operator+=(const TypeV& right)
    {
        mX += right.mX;
        mY += right.mY;
        mZ += right.mZ;
        return *this;
    }

    /// Vektorové odčítání.
    #ifdef __CUDACC__
    __host__ __device__
    #endif
    __always_inline TypeV operator-(const TypeV& right) const
    {
        return TypeV(mX - right.mX, mY - right.mY, mZ - right.mZ);
    }

    /// Vektorové odčítání.
    #ifdef __CUDACC__
    __host__ __device__
    #endif
    __always_inline TypeV& operator-=(const TypeV& right)
    {
        mX -= right.mX;
        mY -= right.mY;
        mZ -= right.mZ;
        return *this;
    }

    /// Vynásobení konstantou.
    #ifdef __CUDACC__
    __host__ __device__
    #endif
    __always_inline TypeV operator*(const TYPE right) const
    {
        return TypeV(mX * right, mY * right, mZ * right);
    }

    /// Vynásobení konstantou.
    #ifdef __CUDACC__
    __host__ __device__
    #endif
    __always_inline TypeV& operator*=(const TYPE right)
    {
        mX *= right;
        mY *= right;
        mZ *= right;
        return *this;
    }

    /// Vynásobení konstantou z levé strany.
    #ifdef __CUDACC__
    __host__ __device__
    #endif
    __always_inline friend TypeV operator*(const TYPE left, const TypeV& right)
    {
        return TypeV(left * right.mX, left * right.mY, left * right.mZ);
    }

    /// Vydělení konstantou.
    #ifdef __CUDACC__
    __host__ __device__
    #endif
    __always_inline TypeV operator/(const TYPE right) const
    {
        return TypeV(mX / right, mY / right, mZ / right);
    }

    /// Vydělení konstantou.
    #ifdef __CUDACC__
    __host__ __device__
    #endif
    __always_inline TypeV& operator/=(const TYPE right)
    {
        mX /= right;
        mY /= right;
        mZ /= right;
        return *this;
    }

    /// Vektorový součin.
    #ifdef __CUDACC__
    __host__ __device__
    #endif
    __always_inline TypeV operator^(const TypeV& b) const
    {
        TypeV ret;
        ret.mX = mY * b.mZ - mZ * b.mY;
        ret.mY = mZ * b.mX - mX * b.mZ;
        ret.mZ = mX * b.mY - mY * b.mX;
        return ret;
    }

    /// Skalární součin.
    #ifdef __CUDACC__
    __host__ __device__
    #endif
    __always_inline TYPE operator&(const TypeV& right)
    {
        return mX * right.mX + mY * right.mY + mZ * right.mZ;
    }

    /// Délka vektoru.
    #ifdef __CUDACC__
    __host__ __device__
    #endif
    __always_inline TYPE len()
    {
        return sqrt(mX * mX + mY * mY + mZ * mZ);
    }

    /// Délka vektoru na druhou.
    #ifdef __CUDACC__
    __host__ __device__
    #endif
    __always_inline TYPE lenSq()
    {
        return mX * mX + mY * mY + mZ * mZ;
    }

    /// Délka vektoru.
    #ifdef __CUDACC__
    __host__ __device__
    #endif
    __always_inline TYPE lenFast()
    {
        float toSqrt = mX * mX + mY * mY + mZ * mZ;

        //https://en.wikipedia.org/wiki/Fast_inverse_square_root
        int32_t i;
        float x2, y;
        const float threehalfs = 1.5F;

        x2 = toSqrt * 0.5F;
        y  = toSqrt;
        i  = * ( int32_t * ) &y;                       // evil floating point bit level hacking
        i  = 0x5f3759df - ( i >> 1 );               // what the fuck?
        y  = * ( float * ) &i;
        y  = y * ( threehalfs - ( x2 * y * y ) );   // 1st iteration
        // y  = y * ( threehalfs - ( x2 * y * y ) );   // 2nd iteration, this can be removed

        return 1.0 / y;
    }
    /// Délka vektoru.
    #ifdef __CUDACC__
    __host__ __device__
    #endif
    __always_inline TYPE invLenFast()
    {
        float toSqrt = mX * mX + mY * mY + mZ * mZ;

        //https://en.wikipedia.org/wiki/Fast_inverse_square_root
        int32_t i;
        float x2, y;
        const float threehalfs = 1.5F;

        x2 = toSqrt * 0.5F;
        y  = toSqrt;
        i  = * ( int32_t * ) &y;                       // evil floating point bit level hacking
        i  = 0x5f3759df - ( i >> 1 );               // what the fuck?
        y  = * ( float * ) &i;
        y  = y * ( threehalfs - ( x2 * y * y ) );   // 1st iteration
        // y  = y * ( threehalfs - ( x2 * y * y ) );   // 2nd iteration, this can be removed

        return y;
    }

    /// Normalizovaný vektor
    #ifdef __CUDACC__
    __host__ __device__
    #endif
    __always_inline TypeV norm()
    {
        return *this / this->len();
    }

    /// Test na NaN.
    #ifdef __CUDACC__
    __host__ __device__
    #endif
    __always_inline bool isnan()
    {
        return std::isnan(mX) || std::isnan(mY) || std::isnan(mZ);
    }

    std::string str()
    {
        return "x:" + std::to_string(mX) + ", y:" + std::to_string(mY) + ", z:" + std::to_string(mZ);
    }

    friend std::ostream & operator<<(std::ostream &str, const TypeV &a)
    {
        return str << "< " << a.mX << " " << a.mY << " " << a.mZ << " >";
    }

    void print(bool newline = true)
    {
        if (newline)
        {
            printf("TypeV(%e, %e, %e),\n", mX, mY, mZ);
        }
        else
        {
            printf("x: %e, y: %e, z: %e", mX, mY, mZ);
        }
    }

    /// Souřadnice x, y, z.
    TYPE mX;
    TYPE mY;
    TYPE mZ;
};

int floatVSelfTest();



using IntV = TypeV<int>;
using FloatV = TypeV<float>;
using DoubleV = TypeV<double>;


#endif
