/**
 * @file      SimSettings.h
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Hlavičkový soubor definující struktury LiveSettings a InitSerttings.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SIM_SETTINGS_H
#define SIM_SETTINGS_H

#include "TypeV.h"

/**
 * @struct LiveSettings
 * @brief Struktura s nastevením simluace, které je měnitelné za běhu.
 */
struct LiveSettings
{
    //Live parameters
    FloatV mExternalAcceleration = FloatV(0, 0);
    float mGravityEnabled = 1.0f;
    float mDT = 1e-3f; // s
    int mSubsteps = 4;
    int mParticleNeighbours = 4;
    float mSpacing = 0.04;

    //Hidden parameters
    float mAlpha = 1e-6;

    //sim space settings
    FloatV mGravityAccell = FloatV(0, -9.81);
    float mDumpingPerpendicular = 0.999f;
    float mDumping = (0.75f + (1 - 0.999f));
    float mRestitution = 0.99f;

    //sim space settings
    FloatV mWallL = FloatV(0.0, 0.0, 0.0);
    FloatV mWallR = FloatV(1.0, 1.0, 1.0);
};

/**
 * @struct InitSerttings
 * @brief Struktura s nastevením simluace, které je měnitelné pouze před startem simulace.
 */
struct InitSettings
{
    int mCubeSide = 7;
    double mTime = 0; // s
    double mEndTime = 10.0; // s
    FloatV mInitialSpeed;
};

#endif
