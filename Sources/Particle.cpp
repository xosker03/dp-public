/**
 * @file      Particle.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Soubor implementující strukturu Particle.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include "Particle.h"

/**
 * Konstruktor. Inicializuje na 0.
 */
Particle::Particle()
{
    mPos = FloatV();
    mPosNew = FloatV();
    mVelocity = FloatV();
    mPosStart = FloatV();
}

/**
 * Konstruktor.
 */
Particle::Particle(FloatV pos, FloatV speed, float mass)
{
    mPos = pos;
    mPosNew = FloatV();
    mVelocity = speed;
    mPosStart = pos;
}

/**
 * Kopírovací konstruktor.
 */
Particle::Particle(const Particle& other)
{
    mPos = other.mPos;
    mPosNew = other.mPosNew;
    mVelocity = other.mVelocity;
    mPosStart = other.mPosStart;
}

/**
 * Destruktor.
 */
Particle::~Particle()
{
}

/**
 * Opeátor přiřazení.
 */
Particle& Particle::operator=(Particle& other)
{
    mPos = other.mPos;
    mPosNew = other.mPosNew;
    mVelocity = other.mVelocity;
    mPosStart = other.mPosStart;
    return *this;
}

/**
 * Vytiskne informace o částici.
 */
void Particle::print()
{
    printf("pos: %f %f,\t V: %f %f\n",
           mPos.mX, mPos.mY,
           mVelocity.mX, mVelocity.mY);
}
