/**
 * @file      Storage.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Soubor implementující třídu Storage.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include "Storage.h"

#include <string.h>
#include <chrono>
#include <iomanip>

#include "zaklad/TError.h"

// constexpr const char * cSettingsDatasetName = "ASettings";
constexpr const char * cInitSettAtributName = "InitSett";
constexpr const char * cLiveSettAtributName = "LiveSett";
constexpr const char * cFrameAtributName = "Frame";

Storage::Storage(std::string name)
{
    mName = name;
}

Storage::Storage(std::string name, bool readFlag, bool benchFlag, std::string fileName, bool specialFlag)
{
    mName = name;
    mReadFlag = readFlag;
    mBenchFlag = benchFlag;
    mFileName = fileName;

    //Takhle by to mělo být
    // mFloatVType = H5::CompType(sizeof(FloatV));
    // mFloatVType.insertMember("x", HOFFSET(FloatV, mX), H5::PredType::NATIVE_FLOAT);
    // mFloatVType.insertMember("y", HOFFSET(FloatV, mY), H5::PredType::NATIVE_FLOAT);
    // mFloatVType.insertMember("z", HOFFSET(FloatV, mZ), H5::PredType::NATIVE_FLOAT);

    //Takhle to funguje v hdfview
    hsize_t arrayDims[1] {3};
    mFloatVType = H5::ArrayType(H5::PredType::NATIVE_FLOAT, 1, arrayDims);

    mLiveSettingsType = H5::CompType(sizeof(LiveSettings));
    mLiveSettingsType.insertMember("mExternalAcceleration", HOFFSET(LiveSettings, mExternalAcceleration), mFloatVType);
    mLiveSettingsType.insertMember("mGravityEnabled", HOFFSET(LiveSettings, mGravityEnabled), H5::PredType::NATIVE_FLOAT);
    mLiveSettingsType.insertMember("mDT", HOFFSET(LiveSettings, mDT), H5::PredType::NATIVE_FLOAT);
    mLiveSettingsType.insertMember("mSubsteps", HOFFSET(LiveSettings, mSubsteps), H5::PredType::NATIVE_INT);
    mLiveSettingsType.insertMember("mParticleNeighbours", HOFFSET(LiveSettings, mParticleNeighbours), H5::PredType::NATIVE_INT);
    mLiveSettingsType.insertMember("mSpacing", HOFFSET(LiveSettings, mSpacing), H5::PredType::NATIVE_FLOAT);
    mLiveSettingsType.insertMember("mAlpha", HOFFSET(LiveSettings, mAlpha), H5::PredType::NATIVE_FLOAT);
    mLiveSettingsType.insertMember("mGravityAccell", HOFFSET(LiveSettings, mGravityAccell), mFloatVType);
    mLiveSettingsType.insertMember("mDumpingPerpendicular", HOFFSET(LiveSettings, mDumpingPerpendicular), H5::PredType::NATIVE_FLOAT);
    mLiveSettingsType.insertMember("mDumping", HOFFSET(LiveSettings, mDumping), H5::PredType::NATIVE_FLOAT);
    mLiveSettingsType.insertMember("mRestitution", HOFFSET(LiveSettings, mRestitution), H5::PredType::NATIVE_FLOAT);
    mLiveSettingsType.insertMember("mWallL", HOFFSET(LiveSettings, mWallL), mFloatVType);
    mLiveSettingsType.insertMember("mWallR", HOFFSET(LiveSettings, mWallR), mFloatVType);

    mInitSettingsType = H5::CompType(sizeof(InitSettings));
    mInitSettingsType.insertMember("mCubeSide", HOFFSET(InitSettings, mCubeSide), H5::PredType::NATIVE_INT);
    mInitSettingsType.insertMember("mTime", HOFFSET(InitSettings, mTime), H5::PredType::NATIVE_FLOAT);
    mInitSettingsType.insertMember("mEndTime", HOFFSET(InitSettings, mEndTime), H5::PredType::NATIVE_FLOAT);
    mInitSettingsType.insertMember("mInitialSpeed", HOFFSET(InitSettings, mInitialSpeed), mFloatVType);

    mFrameType = H5::CompType(sizeof(Storage::Frame));
    mFrameType.insertMember("mFrameNum", HOFFSET(Frame, mFrameNum), H5::PredType::NATIVE_INT);
    mFrameType.insertMember("mSimTime", HOFFSET(Frame, mSimTime), H5::PredType::NATIVE_FLOAT);
    mFrameType.insertMember("mStepMS", HOFFSET(Frame, mStepMS), H5::PredType::NATIVE_FLOAT);
    mFrameType.insertMember("mArraySize", HOFFSET(Frame, mArraySize), H5::PredType::NATIVE_INT);
    mFrameType.insertMember("mEnergy", HOFFSET(Frame, mEnergy), H5::PredType::NATIVE_FLOAT);
    mFrameType.insertMember("mVolume", HOFFSET(Frame, mVolume), H5::PredType::NATIVE_FLOAT);

    mGroupName = mName;

    openHDF5File(specialFlag);
}

Storage::~Storage()
{
    if (mFileOpenFlag)
    {
        closeHDF5File();
    }
    // printblue("Zavírám %s\n", mFileName.c_str());
}

void Storage::openHDF5File(bool specialFlag)
{
    unsigned int flags = 0;

    if (mReadFlag)
    {
        flags = H5F_ACC_RDONLY;
    }
    else
    {
        flags = H5F_ACC_RDWR | H5F_ACC_CREAT; //H5F_ACC_TRUNC //H5F_ACC_RDWR
    }

    try
    {
        mFile = H5::H5File(mFileName.c_str(), flags);
        // Zde můžete pracovat se souborem
    }
    catch (const H5::FileIException& e)
    {
        // Chyba při otevírání souboru
        throw QTError("Storage::openHDF5File: Chyba při otevírání souboru: " + e.getDetailMsg());
    }
    catch (const H5::Exception& e)
    {
        // Jiná chyba
        throw QTError("Storage::openHDF5File: Chyba: " + e.getDetailMsg());
    }

    if (!specialFlag)
    {
        if (!mReadFlag)
        {
            if (H5Lexists(mFile.getId(), mGroupName.c_str(), H5P_DEFAULT))
            {
                // mGroup = mFile.openGroup(mGroupName);
                auto now = std::chrono::system_clock::now();
                auto now_c = std::chrono::system_clock::to_time_t(now);

                std::tm* now_tm = std::localtime(&now_c);
                std::stringstream ss;
                // ss << std::put_time(now_tm, "%Y/%m/%d_%H:%M:%S");
                ss << std::put_time(now_tm, "%Y-%m-%d_%H:%M:%S");

                // mGroupName += "_" + std::to_string(now_c);
                mGroupName += "_" + ss.str();
                //     mGroup = mFile.createGroup(mGroupName);
                // } else {
                //     mGroup = mFile.createGroup(mGroupName);
            }
            mGroup = mFile.createGroup(mGroupName);
        }
        else
        {
            // printblue("Storage: otevírám: %s\n", mGroupName.c_str());
            mGroup = mFile.openGroup(mGroupName);
        }
        // createDataset(cSettingsDatasetName);
    }

    mFileOpenFlag = true;
}

void Storage::closeHDF5File()
{
    mFileOpenFlag = false;

    try
    {
        mFile.close();
    }
    catch (const H5::FileIException& e)
    {
        // Chyba při zavírání souboru
        throw QTError("Storage::closeHDF5File: Chyba při zavírání souboru: " + e.getDetailMsg());
    }
    catch (const H5::Exception& e)
    {
        // Jiná chyba
        throw QTError("Storage::closeHDF5File: Chyba: " + e.getDetailMsg());
    }
}

void Storage::storeSettings(LiveSettings live, InitSettings init)
{
    if (mReadFlag)
    {
        throw QTError("Storage::storeSettings: Nastaveno pro čtení");
    }

    if (mFileOpenFlag)
    {
        //TODO uložit do souboru
        writeInitSettingsAttribute(init);
        writeLiveSettingsAttribute(live);
    }
}

void Storage::storeFrame(Frame f, FloatV* posArray, FloatV* colorsArray)
{
    if (mReadFlag)
    {
        throw QTError("Storage::storeSettings: Nastaveno pro čtení");
    }

    latestFrameLock.lock();
    mLastFrame = f;

    mStats.push_back(f.mStepMS);

    mLastPosArray.resize(mLastFrame.mArraySize);
    mLastColorsArray.resize(mLastFrame.mArraySize);

    memcpy(mLastPosArray.data(), posArray, mLastFrame.mArraySize * sizeof(FloatV));
    memcpy(mLastColorsArray.data(), colorsArray, mLastFrame.mArraySize * sizeof(FloatV));

    latestFrameLock.unlock();

    if (mFileOpenFlag)
    {
        //TODO uložit do souboru
        std::string dataseName = "PosArray-Frame_" + std::to_string(f.mFrameNum);
        writeDataset(dataseName, f.mArraySize, posArray);
        writeFrameAttribute(dataseName, f);
    }
}

void Storage::loadSettings(LiveSettings& live, InitSettings& init)
{
    if (!mFileOpenFlag)
    {
        throw QTError("Storage::storeSettings: Soubor nebyl otevřen");
    }
    if (!mReadFlag)
    {
        throw QTError("Storage::storeSettings: Nastaveno pro zápis"); //TODO tohle asi nebude potřeba
    }
    readInitSettingsAttribute(init);
    readLiveSettingsAttribute(live);
}

Storage::Frame Storage::loadFrame(int frameNum)
{
    if (frameNum == mLastFrame.mFrameNum)
    {
        return mLastFrame;
    }

    if (!mFileOpenFlag)
    {
        throw QTError("Storage::storeSettings: Soubor nebyl otevřen");
    }
    if (!mReadFlag)
    {
        throw QTError("Storage::storeSettings: Nastaveno pro zápis");
    }

    latestFrameLock.lock();

    try
    {
        std::string dataseName = "PosArray-Frame_" + std::to_string(frameNum);
        readFrameAttribute(dataseName, mLastFrame);
        mStats.push_back(mLastFrame.mStepMS);
        mLastPosArray.resize(mLastFrame.mArraySize);
        readDataset(dataseName, mLastFrame.mArraySize, mLastPosArray.data());
    }
    catch (TError t)
    {
        // printblue("Nejspíš dosažen konec souboru\n");
        latestFrameLock.unlock();
        throw QTError("Nejspíš dosažen konec souboru");
    }

    latestFrameLock.unlock();
    return mLastFrame;
}

Storage::PackedFrame Storage::lockLatestFrame()
{
    latestFrameLock.lock();
    return PackedFrame(mLastFrame, mLastPosArray, mLastColorsArray);
}

void Storage::unLockLatestFrame()
{
    latestFrameLock.unlock();
}

Storage::PackedFrame Storage::lockFrame(int frameNum)
{
    latestFrameLock.lock();
    if (mLastFrame.mFrameNum == frameNum)
    {
        return PackedFrame(mLastFrame, mLastPosArray, mLastColorsArray);
    }
    if (!mFileOpenFlag)
    {
        throw QTError("Storage::lockFrame: Pokus o získání jiného símku než posledního z aktivní simulace"); //TODO nebo možná ještě neexistujícího
    }
    throw QTError("načtení ze souboru zatím neimplementováno");
}

void Storage::unLockFrame()
{
    latestFrameLock.unlock();
}

std::vector<float> Storage::getStats()
{
    latestFrameLock.lock();
    std::vector<float> tmp(mStats);
    latestFrameLock.unlock();
    return tmp;
}

void Storage::save()
{
    closeHDF5File();
    // openHDF5File();
}

std::vector<std::string> Storage::listGroups()
{
    std::vector<std::string> groups;
    if (mFileOpenFlag)
    {
        H5Literate(mFile.getId(), H5_INDEX_NAME, H5_ITER_NATIVE, NULL, [](hid_t loc_id, const char* name, const H5L_info_t* info, void* operator_data) -> herr_t
        {
            std::vector<std::string>* groups = static_cast<std::vector<std::string>*>(operator_data);
            groups->push_back(name);
            return 0;
        }, &groups);
    }
    return groups;
}

void Storage::createDataset(const std::string& datasetName)
{
    try
    {
        // Vytvoření datového prostoru
        hsize_t dim[1] = {0}; // počáteční rozměr datasetu
        H5::DataSpace dataspace(1, dim);

        // Vytvoření datasetu
        H5::DataSet dataset = mGroup.createDataSet(datasetName, H5::PredType::NATIVE_FLOAT, dataspace);
    }
    catch (const H5::Exception& e)
    {
        // Chyba
        throw QTError("Storage::createDataset: Chyba: " + e.getDetailMsg());
    }
}

void Storage::writeDataset(const std::string& datasetName, unsigned long  size, FloatV* data)
{
    try
    {
        // Vytvoření vlastního datového typu pro trojici floatů
        H5::CompType FloatVType(sizeof(FloatV));
        FloatVType.insertMember("x", HOFFSET(FloatV, mX), H5::PredType::NATIVE_FLOAT);
        FloatVType.insertMember("y", HOFFSET(FloatV, mY), H5::PredType::NATIVE_FLOAT);
        FloatVType.insertMember("z", HOFFSET(FloatV, mZ), H5::PredType::NATIVE_FLOAT);

        // Vytvoření datové sady
        hsize_t dim[] = {size}; // rozměry datové sady
        H5::DataSpace dataspace(1, dim);
        H5::DataSet dataset = mGroup.createDataSet(datasetName, FloatVType, dataspace);

        // Zápis dat do datové sady
        dataset.write(data, FloatVType);
    }
    catch (const H5::Exception& e)
    {
        // Chyba
        throw QTError("Storage::writeData: Chyba: " + e.getDetailMsg());
    }
}

void Storage::readDataset(const std::string& datasetName, unsigned long  size, FloatV* data)
{
    try
    {
        H5::DataSet dataset = mGroup.openDataSet(datasetName);

        // Zjištění rozměrů datové sady
        H5::DataSpace dataspace = dataset.getSpace();
        hsize_t dim_out[1];
        dataspace.getSimpleExtentDims(dim_out, NULL);
        // data.resize(dim_out[0]);
        if (dim_out[0] != size)
        {
            QTError("Storage::readDataset: nčítaný datase je jinak velký");
        }

        // Vytvoření vlastního datového typu pro trojici floatů
        H5::CompType FloatVType(sizeof(FloatV));
        FloatVType.insertMember("x", HOFFSET(FloatV, mX), H5::PredType::NATIVE_FLOAT);
        FloatVType.insertMember("y", HOFFSET(FloatV, mY), H5::PredType::NATIVE_FLOAT);
        FloatVType.insertMember("z", HOFFSET(FloatV, mZ), H5::PredType::NATIVE_FLOAT);

        // Čtení dat z datové sady
        dataset.read(data, FloatVType);
    }
    catch (const H5::Exception& e)
    {
        // Chyba
        throw QTError("Storage::readData: Chyba: " + e.getDetailMsg());
    }

    return;
}

void Storage::writeLiveSettingsAttribute(const LiveSettings& attributeValue)
{
    try
    {
        // Vytvoření atributu
        H5::DataSpace attr_dataspace = H5::DataSpace(H5S_SCALAR);
        H5::Attribute attribute = mGroup.createAttribute(cLiveSettAtributName, mLiveSettingsType, attr_dataspace);

        // Zápis hodnoty do atributu
        attribute.write(mLiveSettingsType, &attributeValue);
    }
    catch (const H5::Exception& e)
    {
        // Chyba
        throw QTError("Storage::writeLiveSettingsAttribute: Chyba: " + e.getDetailMsg());
    }
}

void Storage::writeInitSettingsAttribute(const InitSettings& attributeValue)
{
    try
    {
        // Vytvoření atributu
        H5::DataSpace attr_dataspace = H5::DataSpace(H5S_SCALAR);
        H5::Attribute attribute = mGroup.createAttribute(cInitSettAtributName, mInitSettingsType, attr_dataspace);

        // Zápis hodnoty do atributu
        attribute.write(mInitSettingsType, &attributeValue);
    }
    catch (const H5::Exception& e)
    {
        // Chyba
        throw QTError("Storage::writeInitSettingsAttribute: Chyba: " + e.getDetailMsg());
    }
}

void Storage::readLiveSettingsAttribute(LiveSettings& attributeValue)
{
    try
    {
        // Otevření atributu
        H5::Attribute attribute = mGroup.openAttribute(cLiveSettAtributName);

        // Čtení hodnoty z atributu
        attribute.read(mLiveSettingsType, &attributeValue);
    }
    catch (const H5::Exception& e)
    {
        // Chyba
        throw QTError("Storage::readLiveSettingsAttribute: Chyba: " + e.getDetailMsg());
    }
}

void Storage::readInitSettingsAttribute(InitSettings& attributeValue)
{
    try
    {
        // Otevření atributu
        H5::Attribute attribute = mGroup.openAttribute(cInitSettAtributName);

        // Čtení hodnoty z atributu
        attribute.read(mInitSettingsType, &attributeValue);
    }
    catch (const H5::Exception& e)
    {
        // Chyba
        throw QTError("Storage::readInitSettingsAttribute: Chyba: " + e.getDetailMsg());
    }
}

void Storage::writeFrameAttribute(const std::string& datasetName, const Frame& attributeValue)
{
    try
    {
        H5::DataSet dataset = mGroup.openDataSet(datasetName);

        // Vytvoření atributu
        H5::DataSpace attr_dataspace = H5::DataSpace(H5S_SCALAR);
        H5::Attribute attribute = dataset.createAttribute(cFrameAtributName, mFrameType, attr_dataspace);

        // Zápis hodnoty do atributu
        attribute.write(mFrameType, &attributeValue);
    }
    catch (const H5::Exception& e)
    {
        // Chyba
        throw QTError("Storage::writeFrameAttribute: Chyba: " + e.getDetailMsg());
    }
}

void Storage::readFrameAttribute(const std::string& datasetName, Frame& attributeValue)
{
    try
    {
        H5::DataSet dataset = mGroup.openDataSet(datasetName);

        // Vytvoření atributu
        // H5::DataSpace attr_dataspace = H5::DataSpace(H5S_SCALAR);
        // H5::Attribute attribute = dataset.createAttribute(cFrameAtributName, mFrameType, attr_dataspace);
        H5::Attribute attribute = dataset.openAttribute(cFrameAtributName);

        // Zápis hodnoty do atributu
        attribute.read(mFrameType, &attributeValue);
    }
    catch (const H5::Exception& e)
    {
        // Chyba
        throw QTError("Storage::readFrameAttribute: Chyba: " + e.getDetailMsg());
    }

}
