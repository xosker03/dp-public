/**
 * @file      Registrator.h
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Hlavičkový soubor definující třídu Registrator.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REGISTRATOR_H
#define REGISTRATOR_H

#include <algorithm>
#include "Simulation.h"

class Registrator
{
    public:
        Registrator() = delete;
        ~Registrator() = delete;

        struct Module
        {
            std::string mName = "NoName";
            std::string mDescription = "NoDescription";
            int mPriority = false;
            Simulation* (*mCreate)() = NULL;
        };

        struct SelfRegModule : Module
        {
            SelfRegModule(const std::string& name, Simulation * (*create)(), int priority, std::string description)
            {
                mName = name;
                mCreate = create;
                mDescription = description;
                mPriority = priority;
                Registrator::registerModule(*this);
            }
        };

        static void registerModule(Module m)
        {
            mModules.push_back(m);
        };

        static void initModules()
        {
            sortModules();
            for (auto& m : mModules)
            {
                mNames.push_back(m.mName.c_str());
            }
        }

        static std::vector<Module>& getModules()
        {
            return mModules;
        };
        static std::vector<const char *>& modulesNames()
        {
            return mNames;
        };
        static int modulesCount()
        {
            return mModules.size();
        };

    private:
        static void sortModules()
        {
            std::stable_sort(mModules.begin(), mModules.end(), [](const Module & a, const Module & b)
            {
                return a.mName < b.mName;
            });
            std::stable_sort(mModules.begin(), mModules.end(), [](const Module & a, const Module & b)
            {
                return a.mPriority < b.mPriority;
            });
        }

        static std::vector<Module> mModules;
        static std::vector<const char *> mNames;
};

//https://gcc.gnu.org/onlinedocs/cpp/Macros.html
//https://gcc.gnu.org/onlinedocs/gcc-7.5.0/cpp/Argument-Prescan.html
#define CONCAT(A) __registrator_ ## A
#define EXPAND(A) CONCAT(A)

#define REGISTER_MODULE(name, sim, priority, desc) static Registrator::SelfRegModule EXPAND(__COUNTER__) (name, sim, priority, desc)

#define REGISTER_SIMULATIUON(classname, name, priority, desc) REGISTER_MODULE(name, [](){return static_cast<Simulation*>(new classname());}, priority, desc)
#define REGISTER_USER_SIMULATIUON(classname, name, desc) REGISTER_MODULE(name, [](){return static_cast<Simulation*>(new classname());}, 4, desc)

#endif
