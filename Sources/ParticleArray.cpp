/**
 * @file      ParticleArray.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Soubor implementující třídu ParticleArray.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include "ParticleArray.h"

#include <assert.h>

/**
 * Konstruktor.
 * @param size - Počet částic.
 */
ParticleArray::ParticleArray(unsigned size)
{
    mSize = size;
    if (mSize)
    {
        mPosArray = (FloatV *) malloc(size * sizeof(FloatV));
        mPosNewArray = (FloatV *) malloc(size * sizeof(FloatV));
        mVelocityArray = (FloatV *) malloc(size * sizeof(FloatV));
        mPosStartArray = (FloatV *) malloc(size * sizeof(FloatV));
        mColorsArray = (FloatV *) malloc(size * sizeof(FloatV));
    }
}

/**
 * Destruktor.
 */
ParticleArray::~ParticleArray()
{
    if (mSize)
    {
        free(mPosArray);
        free(mPosNewArray);
        free(mVelocityArray);
        free(mPosStartArray);
        free(mColorsArray);
    }
}

/**
 * Nastaví částici na daném indexu.
 * @param idx - Index částice.
 * @param p - Částice.
 */
void ParticleArray::set(unsigned idx, Particle p)
{
    assert(idx < mSize && "ParticleArray not initialized");
    mPosArray[idx] = p.mPos;
    mPosNewArray[idx] = p.mPosNew;
    mVelocityArray[idx] = p.mVelocity;
    mPosStartArray[idx] = p.mPosStart;
}

/**
 * Vrátí částici na daném indexu.
 * @param idx - Index částice.
 * @return Částice.
 */
Particle ParticleArray::get(unsigned idx)
{
    assert(idx < mSize && "ParticleArray not initialized");
    Particle p;
    p.mPos = mPosArray[idx];
    p.mPosNew = mPosNewArray[idx];
    p.mVelocity = mVelocityArray[idx];
    p.mPosStart = mPosStartArray[idx];
    return p;
}

/**
 * Prohodí pole mPosArray a mPosNewArray.
 */
void ParticleArray::swapPos()
{
    FloatV * tmp = mPosArray;
    mPosArray = mPosNewArray;
    mPosNewArray = tmp;
}
