/**
 * @file      ParticleArray.h
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Hlavičkový soubor definující třídu ParticleArray.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PARTICLE_ARRAY_H
#define PARTICLE_ARRAY_H

#include "TypeV.h"
#include "Particle.h"

/**
 * @class ParticleArray
 * @brief Třída reprezentující pole částic.
 *
 * Tato třída obsahuje pole prvků částice a umožňuje s nimi pracovat jako s normálními částicemi.
 * Je určená pro práci s částicemi v simulaci s ohledem na výkon na GPU.
 */
class ParticleArray
{
    public:
        /// Konstruktor.
        ParticleArray(unsigned size = 0);
        /// Destruktor.
        ~ParticleArray();

        /// Kopírovací konstruktor. Není povolen.
        ParticleArray(const ParticleArray& other) = delete;
        /// Přesouvací konstruktor. Není povolen.
        ParticleArray(ParticleArray&& other) noexcept = delete;

        /// Opeátor přiřazení. Není povolen.
        ParticleArray& operator=(const ParticleArray& other) = delete;
        /// Operátor přiřazení pomocí přesunutí. Není povolen.
        ParticleArray& operator=(ParticleArray&& other) noexcept = delete;

        /// Nastaví částici na daném indexu.
        void set(unsigned idx, Particle p);
        /// Vrátí částici na daném indexu.
        Particle get(unsigned idx);

        /// Prohodí pole mPosArray a mPosNewArray. Proč? viz Particle.
        void swapPos();

        /// Vrátí velikost pole.
        unsigned mSize = 0;

        //TODO rozložit i floatV
        /// Pole pozic částic. Viz Particle.
        FloatV* mPosArray;
        /// Pole nových pozic částic. Viz Particle.
        FloatV* mPosNewArray;
        /// Pole počátečních pozic částic. Viz Particle.
        FloatV* mPosStartArray;

        /// Pole rychlostí částic.
        FloatV* mVelocityArray;

        FloatV* mColorsArray;

    protected:
};

#endif
