/**
 * @file      GLEngineBenchScreen.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Soubor implementující třídu GLEngine.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include "GLEngine.h"

#include <stdint.h>

#include <algorithm>
#include <unordered_map>
#include <limits>
#include <set>

std::vector<int> cubeSizes;
std::vector<Simulation::simType> simTypes;
int neighboursInitial = 1;

int initialBenchScriptSize = 0;

using bSType = std::tuple<int, int, Simulation::simType>;
std::vector<bSType> benchScript;

using sideSizeType = std::vector<int>;
using neighbourCountType = std::vector<int>;
using constrainCountType = std::vector<float>;
using stepTimeType = std::vector<float>;
using resultsType = std::tuple<sideSizeType, neighbourCountType, constrainCountType,  stepTimeType>;
std::unordered_map<Simulation::simType, resultsType> plotableResults;

/**
 * @brief Spočítá počet omezení částic v závislosti na velikosti kostky a počtu sousedů.
 * 
 * @param cubeSide - počet částic na jedné hraně kostky
 * @param neighbours - počet sousedů
 * @return int 
 */
uint64_t computeConditions(int cubeSide, int neighbours)
{
    static std::unordered_map<std::string, uint64_t> sConditionsMap;

    if (sConditionsMap.find(std::to_string(cubeSide) + "_" + std::to_string(neighbours)) != sConditionsMap.end())
    {
        return sConditionsMap[std::to_string(cubeSide) + "_" + std::to_string(neighbours)];
    }

    uint64_t count = 0;

    for (int x = 0; x < cubeSide; ++x)
        for (int y = 0; y < cubeSide; ++y)
            for (int z = 0; z < cubeSide; ++z)
                for (int dx = -neighbours; dx <= neighbours; ++dx)
                {
                    for (int dy = -neighbours; dy <= neighbours; ++dy)
                    {
                        for (int dz = -neighbours; dz <= neighbours; ++dz)
                        {
                            //pokud soused je mimo pole existujících částit, tzn. neexistuje, tak se přeskočí
                            if (x + dx < 0 || x + dx >= cubeSide || y + dy < 0 || y + dy >= cubeSide || z + dz < 0 || z + dz >= cubeSide)
                            {
                                continue;
                            }
                            //vybraná částice a soused je totožná
                            if (dx == 0 && dy == 0 && dz == 0)
                            {
                                continue;
                            }

                            ++count;
                        }
                    }
                }

    sConditionsMap[std::to_string(cubeSide) + "_" + std::to_string(neighbours)] = count;

    return count;
}

/**
 * @brief Uloží výsledky do CSV souboru.
 * 
 * @param filename - název souboru
 * @param append - pokud true, tak se výsledky přidají na konec souboru, jinak se soubor přepíše
 */
void saveToCSV(const std::string& filename, bool append)
{
    std::ofstream file;

    if(append)
    {
        file.open(filename, std::ios_base::app);
    }
    else
    {
        file.open(filename);
        file << "#SimTypeStr,SimType,SideSize,Neighbours,Conditions,Result\n";
    }

    for (auto& [k, v] : plotableResults)
    {
        auto& [s, n, c, r] = v;
        for (int i = 0; i < r.size(); ++i)
        {
            file << Registrator::modulesNames()[k] << ',' << k << ',' << s[i] << ',' << n[i] << ',' << c[i] << ',' << r[i] << '\n';
        }
    }
    file.close();
}

/**
 * @brief Načte výsledky z CSV souboru.
 * @param filename - název souboru
 */
void loadFromCSV(const std::string& filename)
{
    std::ifstream file(filename);
    std::string line;
    while (std::getline(file, line))
    {
        if (line[0] == '#')
        {
            continue;
        }
        std::istringstream iss(line);
        std::string token;

        std::getline(iss, token, ','); // přeskočit string s názvem typu simulace

        std::getline(iss, token, ','); // typ simulace
        Simulation::simType key = (Simulation::simType)  std::stoi(token);

        if (plotableResults.find(key) == plotableResults.end())
        {
            plotableResults[key] = resultsType();
        }

        std::getline(iss, token, ','); // side
        int side = std::stoi(token);
        std::getline(iss, token, ','); // neighbours
        int neigh = std::stoi(token);
        std::getline(iss, token, ','); // constraints
        float cons = std::stof(token);
        std::getline(iss, token, ','); // reuslt
        float result = std::stof(token);
        // std::cout << std::endl;
        // plotableResults[key] = value;
        auto& [s, n, c, r] = plotableResults[key];

        s.push_back(side);
        n.push_back(neigh);
        // c.push_back(computeConditions(side, neigh));
        c.push_back(cons);
        r.push_back(result);

        // std::cout << key << " " << vs << " " << vn << " " << vr << std::endl;
    }
}

/**
 * @brief Kompletní GUI a logika pro bechnmark obrazovku.
 * 
 */
void GLEngine::guiBenchScreen()
{
    static bool sBenchIsRunning = false;

    ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(40.0f, 10.0f));
    ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0.0f, 0.0f));
    // ImGui::PushStyleVar(ImGuiStyleVar_IndentSpacing, ImVec2(20.0f, 0.0f));

    /// Horní menu
    if (ImGui::BeginMainMenuBar())
    {
        // ImGui::Dummy(ImVec2(40.0f, 0));
        ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(10.0f, 0.0f));
        if (ImGui::BeginMenu("MENU"))
        {
            if (ImGui::MenuItem("Main Screen"))
            {
                mProgramState = eMainScreen;
            }
            if (ImGui::MenuItem("Develop Screen"))
            {
                mProgramState = eDevelScreen;
            }
            ImGui::Text("");
            if (ImGui::MenuItem("Load Last Save"))
            {
                loadState(eLastSlot);
            }
            ImGui::Text("");
            if (ImGui::MenuItem("Load Quick Save 1"))
            {
                loadState(eQuickSlot1);
            }
            if (ImGui::MenuItem("Load Quick Save 2"))
            {
                loadState(eQuickSlot2);
            }
            if (ImGui::MenuItem("Load Quick Save 3"))
            {
                loadState(eQuickSlot3);
            }
            ImGui::Text("");
            if (ImGui::MenuItem("Save Quick Save 1"))
            {
                saveState(eQuickSlot1);
            }
            if (ImGui::MenuItem("Save Quick Save 2"))
            {
                saveState(eQuickSlot2);
            }
            if (ImGui::MenuItem("Save Quick Save 3"))
            {
                saveState(eQuickSlot3);
            }
            ImGui::EndMenu();
        }
        ImGui::PopStyleVar();

        if (grayBlueButton("Benchmark", mShowSimulationWindow))
        {
        }
        if (grayBlueButton("Výsledky", mShowResultWindow))
        {
        }
        if (grayBlueButton("Statistiky", mShowStatWindow))
        {
        }
        if (grayBlueButton("Kamera", mShowCameraWindow))
        {
        }

        ImGui::EndMainMenuBar();
    }
    ImGui::PopStyleVar(2);

    /// Okno benchmark
    if (mShowSimulationWindow)
    {
        ImGui::Begin("Benchmark", &mShowSimulationWindow);

        static bool sFirstRun = true;
        static int sWatchdogMaxTime = 10;
        static Timer sBenchWatchDog;
        static std::vector<char> sChecker;

        static int sCubeSizeLow = 2;
        static int sCubeSizeHigh = 16;

        static int sNeighboursStyle = 0;

        static bool sAppendResults = false;

        if (ImGui::Button("SPUSŤ BENCHMARK"))
        {
            mInitSettCopy.mInitialSpeed = FloatV(0.1, 0, -0.05);
            // mLiveSettCopy.mGravityAccell = FloatV(0,0,0);

            mLiveSettCopy.mSubsteps = 2;
            mLiveSettCopy.mSpacing = 0.02;
            mInitSettCopy.mEndTime = 5;
            // benchScript.push_back(bSType(2, 2, Simulation::eSimCPU));

            simTypes.clear();
            for (int i = Simulation::eSimCPU; i < Registrator::modulesCount(); ++i)
            {
                if (sChecker[i])
                {
                    simTypes.push_back(static_cast<Simulation::simType>(i));
                }
            }

            cubeSizes.clear();
            for (int i = sCubeSizeLow; i <= sCubeSizeHigh; i += 1)
            {
                cubeSizes.push_back(i);
            }

            benchScript.clear();
            for (auto cSize : cubeSizes)
            {
                if (sNeighboursStyle == 0)
                {
                    for (int i = neighboursInitial; i <= cSize - 1; i += 1)
                    {
                        for (auto simType : simTypes)
                        {
                            benchScript.push_back(bSType(cSize, i, simType));
                        }
                    }
                }
                if (sNeighboursStyle == 1)
                {
                    for (auto simType : simTypes)
                    {
                        benchScript.push_back(bSType(cSize, 1, simType));
                    }
                }
                if (sNeighboursStyle == 2)
                {
                    for (auto simType : simTypes)
                    {
                        benchScript.push_back(bSType(cSize, cSize - 1, simType));
                    }
                }
                if (sNeighboursStyle == 3)
                {
                    for (auto simType : simTypes)
                    {
                        benchScript.push_back(bSType(cSize, 1, simType));
                        if(cSize > 3)
                            benchScript.push_back(bSType(cSize, (cSize - 1) / 2, simType));
                        if(cSize > 2)
                            benchScript.push_back(bSType(cSize, cSize - 1, simType));
                    }
                }
            }
            std::reverse(benchScript.begin(), benchScript.end());
            sBenchIsRunning = true;
            sFirstRun = true;
            initialBenchScriptSize = benchScript.size();

            plotableResults.clear();
            for (auto simType : simTypes)
            {
                plotableResults[simType] = resultsType();
            }
        }

        ImGui::SameLine();
        if (ImGui::Button("Zastav benchmark"))
        {
            sBenchIsRunning = false;
            stopSims();
        }

        ImGui::Checkbox("Přidej k poslednímu měření", &sAppendResults);

        if (sBenchIsRunning)
        {
            ImGui::BeginDisabled();
        }
        ImGui::Text("");
        ImGui::Text("Maximální trvání jednoho testu");
        ImGui::PushID(0);
        ImGui::InputInt("sekund", &sWatchdogMaxTime);
        ImGui::PopID();
        ImGui::Text("");
        if (sBenchIsRunning)
        {
            ImGui::EndDisabled();
        }

        if (!sBenchIsRunning)
        {
            ImGui::Text("Velikost hrany kostky");
            ImGui::SliderInt("Do", &sCubeSizeHigh, 2, 64);
            ImGui::SliderInt("Od", &sCubeSizeLow, 2, sCubeSizeHigh);

            ImGui::Text("");
            ImGui::Text("Styl sousedů");
            ImGui::RadioButton("Všechny počty", &sNeighboursStyle, 0);
            ImGui::RadioButton("Pouze 1 soused", &sNeighboursStyle, 1);
            ImGui::RadioButton("Pouze všichni sousedé", &sNeighboursStyle, 2);
            ImGui::RadioButton("1 + Půlka + Všichni", &sNeighboursStyle, 3);

            ImGui::Text("");
            if (ImGui::Button("Označ vše"))
            {
                for (int i = Simulation::eSimCPU; i < Registrator::modulesCount(); ++i)
                {
                    sChecker[i] = true;
                }
            }
            ImGui::SameLine();
            if (ImGui::Button("Odznač vše"))
            {
                for (int i = Simulation::eSimCPU; i < Registrator::modulesCount(); ++i)
                {
                    sChecker[i] = false;
                }
            }
            if (sChecker.size() != Registrator::modulesCount())
            {
                sChecker.resize(Registrator::modulesCount(), true);
            }
            for (int i = Simulation::eSimCPU; i < Registrator::modulesCount(); ++i)
            {
                ImGui::Checkbox(Registrator::modulesNames()[i], reinterpret_cast<bool*>(&sChecker[i]));
            }
        }

        if (sBenchIsRunning && !mSim->mThreadIsRunning)
        {
            if (!sFirstRun)
            {
                sBenchWatchDog.stop();

                float avg = 0;
                for (auto a : mSim->mStorage->getStats())
                {
                    avg += a;
                }
                avg /= mSim->mFrame;

                auto& [size, neigh, cond, res] = plotableResults[mSimSelect];
                size.push_back(mInitSettCopy.mCubeSide);
                neigh.push_back(mLiveSettCopy.mParticleNeighbours);
                cond.push_back(computeConditions(mInitSettCopy.mCubeSide, mLiveSettCopy.mParticleNeighbours));
                res.push_back(avg);

                printgreen("Sim: %s,\t cube: %d,\t neighbours %d,\t Avg: %f ms, conditions %d\n", Registrator::modulesNames()[mSimSelect], mInitSettCopy.mCubeSide, mLiveSettCopy.mParticleNeighbours, avg, computeConditions(mInitSettCopy.mCubeSide, mLiveSettCopy.mParticleNeighbours));

                /// Zde skript benchmarku končí
                if (benchScript.empty())
                {
                    sBenchIsRunning = false;
                    for (auto& [k, v] : plotableResults)
                    {
                        auto& [s, n, c, r] = v;
                        for (int i = 0; i < r.size(); ++i)
                        {
                            printgreen("Sim: %s,\t cube: %d,\t neighbours %d,\t Avg: %f ms, conditions %d\n", Registrator::modulesNames()[k], s[i], n[i], r[i], c[i]);
                        }
                    }
                    saveToCSV("results.csv", sAppendResults);
                }
            }
            else
            {
                sFirstRun = false;
            }

            if (!benchScript.empty())
            {
                auto [s, n, t] = benchScript.back();
                mInitSettCopy.mCubeSide = s;
                mLiveSettCopy.mParticleNeighbours = n;
                mSimSelect = t;
                restartSims();
                benchScript.pop_back();
                sBenchWatchDog.start();
            }
        }

        if (sBenchIsRunning && mSim->mThreadIsRunning)
        {
            sBenchWatchDog.stop();
            float seconds = sBenchWatchDog.s();
            // printblue("%f\t", seconds);
            if (seconds > sWatchdogMaxTime)
            {
                printdyellow("\t killing by watchdog\n");
                mSim->asyncStop();
            }

            ImGui::ProgressBar((float(initialBenchScriptSize) - benchScript.size()) / initialBenchScriptSize, ImVec2(0.0f, 0.0f));
            ImGui::SameLine(0.0f, ImGui::GetStyle().ItemInnerSpacing.x);
            ImGui::Text("Postup");
            ImGui::Text("");

            int leftTime = (initialBenchScriptSize * sWatchdogMaxTime) - ((initialBenchScriptSize - benchScript.size()) * sWatchdogMaxTime);
            ImGui::Text("Maximální zbývající čas: %d:%.2d", leftTime / 60, leftTime % 60);
            ImGui::Text("");

            ImGui::BeginDisabled();
            if (ImGui::BeginCombo("Implementace", Registrator::modulesNames()[mSimSelect]))
            {
                ImGui::SetItemDefaultFocus();
                ImGui::EndCombo();
            }
            ImGui::SliderInt("Kostka", &mInitSettCopy.mCubeSide, cubeSizes.front(), cubeSizes.back());
            ImGui::SliderInt("Sousedů", &mLiveSettCopy.mParticleNeighbours, neighboursInitial, mInitSettCopy.mCubeSide);
            ImGui::Text("");
            ImGui::EndDisabled();

            ImGui::Text("Běží: %d %d %s", mInitSettCopy.mCubeSide, mLiveSettCopy.mParticleNeighbours, Registrator::modulesNames()[mSimSelect]);
            for (int i = benchScript.size() - 1; i >= 0; --i)
            {
                auto [s, n, t] = benchScript[i];
                ImGui::Text("--> %d %d %s", s, n, Registrator::modulesNames()[t]);
            }
            // benchWatchDog.start();
        }
        ImGui::End();
    }

    /// Okno výsledků / grafů
    if (mShowResultWindow)
    {
        static bool sLogScale = false;
        static bool sAutofit = true;

        static int sGraphType = 0;

        static bool sFilterEnabled = false;
        static std::set<std::pair<int, bool>> sFilter;
        static std::unordered_map<Simulation::simType, resultsType> sFilteredResults;
        static std::unordered_map<Simulation::simType, resultsType> * sToPlotResults = &plotableResults;

        //graf zrychlení
        static Simulation::simType sBaseSim;

        //heatmap
        static bool sGlobalMax = false;
        static int sHeatMapMaxSide = 0;
        // static int heatMapMinSide = 0;
        static int sHeatMapMaxNeigh = 0;
        // static int heatMapMinNeigh = 0;
        static int sMapRows = 0;
        static int sMapCols = 0;
        static std::vector<float> sValuesForHeatMap;
        static ImPlotColormap sMapColor = ImPlotColormap_Jet;
        static float sMinResVal = 0;
        static float sMaxResVal = 0;
        static std::vector<const char *> sXLabels;
        static std::vector<const char *> sYLabels;


        ImGui::Begin("Graf", &mShowResultWindow);

        if (sBenchIsRunning)
        {
            ImGui::BeginDisabled();
        }
        if (ImGui::Button("Načti výsledky z posledního běhu"))
        {
            plotableResults.clear();
            loadFromCSV("results.csv");
        }
        if (sBenchIsRunning)
        {
            ImGui::EndDisabled();
        }

        ImGui::Text("");

        ImGui::RadioButton("Základní", &sGraphType, 0);
        ImGui::SameLine();
        ImGui::RadioButton("Zrychlení", &sGraphType, 1);
        ImGui::SameLine();
        ImGui::RadioButton("Tepelná mapa", &sGraphType, 2);

        ImGui::Text("");

        if (sGraphType == 0)
        {
            ImGui::Checkbox("Automatické měřítko", &sAutofit);
            ImGui::Checkbox("Logaritmická škála", &sLogScale);
        }
        if (sGraphType == 1)
        {
            ImGui::Checkbox("Automatické měřítko", &sAutofit);
            ImGui::Checkbox("Logaritmická škála", &sLogScale);
            ImGui::Text("");

            static int sSelector = -1;

            if (sSelector == -1)
                for (auto& [k, v] : plotableResults)
                {
                    sSelector = k;
                    break;
                }

            for (auto& [k, v] : plotableResults)
            {
                if (k >= Simulation::eSimCPU)
                {
                    ImGui::RadioButton(Registrator::modulesNames()[k], &sSelector, k);
                }
            }
            sBaseSim = static_cast<Simulation::simType>(sSelector);
        }

        if(sGraphType == 0 || sGraphType == 1)
        {
            ImGui::Checkbox("Filtrovat podle počtu sousedů", &sFilterEnabled);
            if(sFilterEnabled){
                Simulation::simType localSelector = -1;
                for (auto& [k, v] : plotableResults)
                {
                    localSelector = k;
                    break;
                }
                auto& [s, n, c, r] = plotableResults[localSelector];
                std::set<int> tempSet(n.begin(), n.end());
                if(sFilter.size() != tempSet.size()){
                    sFilter.clear();
                    for(auto a : tempSet){
                        sFilter.insert(std::make_pair(a, true));
                    }
                }

                ImGui::Text("");

                for(auto& [num, check] : sFilter){
                    ImGui::Checkbox(std::to_string(num).c_str(), &check);
                }

                for(auto& [k,v] : plotableResults){
                    auto& [s, n, c, r] = v;
                    resultsType temp;
                    auto& [sTemp, nTemp, cTemp, rTemp] = temp;
                    for(int i = 0; i < n.size(); ++i){
                        if(sFilter.find(std::pair<int, bool>(n[i], true)) != sFilter.end())
                        {
                            sTemp.push_back(s[i]);
                            nTemp.push_back(n[i]);
                            cTemp.push_back(c[i]);
                            rTemp.push_back(r[i]);
                        }
                    }
                    sFilteredResults[k] = temp;
                }

            }
        }

        if (sGraphType == 2)
        {
            static int sSelector = -1;
            static int sSelectorLast = -1;

            if (sSelector == -1)
                for (auto& [k, v] : plotableResults)
                {
                    sSelector = k;
                    break;
                }

            ImGui::Checkbox("Globální maximum", &sGlobalMax);

            for (auto& [k, v] : plotableResults)
            {
                if (k >= Simulation::eSimCPU)
                {
                    ImGui::RadioButton(Registrator::modulesNames()[k], &sSelector, k);
                }
            }

            if (ImPlot::ColormapButton(ImPlot::GetColormapName(sMapColor), ImVec2(225, 0), sMapColor))
            {
                sMapColor = (sMapColor + 1) % ImPlot::GetColormapCount();
                ImPlot::BustColorCache("##Heatmap1");
            }

            ImGui::Text("Čím více vpravo na barevné škále,\ntím rychlejší výpočet\n(spočtených omezení / ms)");

            if (sSelector != sSelectorLast)
            {
                sSelectorLast = sSelector;
                auto& [s, n, c, r] = plotableResults[static_cast<Simulation::simType>(sSelector)];
                sValuesForHeatMap.clear();
                int max = 0;
                int min = 1000000;
                for (auto a : s)
                {
                    if (a > max)
                    {
                        max = a;
                    }
                    if (a < min)
                    {
                        min = a;
                    }
                }
                sHeatMapMaxSide = max;
                // heatMapMinSide = min;
                sHeatMapMaxNeigh = max;
                // heatMapMinNeigh = 1;
                sMapRows = sHeatMapMaxSide + 1;// - heatMapMinSide + 1;
                sMapCols = sHeatMapMaxNeigh + 1;// - heatMapMinNeigh;
                int size = sMapRows * sMapCols;
                sValuesForHeatMap.resize(size, 0);
                for (int i = 0; i < s.size(); ++i)
                {
                    int idx = (sHeatMapMaxSide - s[i]) * sMapCols + (n[i]);
                    if (idx >= sValuesForHeatMap.size())
                    {
                        QTError("Index out of range");
                    }
                    // valuesForHeatMap[idx] = r[i];
                    sValuesForHeatMap[idx] = c[i] / r[i]; // Počet spočtených omezení za ms
                }
                static std::vector<std::string> sXLabelsStore;
                static std::vector<std::string> sYLabelsStore;
                sXLabelsStore.clear();
                sYLabelsStore.clear();
                sXLabels.clear();
                sYLabels.clear();
                for (int i = 0; i <= sHeatMapMaxNeigh; ++i)
                {
                    sXLabelsStore.push_back(std::to_string(i));
                }
                for (int i = 0; i <= sHeatMapMaxSide; ++i)
                {
                    sYLabelsStore.push_back(std::to_string(i));
                }
                for (auto& a : sXLabelsStore)
                {
                    sXLabels.push_back(a.c_str());
                }
                for (auto& a : sYLabelsStore)
                {
                    sYLabels.push_back(a.c_str());
                }

                if(!sGlobalMax){
                    sMaxResVal = 0;
                } else {
                    for(auto & a : plotableResults){
                        auto & [s, n, c, r] = a.second;
                        for(int i = 0; i < c.size(); ++i){
                            if(c[i] / r[i] > sMaxResVal){
                                sMaxResVal = c[i] / r[i];
                            }
                        }
                    }
                }
            }

        }

        ImGui::End();
        ImGui::Begin("Výsledky", &mShowResultWindow);


        if(sFilterEnabled)
        {
            sToPlotResults = &sFilteredResults;
        }
        else
        {
            sToPlotResults = &plotableResults;
        }


        if (sGraphType == 0)
        {
            if (ImPlot::BeginPlot("Line Plots", ImVec2(-FLT_MIN, -FLT_MIN)))
            {
                ImPlotAxisFlags axisFlags = ImPlotAxisFlags_None;
                if (sLogScale)
                {
                    ImPlot::SetupAxisScale(ImAxis_X1, ImPlotScale_Log10);
                    ImPlot::SetupAxisScale(ImAxis_Y1, ImPlotScale_Log10);
                }
                if (sAutofit)
                {
                    axisFlags = ImPlotAxisFlags_AutoFit;// | ImPlotAxisFlags_RangeFit;
                }
                ImPlot::SetupAxes("Počet omezení částic", "Doba výpočtu kroku v ms", axisFlags, axisFlags);

                ImPlotMarker marker = ImPlotMarker_Circle;

                for (auto& [k, v] : (*sToPlotResults))
                {
                    auto& [s, n, c, r] = v;
                    std::string name = std::string(Registrator::modulesNames()[k]);
                    ImPlot::SetNextMarkerStyle((++marker / 5) % ImPlotMarker_COUNT);
                    ImPlot::PlotLine(name.c_str(), c.data(), r.data(), r.size());
                    // ImPlot::PlotScatter(name.c_str(), c.data(), r.data(), r.size());
                }
                ImPlot::EndPlot();
            }
        }

        if (sGraphType == 1)
        {
            if (ImPlot::BeginPlot("Line Plots Accel", ImVec2(-FLT_MIN, -FLT_MIN)))
            {
                ImPlotAxisFlags axisFlags = ImPlotAxisFlags_None;
                if (sLogScale)
                {
                    ImPlot::SetupAxisScale(ImAxis_X1, ImPlotScale_Log10);
                    ImPlot::SetupAxisScale(ImAxis_Y1, ImPlotScale_Log10);
                }
                if (sAutofit)
                {
                    axisFlags = ImPlotAxisFlags_AutoFit;// | ImPlotAxisFlags_RangeFit;
                }
                ImPlot::SetupAxes("Počet omezení částic", "Zrychlení (y-krát)", axisFlags, axisFlags);

                auto& [__unused1, __unused2, __unused3, baseRes] = (*sToPlotResults)[sBaseSim];

                ImPlotMarker marker = ImPlotMarker_Circle;
                for (auto& [k, v] : *sToPlotResults)
                {
                    auto& [s, n, c, r] = v;
                    std::string name = std::string(Registrator::modulesNames()[k]);

                    std::vector<float> basedResults; //TODO doělat že se bude přepočítávat jen když se změní baseSim nebo velikost vektoru
                    if (k == sBaseSim)
                    {
                        basedResults.resize(r.size(), 1);
                    }
                    else
                    {
                        for (int i = 0; i < r.size(); ++i)
                        {
                            basedResults.push_back((1.0 / r[i]) / (1.0 / baseRes[i]));
                        }
                    }
                    ImPlot::SetNextMarkerStyle((++marker / 5) % ImPlotMarker_COUNT);
                    ImPlot::PlotLine(name.c_str(), c.data(), basedResults.data(), basedResults.size());
                }
                ImPlot::EndPlot();
            }
        }

        if (sGraphType == 2)
        {
            if (sValuesForHeatMap.size() > 0)
            {
                for(auto& a : sValuesForHeatMap)
                {
                    if(isnan(a))
                    {
                        a = 0;
                    }
                }
                ImPlot::PushColormap(sMapColor);
                if (ImPlot::BeginPlot("##Heatmap1", ImVec2(-FLT_MIN, -FLT_MIN), ImPlotFlags_NoLegend | ImPlotFlags_NoMouseText))
                {
                    ImPlotAxisFlags axes_flags = ImPlotAxisFlags_NoGridLines | ImPlotAxisFlags_NoTickMarks;
                    ImPlot::SetupAxes("Počet sousedů", "Velikost hrany kostky", axes_flags, axes_flags);
                    ImPlot::SetupAxisTicks(ImAxis_X1, 0.5, sHeatMapMaxNeigh - 0.5, sMapRows, sXLabels.data());
                    ImPlot::SetupAxisTicks(ImAxis_Y1, 0.5, sHeatMapMaxSide - 0.5, sMapCols, sYLabels.data());
                    ImPlot::PlotHeatmap("heat", sValuesForHeatMap.data(), sMapRows, sMapCols, sMinResVal, sMaxResVal, "%.0e", ImPlotPoint(0, 0), ImPlotPoint(sHeatMapMaxNeigh, sHeatMapMaxSide), 0);
                    ImPlot::EndPlot();
                }
            }
        }
        ImGui::End();
    }

    showStatWindow(false);
    showCameraWindow();
}
