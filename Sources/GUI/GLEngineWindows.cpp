/**
 * @file      GLEngineWindows.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Soubor implementující třídu GLEngine.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include "GLEngine.h"

/**
 * @brief Zobrazí okno s nastavením kamery
 */
void GLEngine::showCameraWindow()
{
    if (mShowCameraWindow)
    {
        ImGui::Begin("Kamera", &mShowCameraWindow);

        ImGui::SliderFloat("Velikost bodů", &mPointSize, 1.0f, 8.0f);
        ImGui::Text("");

        ImGui::Checkbox("Použít výchozí barvu", &mRederDefaultSimColors);
        if(!mRederDefaultSimColors)
            ImGui::BeginDisabled();
        ImGui::Text("Barva bodů");
        ImGui::SameLine();
        ImGui::ColorEdit4("Barva", mDefaultColor, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoLabel);
        if(!mRederDefaultSimColors)
            ImGui::EndDisabled();

        ImGui::Text("");
        ImGui::SliderFloat("Velikost bodů 2", &mSecondaryPointSize, 1.0f, 8.0f);

        ImGui::Checkbox("Použít výchozí barvu 2", &mRederDefaultSecondarySimColors);
        if(!mRederDefaultSecondarySimColors)
            ImGui::BeginDisabled();
        ImGui::Text("Barva bodů 2");
        ImGui::SameLine();
        ImGui::ColorEdit4("Barva 2", mSecondaryDefaultColor, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoLabel);
        if(!mRederDefaultSecondarySimColors)
            ImGui::EndDisabled();



        ImGui::Text("");
        ImGui::Text("");

        ImGui::Checkbox("Ortogonální projekce", &mCam.mOrthographicProjection);
        
        ImGui::SliderFloat("Fov", &mCam.mFOV, 10.f, 100.0f);

        ImGui::SliderFloat("kamera azimut", &mCam.mYaw, -180.0f, 180.0f);
        ImGui::SliderFloat("kamera náklon", &mCam.mPitch, -89.0f, 89.0f);
        ImGui::SliderFloat("kamera vzdálenost", &mCam.mDistance, 0.1f, 100.0f);

        ImGui::SliderFloat("cíl pohledu x", &mCam.mLookAtX, 0.0f, 1.0f);
        ImGui::SliderFloat("cíl pohledu y", &mCam.mLookAtY, 0.0f, 1.0f);
        ImGui::SliderFloat("cíl pohledu z", &mCam.mLookAtZ, 0.0f, 1.0f);

        ImGui::End();
    }
}

/**
 * @brief Zobrazí okno se předběžnými statistikami
 */
void GLEngine::showStatWindow(bool secondToo)
{
    if (mShowStatWindow)
    {
        ImGui::Begin("Statistiky", &mShowStatWindow);
        // ImGui::Text("------------Test------------");
        ImGui::Text("Program    %.3f ms (%.1f FPS)", 1000.0f / mIO->Framerate, mIO->Framerate);
        ImGui::Text("Vykreslení %.3f ms (%.1f FPS)", mRednerTimer.ms(), 1000.0f /  mRednerTimer.ms());

        ImGui::Text("");

        ImGui::Text("Simulace   %.3f ms (%.1f SPS)", mSim->mStepTimeMS, 1000.0f /  mSim->mStepTimeMS);

        {
            float average = 0.0f;
            auto data = mSim->mStorage->getStats();
            for (int n = 0; n < data.size(); n++)
            {
                average += data[n];
            }
            average /= (float)data.size();
            char overlay[32];
            sprintf(overlay, "avg %f", average);
            ImGui::PlotLines("", data.data(), data.size(), 0, overlay, average - average * 2.0, average * 4.0, ImVec2(-FLT_MIN, 8 * ImGui::GetTextLineHeightWithSpacing()));
        }

        if (secondToo)
        {
            ImGui::Text("Simulace2  %.3f ms (%.1f SPS)", mSecondarySim->mStepTimeMS, 1000.0f /  mSecondarySim->mStepTimeMS);
            float average = 0.0f;
            auto data = mSecondarySim->mStorage->getStats();
            for (int n = 0; n < data.size(); n++)
            {
                average += data[n];
            }
            average /= (float)data.size();
            char overlay[32];
            sprintf(overlay, "avg %f", average);
            ImGui::PlotLines("", data.data(), data.size(), 0, overlay, average - average * 2.0, average * 4.0, ImVec2(-FLT_MIN, 8 * ImGui::GetTextLineHeightWithSpacing()));
        }

        ImGui::End();
    }
}

/**
 * @brief Zobrazí okno s zobrazovačem hodnot ze simulací
 */
void GLEngine::showEditorWindow()
{
    if (mShowEditorWindow)
    {
        constexpr float maxErr = 5.0e-2;
        int side = mInitSettCopy.mCubeSide;
        static bool sUseAxeZ = false;
        static int sAxeZ = 0;
        static int sShowType = 0;

        ImGui::RadioButton("Sim1", &sShowType, 0);
        ImGui::SameLine();
        ImGui::RadioButton("Sim2", &sShowType, 1);
        ImGui::SameLine();
        ImGui::RadioButton("Abs Diff", &sShowType, 2);

        std::vector<FloatV> pos;
        std::vector<FloatV> pos2;

        auto [frame, positions, velocities] = mSim->mStorage->lockLatestFrame();
        pos = positions;
        mSim->mStorage->unLockLatestFrame();

        auto [frame2, positions2, velocities2] = mSecondarySim->mStorage->lockLatestFrame();
        pos2 = positions2;
        mSecondarySim->mStorage->unLockLatestFrame();

        ImGui::Checkbox("Použít osu Z", &sUseAxeZ);
        if (sUseAxeZ)
        {
            ImGui::SliderInt("Posun Osy Z", &sAxeZ, 0, side - 1);
        }

        // static int lines = 15;
        // ImGui::SliderInt("Lines", &lines, 1, 15);
        ImGui::PushStyleVar(ImGuiStyleVar_FrameRounding, 3.0f);
        ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(2.0f, 1.0f));
        ImVec2 scrolling_child_size = ImVec2(-FLT_MIN, -FLT_MIN);
        ImGui::BeginChild("scrolling", scrolling_child_size, ImGuiChildFlags_Border, ImGuiWindowFlags_HorizontalScrollbar);

        auto zobraz = [&](int i)
        {
            std::string label = "";
            float hue = i * 1.0 / (side * side * side);
            ImVec4 color = (ImVec4)ImColor::HSV(hue, 0.8f, 0.8f);

            if (sShowType == 0)
            {
                if (i >= pos.size())
                {
                    return true;
                }
                label = std::to_string(pos[i].mX) + "\n" + std::to_string(pos[i].mY) + "\n" + std::to_string(pos[i].mZ);
            }
            if (sShowType == 1)
            {
                if (i >= pos2.size())
                {
                    return true;
                }
                label = std::to_string(pos2[i].mX) + "\n" + std::to_string(pos2[i].mY) + "\n" + std::to_string(pos2[i].mZ);
            }
            if (sShowType == 2)
            {
                if (i >= pos.size() || i >= pos2.size())
                {
                    return true;
                }
                float x = abs(pos[i].mX - pos2[i].mX);
                float y = abs(pos[i].mY - pos2[i].mY);
                float z = abs(pos[i].mZ - pos2[i].mZ);
                label = std::to_string(x) + "\n" + std::to_string(y) + "\n" + std::to_string(z);
                float err = x + y + z;
                hue = (err / maxErr) <= 1.0 ? 0.35 - (err / maxErr) * 0.35 : 0.0;
                color = (ImVec4)ImColor::HSV(hue, 0.8f, 0.8f);
            }
            if (i % side != 0)
            {
                ImGui::SameLine();
            }

            ImGui::PushID(i);

            ImGui::PushStyleColor(ImGuiCol_Button, color);
            ImGui::PushStyleColor(ImGuiCol_ButtonHovered, color);
            ImGui::PushStyleColor(ImGuiCol_ButtonActive, color);

            ImGui::Button(label.c_str(), ImVec2(42.0f, 42.0f));

            ImGui::PopStyleColor(3);
            ImGui::PopID();
            return false;
        };

        if (sUseAxeZ)
        {
            for (int i = sAxeZ * side * side; i < side * side + sAxeZ * side * side; i++)
            {
                if (zobraz(i))
                {
                    continue;
                }
            }
        }
        else
        {
            for (int i = 0; i < side * side * side; i++)
            {
                if (i % (side * side) == 0)
                {
                    ImGui::Text("Z: %d", i / (side * side));
                }
                if (zobraz(i))
                {
                    continue;
                }
            }
        }

        ImGui::EndChild();
        ImGui::PopStyleVar(2);

    }
}
