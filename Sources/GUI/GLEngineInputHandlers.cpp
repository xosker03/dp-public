/**
 * @file      GLEngineInputHandlers.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Soubor implementující třídu GLEngine.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include "GLEngine.h"

// Zpracování chyb z OpenGL
void error_callback(int error, const char* description)
{
    fprintf(stderr, "Error: %s\n", description);
}

// Zpracování vstupů klávesnice
void keyCallbackStatic(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
    {
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }
    engine->keyCallback(window, key, scancode, action, mods);
}

void mouseCallbackStatic(GLFWwindow* window, double xpos, double ypos){
    engine->mouseCallback(window, xpos, ypos);
}

void mouseButtonCallbackStatic(GLFWwindow* window, int button, int action, int mods){
    engine->mouseButtonCallback(window, button, action, mods);
}
void scrollCallbackStatic(GLFWwindow* window, double xoffset, double yoffset){
    engine->scrollCallback(window, xoffset, yoffset);
}

// Zpracování změn velikosti okna
void framebufferSizeCallbackStatic(GLFWwindow* window, int width, int height)
{
    engine->framebufferSizeCallback(window, width, height);
}

//Zpracování ladících zpráv z OpenGL https://learnopengl.com/In-Practice/Debugging
void APIENTRY debugCallback(GLenum source,
                            GLenum type,
                            GLuint id,
                            GLenum severity,
                            GLsizei length,
                            const GLchar *message,
                            const void *userParam)
{
    if (id == 131169 || id == 131185 || id == 131218 || id == 131204)
    {
        return;
    }

    std::cout << "---------------" << std::endl;
    std::cout << "Debug message (" << id << "): " <<  message << std::endl;

    switch (source)
    {
    case GL_DEBUG_SOURCE_API:
        std::cout << "Source: API";
        break;
    case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
        std::cout << "Source: Window System";
        break;
    case GL_DEBUG_SOURCE_SHADER_COMPILER:
        std::cout << "Source: Shader Compiler";
        break;
    case GL_DEBUG_SOURCE_THIRD_PARTY:
        std::cout << "Source: Third Party";
        break;
    case GL_DEBUG_SOURCE_APPLICATION:
        std::cout << "Source: Application";
        break;
    case GL_DEBUG_SOURCE_OTHER:
        std::cout << "Source: Other";
        break;
    }
    std::cout << std::endl;

    switch (type)
    {
    case GL_DEBUG_TYPE_ERROR:
        std::cout << "Type: Error";
        break;
    case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
        std::cout << "Type: Deprecated Behaviour";
        break;
    case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
        std::cout << "Type: Undefined Behaviour";
        break;
    case GL_DEBUG_TYPE_PORTABILITY:
        std::cout << "Type: Portability";
        break;
    case GL_DEBUG_TYPE_PERFORMANCE:
        std::cout << "Type: Performance";
        break;
    case GL_DEBUG_TYPE_MARKER:
        std::cout << "Type: Marker";
        break;
    case GL_DEBUG_TYPE_PUSH_GROUP:
        std::cout << "Type: Push Group";
        break;
    case GL_DEBUG_TYPE_POP_GROUP:
        std::cout << "Type: Pop Group";
        break;
    case GL_DEBUG_TYPE_OTHER:
        std::cout << "Type: Other";
        break;
    }
    std::cout << std::endl;

    switch (severity)
    {
    case GL_DEBUG_SEVERITY_HIGH:
        std::cout << "Severity: high";
        break;
    case GL_DEBUG_SEVERITY_MEDIUM:
        std::cout << "Severity: medium";
        break;
    case GL_DEBUG_SEVERITY_LOW:
        std::cout << "Severity: low";
        break;
    case GL_DEBUG_SEVERITY_NOTIFICATION:
        std::cout << "Severity: notification";
        break;
    }
    std::cout << std::endl;
    std::cout << std::endl;
}

void GLEngine::framebufferSizeCallback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);

    float ratio = (float) width / (float) height;
    sProj = glm::perspective(glm::radians(60.f), ratio, .1f, 1000.f);
    sView = glm::lookAt(glm::vec3(2.f, 2.f, 3.f), glm::vec3(0.f), glm::vec3(0.f, 1.f, 0.f));

    mDisplayW = width;
    mDisplayH = height;
}

void GLEngine::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    // if (key == GLFW_KEY_SPACE && action == GLFW_PRESS)
    // {
    //     mSim->mSimIsRunning = !mSim->mSimIsRunning;
    //     printif(mSim->mSimIsRunning, "[%c] Pause\n", key);
    // }
    //
    // if (key == GLFW_KEY_W && action == GLFW_PRESS)
    // {
    //     mLiveSettCopy.mExternalAcceleration += FloatV(0, 2.0e0);
    //     printgreen("[%c] ext accel %s\n", key, mLiveSettCopy.mExternalAcceleration.str().c_str());
    // }
    // if (key == GLFW_KEY_A && action == GLFW_PRESS)
    // {
    //     mLiveSettCopy.mExternalAcceleration += FloatV(-2.0e0, 0);
    //     printgreen("[%c] ext accel %s\n", key, mLiveSettCopy.mExternalAcceleration.str().c_str());
    // }
    // if (key == GLFW_KEY_S && action == GLFW_PRESS)
    // {
    //     mLiveSettCopy.mExternalAcceleration += FloatV(0, -2.0e0);
    //     printgreen("[%c] ext accel %s\n", key, mLiveSettCopy.mExternalAcceleration.str().c_str());
    // }
    // if (key == GLFW_KEY_D && action == GLFW_PRESS)
    // {
    //     mLiveSettCopy.mExternalAcceleration += FloatV(2.0e0, 0);
    //     printgreen("[%c] ext accel %s\n", key, mLiveSettCopy.mExternalAcceleration.str().c_str());
    // }
    // if (key == GLFW_KEY_Q && action == GLFW_PRESS)
    // {
    //     mLiveSettCopy.mExternalAcceleration += FloatV(0, 0, +2.0e0);
    //     printgreen("[%c] ext accel %s\n", key, mLiveSettCopy.mExternalAcceleration.str().c_str());
    // }
    // if (key == GLFW_KEY_E && action == GLFW_PRESS)
    // {
    //     mLiveSettCopy.mExternalAcceleration += FloatV(0, 0, -2.0e0);
    //     printgreen("[%c] ext accel %s\n", key, mLiveSettCopy.mExternalAcceleration.str().c_str());
    // }
    //
    // if (key == GLFW_KEY_R && action == GLFW_PRESS)
    // {
    //     mLiveSettCopy.mExternalAcceleration = FloatV(0, 0);
    //     printgreen("[%c] ext accel %s\n", key, mLiveSettCopy.mExternalAcceleration.str().c_str());
    // }
    //
    // if (key == GLFW_KEY_X && action == GLFW_PRESS)
    // {
    //     mLiveSettCopy.mDT /= 10.0;
    //     printif(mLiveSettCopy.mDT > 9.0e-5l, "[%c] dt %e\n", key, mLiveSettCopy.mDT * 1.0000001);
    // }
    // if (key == GLFW_KEY_Z && action == GLFW_PRESS)
    // {
    //     mLiveSettCopy.mDT *= 10.0;
    //     printif(mLiveSettCopy.mDT > 9.0e-5l, "[%c] dt %e\n", key, mLiveSettCopy.mDT * 1.0000001);
    // }
    //
    // if (key == GLFW_KEY_G && action == GLFW_PRESS)
    // {
    //     if (mLiveSettCopy.mGravityEnabled)
    //     {
    //         mLiveSettCopy.mGravityEnabled = 0.0f;
    //     }
    //     else
    //     {
    //         mLiveSettCopy.mGravityEnabled = 1.0f;
    //     }
    //     printif(mLiveSettCopy.mGravityEnabled, "[%c] Gravity\n", key);
    // }
}


void GLEngine::scrollCallback(GLFWwindow* window, double xoffset, double yoffset)
{
    ImGuiIO& io = ImGui::GetIO();
    if (!io.WantCaptureMouse) {
        mCam.mDistance -= yoffset * 0.2f;
        if(mCam.mDistance < 0.1f)
            mCam.mDistance = 0.1f;
    }
}


void GLEngine::mouseButtonCallback(GLFWwindow* window, int button, int action, int mods) {
    if (button == GLFW_MOUSE_BUTTON_RIGHT) {
        if (action == GLFW_PRESS) {
            mRightMouseButtonPressed = true;
            glfwGetCursorPos(window, &mMouseLastX, &mMouseLastY);
        } else if (action == GLFW_RELEASE) {
            mRightMouseButtonPressed = false;
        }
    }
}


void GLEngine::mouseCallback(GLFWwindow* window, double xpos, double ypos) {
    ImGuiIO& io = ImGui::GetIO();
    if (mRightMouseButtonPressed && !io.WantCaptureMouse) {

        if (mFirstMouse) {
            mMouseLastX = xpos;
            mMouseLastY = ypos;
            mFirstMouse = false;
        }

        float xoffset = xpos - mMouseLastX;
        float yoffset = mMouseLastY - ypos;
        mMouseLastX = xpos;
        mMouseLastY = ypos;

        float sensitivity = 0.2f;
        xoffset *= sensitivity;
        yoffset *= sensitivity;

        // printf("xoffset: %f, yoffset: %f\n", xoffset, yoffset);
        mCam.mYaw += xoffset;
        mCam.mPitch -= yoffset;
        // printf("yaw: %f, pitch: %f\n", mCam.mYaw, mCam.mPitch);
        if(mCam.mPitch > 89.999f)
            mCam.mPitch = 89.999f;
        if(mCam.mPitch < -89.999f)
            mCam.mPitch = -89.999f;
    }
}



