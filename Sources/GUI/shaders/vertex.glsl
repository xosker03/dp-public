#version 450 core

/**
 * @file      vertex.glsl
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Soubor implementující vertex shader.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

uniform mat4 view = mat4(1);
uniform mat4 proj = mat4(1);
uniform bool box = false;
uniform vec3 defaultColor = vec3(0.0f, 0.0f, 1.0f);
uniform bool useDefaultColor = true;

layout(location = 0) in vec3 aPos;
layout(location = 1) in vec3 color;

out vec4 colorF; // barva pro fragment shader

void main()
{
    gl_Position = proj * view * vec4(aPos.x, aPos.y, aPos.z, 1.0);
    if (box)
    {
        colorF = vec4(color, 1.0);
    }
    else
    {
        if (useDefaultColor)
            colorF = vec4(defaultColor, 1.0f);    // výchozí barva
        else
            colorF = vec4(color, 1.0);
    }
}
