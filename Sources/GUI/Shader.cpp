/**
 * @file      Shrekder.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Soubor implementující třídu Shader.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include "Shader.h"
#include "../zaklad/TError.h"

Shader::Shader(std::string name, std::string srcDir)
{
    mProgramName = name;
    mSrcDir = srcDir;
}

Shader::~Shader()
{

}

std::string Shader::readFile(const char* file_path)
{
    std::ifstream file(file_path);
    if (!file)
    {
        // std::cerr << "Nepodařilo se otevřít soubor: " << file_path << std::endl;
        throw QTError("Shader::readFile: Nepodařilo se otevřít soubor: " + std::string(file_path));
    }

    std::stringstream buffer;
    buffer << file.rdbuf();
    return buffer.str();
}

void Shader::compile(GLenum type, std::string name, std::string file)
{
    if (file == "")
    {
        file = name + ".glsl";
    }
    file = mSrcDir + "/" + file;
    std::string data = readFile(file.c_str());
    const char* src = data.c_str();
    unsigned int shader;
    shader = glCreateShader(type);
    glShaderSource(shader, 1, &src, NULL);
    glCompileShader(shader);
    int  success;
    char infoLog[512];
    glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
    if (!success)
    {
        glGetShaderInfoLog(shader, 512, NULL, infoLog);
        std::cout << mProgramName << "::" << name << "::COMPILATION_FAILED\n";
        std::cout << infoLog << std::endl;
        throw QTError("Shader::compile: Selhala kompilace shaderu: " + mProgramName);
    }
    mShader.push_back(shader);
}

void Shader::compile_vertex()
{
    compile(GL_VERTEX_SHADER, "Vertex", "vertex.glsl");
}
void Shader::compile_fragment()
{
    compile(GL_FRAGMENT_SHADER, "Fragment", "fragment.glsl");
}

void Shader::link()
{
    mMainProgram = glCreateProgram();
    for (auto & a : mShader)
    {
        glAttachShader(mMainProgram, a);
    }
    int  success;
    char infoLog[512];
    glLinkProgram(mMainProgram);
    glGetProgramiv(mMainProgram, GL_LINK_STATUS, &success);
    if (!success)
    {
        glGetProgramInfoLog(mMainProgram, 512, NULL, infoLog);
        std::cout << mProgramName << "::LINK_FAILED\n";
        std::cout << infoLog << std::endl;
        throw QTError("Shader::link: Selhalo linkování shaderu: " + mProgramName);
    }
    for (auto & a : mShader)
    {
        glDeleteShader(a);
    }
    mShader.resize(0);
    mLinked = true;
}

void Shader::use()
{
    if (!mLinked)
    {
        std::cerr << mProgramName << "::NOT LINKED" << std::endl;
        throw QTError("Shader::use: shader " + mProgramName + " není slinkován");
    }
    glUseProgram(mMainProgram);
}
