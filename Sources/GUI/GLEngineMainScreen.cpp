/**
 * @file      GLEngineMainScreen.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Soubor implementující třidu GLEngine.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include "GLEngine.h"


void GLEngine::guiNewFrame()
{
    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
}

void GLEngine::guiRender()
{
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

/// Zde se spouští GUI a jeho logika
void GLEngine::guiUpdate()
{
    switch (mProgramState)
    {
    case eMainScreen:
    {
        guiMainScreen();
        break;
    }
    case eDevelScreen:
    {
        guiDevelScreen();
        break;
    }
    case eBenchScreen:
    {
        guiBenchScreen();
        break;
    }
    default:
    {
        guiMainScreen();
        break;
    }
    }

}

/**
 * @brief Hlavní obrazovka
 * 
 */
void GLEngine::guiMainScreen()
{
    // Nastavte pozici a velikost okna
    ImGui::SetNextWindowPos(ImVec2(0, 0));
    ImGui::SetNextWindowSize(ImVec2(ImGui::GetIO().DisplaySize.x, ImGui::GetIO().DisplaySize.y));

    // Vytvořte okno
    ImGui::Begin("Main Screen", nullptr, ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove);

    // Nastavte velikost tlačítka
    ImVec2 buttonSize = ImVec2(200, 50);

    ImGui::Dummy(ImVec2(0.0f, 40.0f));

    // Vytvořte neviditelný prvek s šířkou tlačítka
    ImGui::Dummy(ImVec2(buttonSize.x, 0));
    ImGui::SameLine();
    if (ImGui::Button("Benchmark", buttonSize))
    {
        mProgramState = eBenchScreen;
        mShowStatWindow = false;
    }

    ImGui::Dummy(ImVec2(0.0f, 40.0f));

    ImGui::Dummy(ImVec2(buttonSize.x, 0));
    ImGui::SameLine();
    if (ImGui::Button("Develop", buttonSize))
    {
        mProgramState = eDevelScreen;
        mShowStatWindow = true;
    }

    ImGui::Dummy(ImVec2(0.0f, 80.0f));

    ImGui::Dummy(ImVec2(buttonSize.x, 0));
    ImGui::SameLine();
    if (ImGui::Button("Last", buttonSize))
    {
        loadState(eLastSlot);
    }

    ImGui::Dummy(ImVec2(0.0f, 80.0f));

    ImGui::Dummy(ImVec2(buttonSize.x, 0));
    ImGui::SameLine();
    if (ImGui::Button("Quick Save 1", buttonSize))
    {
        loadState(eQuickSlot1);
    }

    ImGui::Dummy(ImVec2(0.0f, 40.0f));

    ImGui::Dummy(ImVec2(buttonSize.x, 0));
    ImGui::SameLine();
    if (ImGui::Button("Quick Save 2", buttonSize))
    {
        loadState(eQuickSlot2);
    }

    ImGui::Dummy(ImVec2(0.0f, 40.0f));

    ImGui::Dummy(ImVec2(buttonSize.x, 0));
    ImGui::SameLine();
    if (ImGui::Button("Quick Save 3", buttonSize))
    {
        loadState(eQuickSlot3);
    }

    ImGui::End();
}
