/**
 * @file      Shader.h
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Hlavičkový soubor definující třídu Shader.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SHADER_H
#define SHADER_H

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>

// #include <GL/glew.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

/**
 * @brief Třída zapouzdřující OpenGL (GLSL) program.
 */
class Shader
{
    public:
        Shader(std::string name = "Program", std::string srcDir = "Sources/GUI/shaders");
        ~Shader();

        Shader(const Shader& other) = delete;
        Shader(Shader&& other) noexcept = delete;

        Shader& operator=(const Shader& other) = delete;
        Shader& operator=(Shader&& other) noexcept = delete;

        void compile(GLenum type, std::string name = "shader", std::string file = "");
        void compile_vertex();
        void compile_fragment();
        void link();
        void use();

        unsigned int mMainProgram;

    private:
        std::string readFile(const char* file_path);

        std::string mSrcDir;
        std::string mProgramName;
        std::vector<unsigned int> mShader;

        bool mLinked = false;
};

#endif
