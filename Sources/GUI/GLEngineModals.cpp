/**
 * @file      GLEngineModals.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Soubor implementující třídu GLEngine.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include "GLEngine.h"
#include <filesystem>

/**
 * @brief Hledá soubory v adresáři
 * 
 * @param baseDir - Adresář, ve kterém se má hledat
 * @return std::vector<std::string> seznam souborů
 */
std::vector<std::string> dirDiscorvery(std::string baseDir)
{
    std::vector<std::string> files;
    for (const auto & entry : std::filesystem::directory_iterator("./" + baseDir))
    {
        files.push_back(entry.path().string());
    }
    return files;
}

/**
 * @brief Zjišťuje, zda soubor existuje
 * 
 * @param name - Jméno souboru
 * @return true - Soubor existuje
 * @return false - Soubor neexistuje
 */
bool fileExists(const std::string& name)
{
    std::ifstream f(name.c_str());
    return f.good();
}

bool GLEngine::fileSelectorButton()
{
    if (ImGui::Button("Vybrat Soubor"))
    {
        ImGui::OpenPopup("Vybrat Soubor");
        return true;
    }
    return false;
}

/**
 * @brief Modální okno pro výběr souboru
 * 
 * @param fileIsOutput - Zda se jedná o výstupní soubor
 * @param file - Výstupní reference zvoleného souboru
 * @param fileSelected - Výstupní reference zda je soubor vybrán
 * @return true - Soubor byl vybrán
 * @return false - Soubor nebyl vybrán
 */
bool GLEngine::fileSelectorModal(bool fileIsOutput, std::string& file, bool& fileSelected)
{
    // Always center this window when appearing
    ImVec2 center = ImGui::GetMainViewport()->GetCenter();
    ImGui::SetNextWindowPos(center, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));

    std::string selectedFile;

    if (ImGui::BeginPopupModal("Vybrat Soubor", NULL))
    {
        bool inputOverride = false;

        ImGui::Text(fileIsOutput ? "Výstupní soubor" : "Vstupní soubor");

        static char sTmpStrFileName[256] = "";
        ImGui::Text("./Data/");
        ImGui::SameLine();
        ImGui::InputTextWithHint(".hdf5", "Prázdné pro nepoužití", sTmpStrFileName, IM_ARRAYSIZE(sTmpStrFileName));
        selectedFile = "./Data/" + std::string(sTmpStrFileName) + ".hdf5";
        inputOverride = std::string(sTmpStrFileName) != "";

        ImGui::Text("Rychlá volba");
        static int sItemCurrentIdx = -1; // Here we store our selection data as an index.
        std::vector<std::string> files = dirDiscorvery("Data");
        if (ImGui::BeginListBox("##listbox 2", ImVec2(-FLT_MIN, 12 * ImGui::GetTextLineHeightWithSpacing())))
        {
            for (int n = 0; n < files.size(); n++)
            {
                const bool isSelected = (sItemCurrentIdx == n);
                if (ImGui::Selectable(files[n].c_str(), isSelected  && !inputOverride))
                {
                    sItemCurrentIdx = n;
                }

                // Set the initial focus when opening the combo (scrolling + keyboard navigation focus)
                if (isSelected)
                {
                    ImGui::SetItemDefaultFocus();
                }
            }
            ImGui::EndListBox();
        }
        ImGui::Text("");
        if (!inputOverride)
        {
            if (sItemCurrentIdx != -1)
            {
                selectedFile = files[sItemCurrentIdx];
            }
            else
            {
                selectedFile = "";
            }
        }
        if (selectedFile != "")
        {
            ImGui::Text("Soubor: %s ", selectedFile.c_str());
            ImGui::SameLine();
            ImVec4 color;
            std::string text;
            bool exists = fileExists(selectedFile);
            fileSelected = exists || fileIsOutput;
            if (exists)
            {
                text = "Existuje";
                if (fileIsOutput)
                {
                    color = ImVec4(1.0f, 1.0f, 0.0f, 1.0f);
                }
                else
                {
                    color = ImVec4(0.0f, 1.0f, 0.0f, 1.0f);
                }
            }
            else
            {
                if (fileIsOutput)
                {
                    color = ImVec4(0.0f, 1.0f, 0.0f, 1.0f);
                    text = "Bude vytvořen";
                }
                else
                {
                    color = ImVec4(1.0f, 0.0f, 0.0f, 1.0f);
                    text = "Neexistuje";
                }
            }

            ImGui::TextColored(color, text.c_str());
        }
        else
        {
            ImGui::Text("Soubor nezvolen");
            fileSelected = false;
        }
        if (ImGui::Button("Zrušit Výbeř"))
        {
            selectedFile = "";
            sItemCurrentIdx = -1;
            sTmpStrFileName[0] = '\0';
        }
        file = selectedFile;
        ImGui::Text("");
        if (ImGui::Button("Zavřít"))
        {
            ImGui::CloseCurrentPopup();
        }
        ImGui::EndPopup();
    }
    return false;
}

bool GLEngine::fileAndGroupSelectorButton()
{
    if (ImGui::Button("Otevřít Simulaci"))
    {
        ImGui::OpenPopup("Otevřít Simulaci");
        return true;
    }
    return false;
}

/**
 * @brief Modální okno pro výběr souboru a skupiny
 * 
 * @param file - Výstupní reference zvoleného souboru
 * @param group - Výstupní reference zvolené skupiny
 * @param fileSelected - Výstupní reference zda je soubor vybrán
 * @return true - Soubor byl vybrán
 * @return false - Soubor nebyl vybrán
 */
bool GLEngine::fileAndGroupSelectorModal(std::string& file, std::string& group, bool fileSelected)
{
    // Always center this window when appearing
    ImVec2 center = ImGui::GetMainViewport()->GetCenter();
    ImGui::SetNextWindowPos(center, ImGuiCond_Appearing, ImVec2(0.5f, 0.5f));

    if (ImGui::BeginPopupModal("Otevřít Simulaci", NULL))
    {
        std::vector<std::string> groups;
        if (fileSelected)
        {
            Storage s("TmpRead", true, false, file, true);
            groups = s.listGroups();
        }

        static int sItemCurrentIdx = -1; // Here we store our selection data as an index.
        if (ImGui::BeginListBox("##listbox 3", ImVec2(-FLT_MIN, 12 * ImGui::GetTextLineHeightWithSpacing())))
        {
            for (int n = 0; n < groups.size(); n++)
            {
                const bool is_selected = (sItemCurrentIdx == n);
                if (ImGui::Selectable(groups[n].c_str(), is_selected))
                {
                    sItemCurrentIdx = n;
                }
                if (is_selected)
                {
                    ImGui::SetItemDefaultFocus();
                }
            }
            ImGui::EndListBox();
        }

        if (sItemCurrentIdx != -1)
        {
            group = groups[sItemCurrentIdx];
        }

        ImGui::Text("");
        if (ImGui::Button("Zavřít"))
        {
            ImGui::CloseCurrentPopup();
            ImGui::EndPopup();
            return sItemCurrentIdx != -1;
        }
        ImGui::EndPopup();
    }
    return false;
}
