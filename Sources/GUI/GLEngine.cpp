/**
 * @file      GLEngine.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Soubor implementující třídu GLEngine.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include "GLEngine.h"

#include <fstream>
#include <nlohmann/json.hpp>

#include "../zaklad/TError.h"

#include "../Simulation.h"

class GLEngine * engine;

static GLuint sViewUniform;
static GLuint sProjUniform;
static GLuint sBoxUniform;
static GLuint sDefaultColorUniform;
static GLuint sUseDefaultColorUniform;

// Data pro osy
float sAxes[] =
{
    // Pozice       // Barvy
    0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f, // Os x - červená
    1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,

    1.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,
    1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f,

    1.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f,

    0.0f, 0.0f, 1.0f, 1.0f, 0.0f, 0.0f,
    0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,

    0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, // Os y - zelená
    0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,

    0.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
    1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,

    1.0f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,
    1.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f,

    0.0f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f, // Os z - modrá
    0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f,

    0.0f, 1.0f, 1.0f, 0.0f, 0.0f, 1.0f, // Os z - modrá
    0.0f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f
};

unsigned int sVAOBox, sVBOBox;

/**
 * @brief Vytváří nové instace simulací.
 * @param type - Typ simulace, neboli index registrované simulace z Registrator.
 * @return Ukazatel na novou instanci simulace.
 */
Simulation * genSimulation(Simulation::simType type)
{
    if (type >= Registrator::modulesCount())
    {
        throw QTError("Vybrána simulace mimo genSimulation");
    }
    return Registrator::getModules()[type].mCreate();
}

GLEngine::GLEngine(int gl_width, int gl_height)
{
    if (engine)
    {
        throw QTError("GLEngine::GLEngine: Pokus o vytvoření druhé instance");
    }
    engine = this;
    const char* glsl_version = "#version 450";

    //GLFW
    glfwSetErrorCallback(error_callback);
    if (!glfwInit())
    {
        exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, true);
    glfwWindowHint(GLFW_MAXIMIZED, GLFW_TRUE);

    // glfwWindowHint(GLFW_DECORATED, GLFW_FALSE);
    // GLFWwindow* window = glfwCreateWindow(1920, 1080, "BS4GC", NULL, NULL);
    mWindow = glfwCreateWindow(gl_width, gl_height, "BS4GC", NULL, NULL);
    if (!mWindow)
    {
        glfwTerminate();
        exit(EXIT_FAILURE);
    }

    glfwMakeContextCurrent(mWindow);
    glfwSwapInterval(1);

    glfwSetKeyCallback(mWindow, keyCallbackStatic);
    glfwSetCursorPosCallback(mWindow, mouseCallbackStatic);
    glfwSetMouseButtonCallback(mWindow, mouseButtonCallbackStatic);
    glfwSetScrollCallback(mWindow, scrollCallbackStatic);
    glfwSetFramebufferSizeCallback(mWindow, framebufferSizeCallbackStatic);

    //GLEW
    // GLenum err = glewInit();
    // if (GLEW_OK != err)
    // {
    //     std::cerr << "Error: " << glewGetErrorString(err) << std::endl;
    //     glfwTerminate();
    //     throw QTError("GLEngine::GLEngine: Selhání inicializace GLEW");
    //     // return -1;
    // }
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        throw QTError("GLEngine::GLEngine: Selhání inicializace GLAD");
    }    


    //OpenGL
    // glClearColor(0.1f,0.2f,0.2f,1.0f);
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glEnable(GL_DEPTH_TEST);
    // glEnable(GL_CULL_FACE);
    // glCullFace(GL_BACK);

    // if (GLEW_ARB_debug_output)
    // {
    //     glEnable(GL_DEBUG_OUTPUT);
    //     glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
    //     glDebugMessageCallback(debugCallback, nullptr);
    //     // glDebugMessageControl(GL_DONT_CARE, GL_DONT_CARE, GL_DONT_CARE, 0, nullptr, GL_TRUE);
    // }
    // else
    // {
    //     std::cerr << "Debug Output Extension not supported!" << std::endl;
    // }

    //IMGUI
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImPlot::CreateContext();

    mIO = &ImGui::GetIO();
    mIO->ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    mIO->ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
    // mIO->ConfigFlags |= ImGuiConfigFlags_DockingEnable;
    // mIO->ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;

    ImGui::StyleColorsDark();

    //https://stackoverflow.com/questions/14303677/languages-supported-by-latin-vs-latin-extended-glyphs-in-fonts-on-google-web
    const float fontSize = 13;
    const ImWchar glyphRangesLatinExtendedA[] = { 0x0100, 0x017F, 0 }; //
    ImFontConfig config;
    config.MergeMode = true;
    mIO->Fonts->AddFontFromFileTTF("Sources/GUI/fonts/Hack-Regular.ttf", fontSize);
    mIO->Fonts->AddFontFromFileTTF("Sources/GUI/fonts/Hack-Regular.ttf", fontSize, &config, glyphRangesLatinExtendedA);
    mIO->Fonts->Build();

    ImGui_ImplGlfw_InitForOpenGL(mWindow, true);
    ImGui_ImplOpenGL3_Init(glsl_version);

    // Buffery
    mBuffer.lateConstruct(&mVertices, &mColors, &mIndices);
    mBuffer.init();
    mBuffer.atribut(3);
    mBuffer.atributColor(3);

    // Vytvoření VAO a VBO
    glGenVertexArrays(1, &sVAOBox);
    glGenBuffers(1, &sVBOBox);
    // Nastavení VAO a VBO
    glBindVertexArray(sVAOBox);
    glBindBuffer(GL_ARRAY_BUFFER, sVBOBox);
    glBufferData(GL_ARRAY_BUFFER, sizeof(sAxes), sAxes, GL_STATIC_DRAW);
    // Nastavení atributů vertexu
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);

    // Shadery
    // program.compile(GL_GEOMETRY_SHADER, "geometry");
    mProgram.compile_vertex();
    mProgram.compile_fragment();
    mProgram.link();
    mProgram.use();

    sViewUniform = glGetUniformLocation(mProgram.mMainProgram, "view");
    sProjUniform = glGetUniformLocation(mProgram.mMainProgram, "proj");
    sBoxUniform = glGetUniformLocation(mProgram.mMainProgram, "box");
    sDefaultColorUniform = glGetUniformLocation(mProgram.mMainProgram, "defaultColor");
    sUseDefaultColorUniform = glGetUniformLocation(mProgram.mMainProgram, "useDefaultColor");
    // sunUniform = glGetUniformLocation(program.m_mainProgram,"sun");

    int displayW, displayH;
    glfwGetFramebufferSize(mWindow, &displayW, &displayH);
    framebufferSizeCallbackStatic(mWindow, displayW, displayH);
    // init();

    mSim = new Simulation();
    mSecondarySim = new Simulation();
}

GLEngine::~GLEngine()
{
    ImGui_ImplOpenGL3_Shutdown();
    ImGui_ImplGlfw_Shutdown();
    ImPlot::DestroyContext();
    ImGui::DestroyContext();

    if (mSim)
    {
        delete mSim;
    }
    if (mSecondarySim)
    {
        delete mSecondarySim;
    }
}

/**
 * @brief Hlavní smyčka, která běží po celou dobu běhu programu. Beží tolikrát za sekundu jaká je obnovovací frekvence monitoru.
 * 
 */
void GLEngine::mainLoop()
{
    while (!glfwWindowShouldClose(mWindow))
    {
        mRednerTimer.stop();
        glfwPollEvents();
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        ////// Moje GUI vykreslování //////

        guiNewFrame();
        guiUpdate();

        ////// Moje vykreslování //////

        mRednerTimer.start();

        renderPrepare();
        renderBox();

        if (mSyncFlag == eFullSync)
        {
            // while(!mSim->mSimIsRunning && !mSecondarySim->mSimIsRunning);
            if (!mSim->mSimIsRunning && !mSecondarySim->mSimIsRunning)
            {
                newFrame(false);
                renderSim(false);

                newFrame(true);
                renderSim(true);

                resumeSims();
            }
            else
            {
                renderSim(true);
            }
        }
        else
        {
            if (mSyncFlag == eOneWaySync && mSecondarySimSelect == Simulation::eSimPlayer)
            {
                int frame = -1;

                frame = newFrame(false);
                renderSim(false);

                if (mSecondarySim && frame != -1)
                {
                    mSecondarySim->loadFrame(frame);
                    newFrame(true);
                    renderSim(true);
                }
            }
            else
            {
                newFrame(false);
                renderSim(false);

                newFrame(true);
                renderSim(true);

            }
        }

        ///////////////////////////////

        guiRender();
        if (mIO->ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
        {
            GLFWwindow* backup_current_context = glfwGetCurrentContext();
            ImGui::UpdatePlatformWindows();
            ImGui::RenderPlatformWindowsDefault();
            glfwMakeContextCurrent(backup_current_context);
        }
        glfwSwapBuffers(mWindow);
    }
}

/**
 * @brief Funkce, která získá data ze simulace.
 * @param secondary - Indikuje zda se nový frame má načíst do sekundární simulace.
 */
int GLEngine::newFrame(bool secondary)
{
    int retFrameNum = -1;
    if (!secondary)
    {
        mSim->set(mLiveSettCopy);

        auto [frame, positions, colors] = mSim->mStorage->lockLatestFrame();
        if (frame.mArraySize != mVertices.size() && frame.mArraySize != 0)
        {
            mVertices.resize(frame.mArraySize);
            mColors.resize(frame.mArraySize);
        }
        memcpy(mVertices.data(), positions.data(), positions.size() * sizeof(FloatV));
        memcpy(mColors.data(), colors.data(), colors.size() * sizeof(FloatV));
        retFrameNum = frame.mFrameNum;
        mSim->mStorage->unLockLatestFrame();

        printc(bc.YELLOW, "\r%f/%f\t", mSim->mTime, mInitSettCopy.mEndTime);
        printc(bc.GRAY, "E: %.3e ", mSim->mEnergy);
        printc(bc.DRED, "SPS: %.1f\t", 1000.0 / mSim->mStepTimeMS);
    }
    else
    {
        mSecondarySim->set(mLiveSettCopy);

        auto [frame, positions, colors] = mSecondarySim->mStorage->lockLatestFrame();
        if (frame.mArraySize != mVertices.size() && frame.mArraySize != 0)
        {
            mVertices.resize(frame.mArraySize);
            mColors.resize(frame.mArraySize);
        }
        memcpy(mVertices.data(), positions.data(), positions.size() * sizeof(FloatV));
        memcpy(mColors.data(), colors.data(), colors.size() * sizeof(FloatV));
        retFrameNum = frame.mFrameNum;
        mSecondarySim->mStorage->unLockLatestFrame();
    }
    return retFrameNum;
}

/**
 * @brief Funkce, která připraví vykreslení. Zvolí shader a nastaví view a projekční matici.
 */
void GLEngine::renderPrepare()
{
    mProgram.use();

    int width, height;
    glfwGetFramebufferSize(mWindow, &width, &height);
    float aspectRatio = (float) width / (float) height;

    if (mCam.mOrthographicProjection) {
        sProj = glm::ortho(-mCam.mDistance * aspectRatio, mCam.mDistance * aspectRatio, -mCam.mDistance, mCam.mDistance, 0.0f, 1000.0f);
    } else {
        sProj = glm::perspective(glm::radians(mCam.mFOV), aspectRatio, 0.1f, 1000.0f);
    }

    glm::vec3 direction;
    direction.x = cos(glm::radians(mCam.mYaw)) * cos(glm::radians(mCam.mPitch));
    direction.y = sin(glm::radians(mCam.mPitch));
    direction.z = sin(glm::radians(mCam.mYaw)) * cos(glm::radians(mCam.mPitch));
    glm::vec3 cameraPos = glm::vec3(mCam.mLookAtX, mCam.mLookAtY, mCam.mLookAtZ);
    glm::vec3 cameraFront = glm::normalize(direction) * mCam.mDistance;
    glm::vec3 cameraTarget = cameraPos + cameraFront;
    sView = glm::lookAt(cameraTarget, glm::vec3(mCam.mLookAtX, mCam.mLookAtY, mCam.mLookAtZ), glm::vec3(0.f, 1.f, 0.f));

    glProgramUniformMatrix4fv(mProgram.mMainProgram, sViewUniform, 1, 0, glm::value_ptr(sView));
    glProgramUniformMatrix4fv(mProgram.mMainProgram, sProjUniform, 1, 0, glm::value_ptr(sProj));

}

/**
 * @brief Funkce, která vykreslí simulaci.
 * @param secondary - Indikuje zda se má vykreslit sekundární simulace.
 */
void GLEngine::renderSim(bool secondary)
{
    mProgram.use();
    mBuffer.bindVertexArray();
    mBuffer.copyVboData();
    glProgramUniform1i(mProgram.mMainProgram, sBoxUniform, 0);

    if (!secondary)
    {
        glProgramUniform3f(mProgram.mMainProgram, sDefaultColorUniform, mDefaultColor[0], mDefaultColor[1], mDefaultColor[2]);
        glProgramUniform1i(mProgram.mMainProgram, sUseDefaultColorUniform, mRederDefaultSimColors);
    }
    else
    {
        glProgramUniform3f(mProgram.mMainProgram, sDefaultColorUniform, mSecondaryDefaultColor[0], mSecondaryDefaultColor[1], mSecondaryDefaultColor[2]);
        glProgramUniform1i(mProgram.mMainProgram, sUseDefaultColorUniform, mRederDefaultSecondarySimColors);
    }

    if (mRenderPoints)
    {
        if (!secondary)
        {
            glPointSize(mPointSize);
        }
        else
        {
            glPointSize(mSecondaryPointSize);
        }
        glDrawArrays(GL_POINTS, 0, mVertices.size());
    }
    else
    {
        glDrawElements(GL_TRIANGLES, mIndices.size(), GL_UNSIGNED_INT, NULL);
    }
}

/**
 * @brief Vykreslí osy ohraničující prostor.
 */
void GLEngine::renderBox()
{
    mProgram.use();
    //Box
    glBindVertexArray(sVAOBox);
    glProgramUniform1i(mProgram.mMainProgram, sBoxUniform, 1);
    glDrawArrays(GL_LINES, 0, 18);
}

/**
 * @brief Uloží stav GUI do souboru.
 */
void GLEngine::saveState(SaveSlot slot)
{
    if (slot == eNoSlot)
    {
        return;
    }
    try
    {
        const char* files[] = {"", "last.json", "quick1.json", "quick2.json", "quick3.json"};

        nlohmann::json j;
        j["mProgramState"] = mProgramState;
        j["mShouldBeStopped"] = mShouldBeStopped;
        j["mShouldBePaused"] = mShouldBePaused;
        j["mNextSyncFlag"] = mNextSyncFlag;
        j["mSyncFlag"] = mSyncFlag;
        j["mSimSelect"] = mSimSelect;
        j["mSimFile"] = mSimFile;
        j["mSimDesc"] = mSimDesc;
        j["mSimFileSelected"] = mSimFileSelected;
        j["mSecondarySimSelect"] = mSecondarySimSelect;
        j["mSecondarySimFile"] = mSecondarySimFile;
        j["mSecondarySimDesc"] = mSecondarySimDesc;
        j["mSecondarySimFileSelected"] = mSecondarySimFileSelected;
        j["mScenarionFromFile"] = mScenarionFromFile;
        j["mScenarioSelected"] = mScenarioSelected;
        j["mScenarioFile"] = mScenarioFile;
        j["mScenarioDesc"] = mScenarioDesc;

        j["mRederDefaultSimColors"] = mRederDefaultSimColors;
        j["mRederDefaultSecondarySimColors"] = mRederDefaultSecondarySimColors;

        j["mPointSize"] = mPointSize;
        j["mSecondaryPointSize"] = mSecondaryPointSize;
        j["mDefaultColor0"] = mDefaultColor[0];
        j["mDefaultColor1"] = mDefaultColor[1];
        j["mDefaultColor2"] = mDefaultColor[2];
        j["mSecondaryDefaultColor0"] = mSecondaryDefaultColor[0];
        j["mSecondaryDefaultColor1"] = mSecondaryDefaultColor[1];
        j["mSecondaryDefaultColor2"] = mSecondaryDefaultColor[2];

        std::ofstream file(files[slot]);
        file << j;
    }
    catch (...)
    {

    }
}

/**
 * @brief Načte stav GUI ze souboru.
 */
void GLEngine::loadState(SaveSlot slot)
{
    if (slot == eNoSlot)
    {
        return;
    }

    try
    {
        const char* files[] = {"", "last.json", "quick1.json", "quick2.json", "quick3.json"};

        std::ifstream file(files[slot]);
        nlohmann::json j;
        file >> j;

        mShouldBeStopped = j["mShouldBeStopped"];
        mShouldBePaused = j["mShouldBePaused"];
        mNextSyncFlag = static_cast<SyncType>(j["mNextSyncFlag"]);
        mSyncFlag = static_cast<SyncType>(j["mSyncFlag"]);
        mSimSelect = static_cast<Simulation::simType>(j["mSimSelect"]);
        mSimFile = j["mSimFile"];
        mSimDesc = j["mSimDesc"];
        mSimFileSelected = j["mSimFileSelected"];
        mSecondarySimSelect = static_cast<Simulation::simType>(j["mSecondarySimSelect"]);
        mSecondarySimFile = j["mSecondarySimFile"];
        mSecondarySimDesc = j["mSecondarySimDesc"];
        mSecondarySimFileSelected = j["mSecondarySimFileSelected"];
        mScenarionFromFile = j["mScenarionFromFile"];
        mScenarioSelected = j["mScenarioSelected"];
        mScenarioFile = j["mScenarioFile"];
        mScenarioDesc = j["mScenarioDesc"];

        mRederDefaultSimColors = j["mRederDefaultSimColors"];
        mRederDefaultSecondarySimColors = j["mRederDefaultSecondarySimColors"];

        mPointSize = j["mPointSize"];
        mSecondaryPointSize = j["mSecondaryPointSize"];
        mDefaultColor[0] = j["mDefaultColor0"];
        mDefaultColor[1] = j["mDefaultColor1"];
        mDefaultColor[2] = j["mDefaultColor2"];
        mSecondaryDefaultColor[0] = j["mSecondaryDefaultColor0"];
        mSecondaryDefaultColor[1] = j["mSecondaryDefaultColor1"];
        mSecondaryDefaultColor[2] = j["mSecondaryDefaultColor2"];

        mProgramState = static_cast<ProgramState>(j["mProgramState"]);

        Storage s(mScenarioDesc, true, false, mScenarioFile, false);
        s.loadSettings(mLiveSettCopy, mInitSettCopy);
    }
    catch (...)
    {

    }
}
