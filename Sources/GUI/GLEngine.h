/**
 * @file      GLEngine.h
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Hlavičkový soubor definující třídu GLEngine.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GLENGINE_H
#define GLENGINE_H

#include <stdio.h>
#include <stdlib.h>

#include <iostream>

// #include <GL/glew.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>

#include "../imgui/imgui.h"
#include "../imgui/backends/imgui_impl_glfw.h"
#include "../imgui/backends/imgui_impl_opengl3.h"
#include "../implot/implot.h"

#include "Shader.h"
#include "ShaderBuffer.h"

#include "../zaklad/Timer.h"

#include "../Simulation.h"
#include "../Registrator.h"

/// Struktura Vertexu.
struct Vertex
{
    float mX, mY, mZ;
    // float r, g, b, a;
};

/// Struktura barvy Vertexu.
struct Point
{
    float mR, mG, mB;
};

/// Struktura kamery.
struct Camera
{
    float mLookAtX = 0.5, mLookAtY = 0.4, mLookAtZ = 0.5;
    float mFOV = 45.0;
    float mYaw = 68.0;
    float mPitch = 16.0;
    float mDistance = 1.6;
    bool mOrthographicProjection = false;
};

Simulation* genSimulation(Simulation::simType type);

void error_callback(int error, const char* description);
void keyCallbackStatic(GLFWwindow* window, int key, int scancode, int action, int mods);
void mouseCallbackStatic(GLFWwindow* window, double xpos, double ypos);
void mouseButtonCallbackStatic(GLFWwindow* window, int button, int action, int mods);
void scrollCallbackStatic(GLFWwindow* window, double xoffset, double yoffset);
void framebufferSizeCallbackStatic(GLFWwindow* window, int width, int height);
void APIENTRY debugCallback(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar *message, const void *userParam);

/**
 * @class GLEngine
 * @brief Třída reprezentující OpenGL engine.
 *
 * Tato třída je typu jedináček (singleton) a reprezentuje OpenGL engine, který zajišťuje vykreslování simulace, řízení běhu simulace a uživatelské rozhraní.
 */
class GLEngine
{
    public:
        /// Konstruktor
        GLEngine(int gl_width = 1280, int gl_height = 720);
        /// Destruktor
        ~GLEngine();

        /// Kopírovací konstruktor. Není povolen.
        GLEngine(const GLEngine& other) = delete;
        /// Přesouvací konstruktor. Není povolen.
        GLEngine(GLEngine&& other) noexcept = delete;

        /// Opeátor přiřazení. Není povolen.
        GLEngine& operator=(const GLEngine& other) = delete;
        /// Operátor přiřazení pomocí přesunutí. Není povolen.
        GLEngine& operator=(GLEngine&& other) noexcept = delete;

        /// Hlavní smyčka OpenGL enginu, která zajišťuje vykreslování simulace ovládá běh simulace.
        void mainLoop();

        /// Zpracování vstupů z klávesnice.
        void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
        void mouseCallback(GLFWwindow* window, double xpos, double ypos);
        void mouseButtonCallback(GLFWwindow* window, int button, int action, int mods);
        void scrollCallback(GLFWwindow* window, double xoffset, double yoffset);
        void framebufferSizeCallback(GLFWwindow* window, int width, int height);

    private:
        enum ProgramState
        {
            eMainScreen,
            eDevelScreen,
            eBenchScreen,
            eTempScreen
        };

        enum SyncType : int
        {
            eNoSync,
            eFullSync,
            eOneWaySync // Synchronizuje pouze SimPlayer s hlavní simulací
        };

        enum SaveSlot : int
        {
            eNoSlot,
            eLastSlot,
            eQuickSlot1,
            eQuickSlot2,
            eQuickSlot3
        };

        //GUI zlepšováky
        static void fakeButton(std::string text, ImVec4 color);
        static bool colorButton(std::string text, ImVec4 color, bool& status);
        static bool redGreenButton(std::string text, bool& status);
        static bool grayBlueButton(std::string text, bool& status);

        /// Uložení a načtení stavu programu.
        void saveState(SaveSlot slot = GLEngine::eLastSlot);
        void loadState(SaveSlot slot = GLEngine::eLastSlot);

        /// Vykreslení uživatelského rozhraní
        void guiNewFrame();
        void guiUpdate();
        void guiRender();

        /// Obrazovky
        void guiMainScreen();
        void guiDevelScreen();
        void guiBenchScreen();

        /// Pomocné funkce tlačítek
        bool fileSelectorButton();
        bool fileSelectorModal(bool fileIsOutput, std::string& file, bool& fileSelected);
        bool fileAndGroupSelectorButton();
        bool fileAndGroupSelectorModal(std::string& file, std::string& group, bool fileSelected);

        /// Okna
        void showCameraWindow();
        void showStatWindow(bool secondToo);
        void showEditorWindow();

        /// Inicializace Simulací ovládaných enginem.
        void startSims();
        void restartSims();
        void stopSims();
        void pauseSims();
        void resumeSims();

        /// Grafické vykreslení simulace
        void renderPrepare();
        void renderBox();
        int newFrame(bool secondary);
        void renderSim(bool secondary);

        ProgramState mProgramState = eMainScreen;

        GLFWwindow* mWindow;
        ImGuiIO* mIO;

        int mDisplayW;
        int mDisplayH;

        bool mShowScenarioWindow = true;
        bool mShowSimulationWindow = true;
        bool mShowSecondarySimulationWindow = true;
        bool mShowStatWindow = true;
        bool mShowCameraWindow = false;
        bool mShowEditorWindow = false;
        bool mShowResultWindow = false;

        bool mShowHelpWindow = false;
        bool mRenderPoints = true;
        bool mRederDefaultSimColors = true;
        bool mRederDefaultSecondarySimColors = true;

        float mPointSize = 2;
        float mSecondaryPointSize = 2;
        float mDefaultColor[3] = {0.0f, 0.0f, 1.0f};
        float mSecondaryDefaultColor[3] = {1.0f, 0.0f, 0.0f};

        Camera mCam;
        glm::mat4 sView;
        glm::mat4 sProj;

        double mMouseLastX;
        double mMouseLastY;
        bool mFirstMouse = true;
        bool mRightMouseButtonPressed = false;

        std::vector<struct Vertex> mVertices;
        std::vector<struct Point> mColors;
        std::vector<unsigned int> mIndices;

        ShaderBuffer<struct Vertex, struct Point> mBuffer;

        Shader mProgram;

        Timer mRednerTimer;

        bool mShouldBeStopped = true;
        bool mShouldBePaused = false;

        int mSimsTargetFrame = -1;

        SyncType mNextSyncFlag = eNoSync;
        SyncType mSyncFlag = eNoSync;

        /// Primární simulace
        Simulation::simType mSimSelect = Simulation::eSimCPU;
        Simulation* mSim = NULL;
        std::string mSimFile = "";
        std::string mSimDesc = "";
        bool mSimFileSelected = false;

        /// Sekundární simulace
        Simulation::simType mSecondarySimSelect = Simulation::eSimEmpty;
        Simulation* mSecondarySim = NULL;
        std::string mSecondarySimFile = "";
        std::string mSecondarySimDesc = "";
        bool mSecondarySimFileSelected = false;

        /// Scénář
        bool mScenarionFromFile = false;
        bool mScenarioSelected = false;
        std::string mScenarioFile = "";
        std::string mScenarioDesc = "";

        struct LiveSettings mLiveSettCopy;
        struct InitSettings mInitSettCopy;
};

extern class GLEngine* engine;

#endif
