/**
 * @file      GLEngineBojects.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Soubor implementující třidu GLEngine.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include "GLEngine.h"

/// Barevné tlačítko co neudělá nic.
void GLEngine::fakeButton(std::string text, ImVec4 color)
{
    ImGui::PushID(0);
    ImGui::PushStyleColor(ImGuiCol_Button, color);
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, color);
    ImGui::PushStyleColor(ImGuiCol_ButtonActive, color);
    ImGui::Button(text.c_str());
    ImGui::PopStyleColor(3);
    ImGui::PopID();
}

/// Barevné tlačítko.
bool GLEngine::colorButton(std::string text, ImVec4 color, bool& status)
{
    ImGui::PushStyleColor(ImGuiCol_Button, color);
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4(color.x * 1.2 + 0.1, color.y * 1.2 + 0.1, color.z * 1.2 + 0.1, color.w));
    ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4(color.x * 1.2, color.y * 1.2, color.z * 1.2, color.w));
    bool ret = ImGui::Button(text.c_str());
    ImGui::PopStyleColor(3);
    if (ret)
    {
        status = !status;
    }
    return ret;
}

bool GLEngine::redGreenButton(std::string text, bool& status)
{
    if (status)
    {
        return colorButton(text, ImVec4(0, 0.5, 0, 1), status);
    }
    else
    {
        return colorButton(text, ImVec4(0.5, 0, 0, 1), status);
    }
}

bool GLEngine::grayBlueButton(std::string text, bool& status)
{
    if (status)
    {
        return colorButton(text, ImVec4(0.1, 0.1, 0.6, 1), status);
    }
    else
    {
        return colorButton(text, ImVec4(0.3, 0.3, 0.3, 1), status);
    }
}

