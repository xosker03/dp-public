/**
 * @file      GLEngineDevelScreen.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Soubor implementující třídu GLEngine.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include "GLEngine.h"

//TODO nastavit počáteční polohy a velikosti oken pokud neexistuje imgui.ini
/**
 * @brief Kompletní GUI a logika pro vývojovou obrazovku.
 * 
 */
void GLEngine::guiDevelScreen()
{
    ImGui::PushStyleVar(ImGuiStyleVar_FramePadding, ImVec2(30.0f, 8.0f));
    ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(0.0f, 0.0f));
    // ImGui::PushStyleVar(ImGuiStyleVar_IndentSpacing, ImVec2(20.0f, 10.0f));

    if (ImGui::BeginMainMenuBar())
    {
        ImGui::PushStyleVar(ImGuiStyleVar_ItemSpacing, ImVec2(10.0f, 0.0f));
        // ImGui::Dummy(ImVec2(40.0f, 0));
        if (ImGui::BeginMenu("MENU"))
        {
            if (ImGui::MenuItem("Main Screen"))
            {
                mProgramState = eMainScreen;
            }
            if (ImGui::MenuItem("Benchmark Screen"))
            {
                mProgramState = eBenchScreen;
            }
            ImGui::Text("");
            if (ImGui::MenuItem("Load Last Save"))
            {
                loadState(eLastSlot);
            }
            ImGui::Text("");
            if (ImGui::MenuItem("Load Quick Save 1"))
            {
                loadState(eQuickSlot1);
            }
            if (ImGui::MenuItem("Load Quick Save 2"))
            {
                loadState(eQuickSlot2);
            }
            if (ImGui::MenuItem("Load Quick Save 3"))
            {
                loadState(eQuickSlot3);
            }
            ImGui::Text("");
            if (ImGui::MenuItem("Save Quick Save 1"))
            {
                saveState(eQuickSlot1);
            }
            if (ImGui::MenuItem("Save Quick Save 2"))
            {
                saveState(eQuickSlot2);
            }
            if (ImGui::MenuItem("Save Quick Save 3"))
            {
                saveState(eQuickSlot3);
            }
            ImGui::EndMenu();
        }
        ImGui::PopStyleVar();

        if (grayBlueButton("Scénář", mShowScenarioWindow))
        {
        }
        if (grayBlueButton("Simulace", mShowSimulationWindow))
        {
        }
        if (grayBlueButton("Simulace 2", mShowSecondarySimulationWindow))
        {
        }
        if (grayBlueButton("Statistiky", mShowStatWindow))
        {
        }
        if (grayBlueButton("Kamera", mShowCameraWindow))
        {
        }
        if (grayBlueButton("Editor", mShowEditorWindow))
        {
        }

        ImGui::Dummy(ImVec2(mDisplayW - 130 * 9, 0));

        fakeButton("Stav Vlákna", mSim->mThreadIsRunning ? ImVec4(0, 0.5, 0, 1) : ImVec4(0.5, 0, 0, 1));
        fakeButton("Stav Simulace", mSim->mSimIsRunning ? ImVec4(0, 0.5, 0, 1) : ImVec4(0.5, 0, mSyncFlag ? 0.5 : 0, 1));
        fakeButton("Odchylka", ImVec4(0, 0.5, 0, 1));

        ImGui::EndMainMenuBar();
    }

    ImGui::PopStyleVar(2);

    /// Okno scénáře
    if (mShowScenarioWindow)
    {
        ImGui::Begin("Scénář", &mShowScenarioWindow);

        ImGui::Checkbox("Ze Souboru", &mScenarionFromFile);

        //TODO tohle by mělo být na lepším místě, protože pokud okno nebude otevřené......
        if (mScenarionFromFile)
        {
            fileSelectorButton();
            fileSelectorModal(false, mScenarioFile, mScenarioSelected);
            ImGui::SameLine();
            fakeButton(mScenarioFile, ImVec4(0, 0, 0.5, 1));
            if (mScenarioSelected)
            {
                fileAndGroupSelectorButton();
                bool simSelected = fileAndGroupSelectorModal(mScenarioFile, mScenarioDesc, mScenarioSelected);
                ImGui::SameLine();
                fakeButton(mScenarioDesc, ImVec4(0, 0, 0.5, 1));

                if (simSelected)
                {
                    Storage s(mScenarioDesc, true, false, mScenarioFile, false);
                    s.loadSettings(mLiveSettCopy, mInitSettCopy);
                }
            }

            ImGui::BeginDisabled();
            ImGui::Text("");
        }

        ImGui::Text("Počáteční Parametry");
        ImGui::SliderInt("Kostka", &mInitSettCopy.mCubeSide, 2, 64);
        ImGui::InputDouble("Počáteční čas", &mInitSettCopy.mTime, 1, 10, "%lf");
        ImGui::InputDouble("Koncový čas", &mInitSettCopy.mEndTime, 1, 10, "%lf");
        ImGui::SliderFloat("Rychlost X", &mInitSettCopy.mInitialSpeed.mX, -16, 16);
        ImGui::SliderFloat("Rychlost Y", &mInitSettCopy.mInitialSpeed.mY, -16, 16);
        ImGui::SliderFloat("Rychlost Z", &mInitSettCopy.mInitialSpeed.mZ, -16, 16);
        ImGui::Text("");
        ImGui::Text("Živé parametry");
        ImGui::SliderFloat("Mezery", &mLiveSettCopy.mSpacing, 0.01, 0.1);
        ImGui::SliderInt("Sousedů", &mLiveSettCopy.mParticleNeighbours, 1, mInitSettCopy.mCubeSide - 1);
        ImGui::SliderInt("Mezikroků", &mLiveSettCopy.mSubsteps, 1, 20);
        ImGui::SliderFloat("Prostor", &mLiveSettCopy.mWallR.mX, 1, 10);
        mLiveSettCopy.mWallR.mY = mLiveSettCopy.mWallR.mX;
        mLiveSettCopy.mWallR.mZ = mLiveSettCopy.mWallR.mX;

        ImGui::Text("");
        ImGui::SliderFloat("Gravitace", &mLiveSettCopy.mGravityEnabled, 0, 2);

        ImGui::Text("");
        ImGui::SliderFloat("Akcelerace X", &mLiveSettCopy.mExternalAcceleration.mX, -16, 16);
        ImGui::SliderFloat("Akcelerace Y", &mLiveSettCopy.mExternalAcceleration.mY, -16, 16);
        ImGui::SliderFloat("Akcelerace Z", &mLiveSettCopy.mExternalAcceleration.mZ, -16, 16);

        ImGui::Text("");
        ImGui::InputFloat("dT", &mLiveSettCopy.mDT, 1.0e-3, 1.0e-2, "%e");
        ImGui::InputFloat("Alfa", &mLiveSettCopy.mAlpha, 1.0e-6, 1.0e-5, "%e");

        ImGui::Text("");
        ImGui::InputFloat("Ztráty", &mLiveSettCopy.mDumping, 1.0e-3, 1.0e-2, "%f");
        ImGui::InputFloat("Kolmé Ztráty", &mLiveSettCopy.mDumpingPerpendicular, 1.0e-3, 1.0e-2, "%f");

        if (mScenarionFromFile)
        {
            ImGui::EndDisabled();
        }

        ImGui::End();
    }

    /// Okno hlavní simulace
    if (mShowSimulationWindow)
    {
        ImGui::Begin("Simulace", &mShowSimulationWindow);

        static ImGuiComboFlags sFlags = 0;
        if (ImGui::BeginCombo("Implementace", Registrator::modulesNames()[mSimSelect], sFlags))
        {
            for (int n = 1; n < Registrator::modulesCount(); n++)
            {
                const bool is_selected = (mSimSelect == n);
                if (ImGui::Selectable(Registrator::modulesNames()[n], is_selected))
                {
                    mSimSelect = (Simulation::simType) n;
                }

                if (is_selected)
                {
                    ImGui::SetItemDefaultFocus();
                }
            }
            ImGui::EndCombo();
        }

        fileSelectorButton();
        fileSelectorModal(mSimSelect != Simulation::eSimPlayer, mSimFile, mSimFileSelected);
        ImGui::SameLine();
        fakeButton(mSimFile, ImVec4(0, 0, 0.5, 1));
        if (mSimSelect == Simulation::eSimPlayer && mSimFileSelected)
        {
            fileAndGroupSelectorButton();
            fileAndGroupSelectorModal(mSimFile, mSimDesc, mSimFileSelected);
            ImGui::SameLine();
            fakeButton(mSimDesc, ImVec4(0, 0, 0.5, 1));
        }
        if (mSimSelect != Simulation::eSimPlayer && mSimFileSelected)
        {
            static char sTmpFileName[128] = {0};
            strncpy(sTmpFileName, mSimDesc.c_str(), 127);
            ImGui::InputTextWithHint("Popis", "", sTmpFileName, IM_ARRAYSIZE(sTmpFileName));
            mSimDesc = std::string(sTmpFileName);
        }

        fakeButton("Stav Simulace", mSim->mSimIsRunning ? ImVec4(0, 0.5, 0, 1) : ImVec4(0.5, 0, mSyncFlag ? 0.5 : 0, 1));
        ImGui::SameLine();
        fakeButton("Stav Vlákna", mSim->mThreadIsRunning ? ImVec4(0, 0.5, 0, 1) : ImVec4(0.5, 0, 0, 1));
        if (mShouldBePaused)
        {
            ImGui::SameLine();
            fakeButton("Pauza", ImVec4(0.5, 0, 0, 1));
        }

        ImGui::Text("Synchronizace");
        if (mSecondarySimSelect != Simulation::eSimPlayer && mNextSyncFlag == eOneWaySync)
        {
            mNextSyncFlag = eFullSync;
        }
        ImGui::RadioButton("Žádná", reinterpret_cast<int*>(&mNextSyncFlag), eNoSync);
        ImGui::SameLine();
        ImGui::RadioButton("Plná", reinterpret_cast<int*>(&mNextSyncFlag), eFullSync);
        ImGui::SameLine();
        if (mSecondarySimSelect != Simulation::eSimPlayer)
        {
            ImGui::BeginDisabled();
        }
        ImGui::RadioButton("Sekundární", reinterpret_cast<int*>(&mNextSyncFlag), eOneWaySync);
        if (mSecondarySimSelect != Simulation::eSimPlayer)
        {
            ImGui::EndDisabled();
        }

        if (ImGui::Button("Restartovat"))
        {
            saveState(eLastSlot);
            restartSims();
        }
        ImGui::SameLine();
        if (ImGui::Button("Pozastavit"))
        {
            mShouldBePaused = true;
            pauseSims();
        }
        ImGui::SameLine();
        if (ImGui::Button("Pokračovat"))
        {
            mShouldBePaused = false;
            resumeSims();
        }
        if (ImGui::Button("Zastavit"))
        {
            stopSims();
        }

        ImGui::Text("");

        if (ImGui::Button("Pokračuj a udělej 1 krok"))
        {
            mSimsTargetFrame = mSim->mFrame + 1;
            mSim->mPauseOnFrame = mSimsTargetFrame;
            mSecondarySim->mPauseOnFrame = mSimsTargetFrame;
            resumeSims();
        }

        ImGui::Text("");

        ImGui::Text("Pozastavení v kroku");
        ImGui::InputInt("Krok", &mSimsTargetFrame);
        ImGui::SameLine();
        if (ImGui::Button("Nastav"))
        {
            mSim->mPauseOnFrame = mSimsTargetFrame;
            mSecondarySim->mPauseOnFrame = mSimsTargetFrame;
        }
        ImGui::Text("");

        ImGui::Text("");
        ImGui::Text("Simulace   %.3f ms (%.1f SPS)", mSim->mStepTimeMS, 1000.0f /  mSim->mStepTimeMS);

        ImGui::Text("");
        ImGui::Text("Simulace");
        ImGui::Text("Krok  %d", mSim->mFrame);
        ImGui::Text("Čas  %f/%f", mSim->mTime, mInitSettCopy.mEndTime);
        ImGui::Text("Energie  %.3e", mSim->mEnergy);
        ImGui::Text("Objem    %.3e", 0.);

        ImGui::End();
    }

    /// Okno sekundární simulace
    if (mShowSecondarySimulationWindow)
    {
        ImGui::Begin("Simulace 2", &mShowSecondarySimulationWindow);

        static ImGuiComboFlags sFlags = 0;
        if (ImGui::BeginCombo("Implementace", Registrator::modulesNames()[mSecondarySimSelect], sFlags))
        {
            for (int n = 0; n < Registrator::modulesCount(); n++)
            {
                const bool is_selected = (mSecondarySimSelect == n);
                if (ImGui::Selectable(Registrator::modulesNames()[n], is_selected))
                {
                    mSecondarySimSelect = (Simulation::simType) n;
                    // init();
                }

                if (is_selected)
                {
                    ImGui::SetItemDefaultFocus();
                }
            }
            ImGui::EndCombo();
        }

        fileSelectorButton();
        fileSelectorModal(mSecondarySimSelect != Simulation::eSimPlayer, mSecondarySimFile, mSecondarySimFileSelected);
        ImGui::SameLine();
        fakeButton(mSecondarySimFile, ImVec4(0, 0, 0.5, 1));

        if (mSecondarySimSelect == Simulation::eSimPlayer && mSecondarySimFileSelected)
        {
            fileAndGroupSelectorButton();
            bool selected = fileAndGroupSelectorModal(mSecondarySimFile, mSecondarySimDesc, mSecondarySimFileSelected);
            ImGui::SameLine();
            fakeButton(mSecondarySimDesc, ImVec4(0, 0, 0.5, 1));

            if (selected)
            {
                mScenarionFromFile = true;
                mScenarioSelected = true;
                mScenarioFile = mSecondarySimFile;
                mScenarioDesc = mSecondarySimDesc;
                Storage s(mScenarioDesc, true, false, mScenarioFile, false);
                s.loadSettings(mLiveSettCopy, mInitSettCopy);
            }
        }
        if (mSecondarySimSelect != Simulation::eSimPlayer && mSecondarySimFileSelected)
        {
            static char sTmpFileName[128] = {0};
            strncpy(sTmpFileName, mSecondarySimDesc.c_str(), 127);
            ImGui::InputTextWithHint("Popis", "", sTmpFileName, IM_ARRAYSIZE(sTmpFileName));
            mSecondarySimDesc = std::string(sTmpFileName);
        }

        fakeButton("Stav Simulace 2", mSecondarySim->mSimIsRunning ? ImVec4(0, 0.5, 0, 1) : ImVec4(0.5, 0, mSyncFlag ? 0.5 : 0, 1));
        ImGui::SameLine();
        fakeButton("Stav Vlákna 2", mSecondarySim->mThreadIsRunning ? ImVec4(0, 0.5, 0, 1) : ImVec4(0.5, 0, 0, 1));
        if (mShouldBePaused)
        {
            ImGui::SameLine();
            fakeButton("Pauza", ImVec4(0.5, 0, 0, 1));
        }

        ImGui::Text("");
        ImGui::Text("Simulace 2 %.3f ms (%.1f SPS)", mSecondarySim->mStepTimeMS, 1000.0f /  mSecondarySim->mStepTimeMS);

        ImGui::Text("");
        ImGui::Text("Simulace 2");
        ImGui::Text("Krok  %d", mSecondarySim->mFrame);
        ImGui::Text("Čas  %f/%f", mSecondarySim->mTime, mInitSettCopy.mEndTime);
        ImGui::Text("Energie  %.3e", mSecondarySim->mEnergy);
        ImGui::Text("Objem    %.3e", 0.);

        ImGui::End();
    }

    showStatWindow(true);

    showCameraWindow();

    showEditorWindow();

}
