/**
 * @file      ShaderBuffer.h
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Hlavičkový soubor definující a implementující třídu ShaderBuffer.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SHADER_BUFFER_H
#define SHADER_BUFFER_H

#include <iostream>
#include <vector>

// #include <GL/glew.h>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>

#include "../zaklad/TError.h"

/**
 * @brief Třída zapouzdřující buffery pro OpenGL.
 */
template<class T, class C>
class ShaderBuffer
{
    public:
        ShaderBuffer(std::vector<T> * ver, std::vector<C> * col, std::vector<unsigned int> * ind = NULL)
        {
            lateConstruct(ver, col, ind);
        }
        ShaderBuffer()
        {

        }
        ~ShaderBuffer()
        {

        }

        void lateConstruct(std::vector<T> * ver, std::vector<C> * col, std::vector<unsigned int> * ind = NULL)
        {
            mVertices = ver;
            mColors = col;
            mIndices = ind;
            mSizeofT = sizeof(T);
            mSizeofC = sizeof(C);
        }

        void init()
        {
            if (!mVertices)
            {
                throw QTError("ShaderBuffer::init: mVertices == NULL");
            }

            glGenVertexArrays(1, &mVAO);
            glBindVertexArray(mVAO);

            glGenBuffers(2, mVBO);
            glBindBuffer(GL_ARRAY_BUFFER, mVBO[0]);

            glGenBuffers(1, &mEBO);

            copyVboData();

            if (mIndices)
            {
                glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mEBO);
                glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * mIndices->size(), mIndices->data(), GL_STATIC_DRAW);
            }
        }

        void atribut(GLuint atSize)
        {
            glBindVertexArray(mVAO);
            glBindBuffer(GL_ARRAY_BUFFER, mVBO[0]);
            glEnableVertexAttribArray(mAtIdx);
            glVertexAttribPointer(mAtIdx, atSize, GL_FLOAT, GL_FALSE, sizeof(T), (GLuint*) 0);

            ++mAtIdx;
        }
        void atributColor(GLuint atSize)
        {
            glBindVertexArray(mVAO);
            glBindBuffer(GL_ARRAY_BUFFER, mVBO[1]);
            glEnableVertexAttribArray(mAtIdx);
            glVertexAttribPointer(mAtIdx, atSize, GL_FLOAT, GL_FALSE, sizeof(C), (GLuint*) 0);

            ++mAtIdx;
        }

        void copyVboData()
        {
            glBindBuffer(GL_ARRAY_BUFFER, mVBO[0]);
            glBufferData(GL_ARRAY_BUFFER, mVertices->size() * mSizeofT, mVertices->data(), GL_DYNAMIC_DRAW);
            glBindBuffer(GL_ARRAY_BUFFER, mVBO[1]);
            glBufferData(GL_ARRAY_BUFFER, mColors->size() * mSizeofC, mColors->data(), GL_DYNAMIC_DRAW);
        }

        void copyVboColors()
        {
            glBindBuffer(GL_ARRAY_BUFFER, mVBO[1]);
            glBufferData(GL_ARRAY_BUFFER, mColors->size() * mSizeofC, mColors->data(), GL_DYNAMIC_DRAW);
        }

        void bindVertexArray()
        {
            glBindVertexArray(mVAO);
        }

        GLuint mVAO = 0;
        GLuint mVBO[2];
        GLuint mEBO;

        GLuint mAtIdx = 0;

        unsigned int mSizeofT;
        unsigned int mSizeofC;

        std::vector<T>* mVertices = NULL;
        std::vector<C>* mColors = NULL;
        std::vector<unsigned int>* mIndices = NULL;
};

#endif
