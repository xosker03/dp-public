/**
 * @file      GLEngineSimControll.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Soubor implementující třídu GLEngine.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include "GLEngine.h"

/**
 * @brief Vygeneruje instance simulací podle výberu a spustí je
 * 
 */
void GLEngine::startSims()
{
    mSyncFlag = mNextSyncFlag;

    delete mSim;
    mSim = NULL;

    mSim = genSimulation(mSimSelect);
    mSim->init(mInitSettCopy, mLiveSettCopy, mSimDesc, false, false, mSimFile);
    // mVertices.resize(mSim->mCubeSize); //TODO pořádně promyslet
    // mSim->mPauseOnFrame = 1000; //TODO odstranit
    if (mSyncFlag == eFullSync)
    {
        mSim->mPauseEveryFrame = true;
    }
    else
    {
        mSim->mPauseEveryFrame = false;
    }
    if (mShouldBePaused)
    {
        (void) mSim->mPauseMutex.try_lock();
    }
    mSim->mPauseOnFrame = mSimsTargetFrame;
    mSim->asyncStart(mProgramState == eBenchScreen);

    delete mSecondarySim;
    mSecondarySim = NULL;

    mSecondarySim = genSimulation(mSecondarySimSelect); //FIXME vyžaduje ošetření
    mSecondarySim->init(mInitSettCopy, mLiveSettCopy, mSecondarySimDesc, false, false, mSecondarySimFile);
    // mSecondarySim->mPauseOnFrame = 1000; //TODO odstranit
    if (mSyncFlag == eFullSync)
    {
        mSecondarySim->mPauseEveryFrame = true;
    }
    else
    {
        mSecondarySim->mPauseEveryFrame = false;
    }
    if (mSyncFlag != eOneWaySync)
    {
        if (mShouldBePaused)
        {
            (void) mSecondarySim->mPauseMutex.try_lock();
        }
        mSecondarySim->mPauseOnFrame = mSimsTargetFrame;
        mSecondarySim->asyncStart(mProgramState == eBenchScreen);
    }

    mShouldBeStopped = false;
}

/**
 * @brief Zastaví simulace
 * 
 */
void GLEngine::stopSims()
{
    if (mSim->mThreadIsRunning)
    {
        mSim->asyncStop();
        while (mSim->mThreadIsRunning);
    }

    if (mSecondarySim->mThreadIsRunning)
    {
        mSecondarySim->asyncStop();
        while (mSecondarySim->mThreadIsRunning);
    }

    delete mSim;
    mSim = genSimulation(Simulation::eSimEmpty);
    delete mSecondarySim;
    mSecondarySim = genSimulation(Simulation::eSimEmpty);

    mShouldBeStopped = true;
}

/**
 * @brief Restartuje simulace
 * 
 */
void GLEngine::restartSims()
{
    stopSims();
    startSims();
}

/**
 * @brief Pozastaví simulace
 * 
 */
void GLEngine::pauseSims()
{
    if (!mShouldBePaused)
    {
        return;
    }

    mSim->asyncPause();
    mSecondarySim->asyncPause();
}

/**
 * @brief Obnoví simulace
 * 
 */
void GLEngine::resumeSims()
{
    if (mShouldBePaused)
    {
        return;
    }

    mSim->asyncResume();
    mSecondarySim->asyncResume();

}
