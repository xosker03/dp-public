/**
 * @file      zaklad.h
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Hlavičkový soubor definující barevný výstup.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef ZAKLAD_H
#define ZAKLAD_H

/* Použitelná Marka
 *
 * ZAKLAD_NO_OUTPUT
 * ZAKLAD_TO_STDERR
 * ZAKLAD_NO_COLORS
 *
 * ZAKLAD_NO_MUTEX
 * ZAKLAD_NO_FLUSH
 *
 */

#include <string>

#ifndef ZAKLAD_NO_OUTPUT
    #ifndef ZAKLAD_TO_STDERR
        #define printd(...) fprintf(stdout, __VA_ARGS__)
    #else
        #define printd(...) fprintf(stderr, __VA_ARGS__)
    #endif
#else
    #define printd(...) voidfun()
#endif

/**
 * @brief Struktura obsahující barvy pro shell.
 */
struct bcolors
{
    const char *BLUE;
    const char *GREEN;
    const char *RED;
    const char *VIOLET;
    const char *YELLOW;
    const char *CYAN;
    const char *DBLUE;
    const char *DGREEN;
    const char *DRED;
    const char *DVIOLET;
    const char *DYELLOW;
    const char *DCYAN;
    const char *WHITE;
    const char *DGRAY;
    const char *GRAY;
    const char *BLACK;
    const char *BOLD;
    const char *UNDERLINE;
    const char *CRITIC;
    const char *ENDC;
    const char *CURSORH;
    const char *CURSORS;
    const char *chyba;
};

extern struct bcolors bc;

#define printblue(...)      printc(bc.BLUE, __VA_ARGS__)
#define printgreen(...)     printc(bc.GREEN, __VA_ARGS__)
#define printred(...)       printc(bc.RED, __VA_ARGS__)
#define printviolet(...)    printc(bc.VIOLET, __VA_ARGS__)
#define printyellow(...)    printc(bc.YELLOW, __VA_ARGS__)
#define printcyan(...)      printc(bc.CYAN, __VA_ARGS__)

#define printdblue(...)     printc(bc.DBLUE, __VA_ARGS__)
#define printdgreen(...)    printc(bc.DGREEN, __VA_ARGS__)
#define printdred(...)      printc(bc.DRED, __VA_ARGS__)
#define printdviolet(...)   printc(bc.DVIOLET, __VA_ARGS__)
#define printdyellow(...)   printc(bc.DYELLOW, __VA_ARGS__)
#define printdcyan(...)     printc(bc.DCYAN, __VA_ARGS__)

#define printcritic(...)    printc(bc.CRITIC, __VA_ARGS__)
#define printchyba(...)     printc(bc.chyba, __VA_ARGS__)

bool printif(bool p);
bool printif(bool p, const char * data, ...);

void printc(const char * color);
void printc(const char * color, const char * data, ...);

void printendc();
int voidfun();

#endif
