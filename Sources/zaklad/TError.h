/**
 * @file      TError.h
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Hlavičkový soubor definující třídu TError.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TERROR_H
#define TERROR_H

#include <string>

#define QTError(text) TError(text, __FILE__, __LINE__)

class TError : std::exception
{
    public:
        TError(std::string text, const char* file = "", const int line = -1) noexcept;
        ~TError();

        const char* what() const noexcept override;

        std::string mText = "TError: Neznámá chyba";
        std::string mFile;
        int mLine;
};

#endif
