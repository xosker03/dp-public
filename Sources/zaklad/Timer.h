/**
 * @file      Timer.h
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Hlavičkový soubor definující třídu Timer.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef TIMER_H
#define TIMER_H

#include <sys/time.h>
#include <cstddef>

/**
 * @brief Třída sloužící k měření reálného času.
 */
class Timer
{
    public:
        Timer() {}
        ~Timer() {}

        inline void start()
        {
            gettimeofday(&mStart, NULL);
        }
        inline void stop()
        {
            gettimeofday(&mStop, NULL);
        }

        inline unsigned long us()
        {
            return ((1000000 * mStop.tv_sec + mStop.tv_usec) - (1000000 * mStart.tv_sec + mStart.tv_usec));
        }
        inline double ms()
        {
            return us() / 1000.0;
        }
        inline double s()
        {
            return ms() / 1000.0;
        }

        struct timeval mStart;
        struct timeval mStop;
};

#endif
