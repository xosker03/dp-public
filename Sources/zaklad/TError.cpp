/**
 * @file      Terror.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Soubor implementující třídu Terror.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include "TError.h"

#include <stdio.h>
#include <execinfo.h>

#include "zaklad.h"

#define TERROR_BACKTRACE
#define BACKTRACE_MAX_SIZE 20

static const char * terrorError = "TError::what: Neznámá chyba";

TError::TError(std::string text, const char* file, const int line) noexcept : std::exception()
{
    try
    {
        mText = text;
    }
    catch (...)
    {

    }
    try
    {
        mFile = std::string(file);
        mLine = line;
    }
    catch (...)
    {

    }
    try
    {
        fprintf(stderr, "%sCHYBA:%s ", bc.CRITIC, bc.ENDC);
        fprintf(stderr, "%s%s%s\n", bc.RED, text.c_str(), bc.ENDC);
        if (line != -1)
        {
            fprintf(stderr, "\t%sline: %d; file: %s%s\n", bc.CYAN, line, file, bc.ENDC);
        }
    }
    catch (...)
    {

    }
    #ifdef TERROR_BACKTRACE
    try
    {
        size_t size;
        char **strings;
        void *array[BACKTRACE_MAX_SIZE];

        size = backtrace(array, BACKTRACE_MAX_SIZE);
        strings = backtrace_symbols(array, size);

        for (int i = 0; i < size; i++)
        {
            fprintf(stderr, "%s\t%s%s\n", bc.BLUE, strings[i], bc.ENDC);
        }
    }
    catch (...)
    {

    }
    #endif
}

TError::~TError()
{

}

const char * TError::what() const noexcept
{
    try
    {
        return mText.c_str();
    }
    catch (...)
    {
        return terrorError;
    }
}
