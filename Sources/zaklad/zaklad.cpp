/**
 * @file      zaklad.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Soubor implementující barevný výstup.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include "zaklad.h"
#include <stdio.h>
#include <stdarg.h>

#include <mutex>

#ifndef ZAKLAD_TO_STDERR
    #define OUTPUT stdout
#else
    #define OUTPUT stderr
#endif

#ifndef ZAKLAD_NO_MUTEX
    #define masterlock() master_lock.lock()
    #define masterunlock() master_lock.unlock()
#else
    #define masterlock() voidfun()
    #define masterunlock() voidfun()
#endif

#ifndef ZAKLAD_NO_FLUSH
    #define flush_output() fflush(OUTPUT)
#else
    #define flush_output() voidfun()
#endif

#ifndef ZAKLAD_NO_COLORS

struct bcolors bc =
{
    .BLUE = "\033[94m",
    .GREEN = "\033[92m",
    .RED = "\033[91m",
    .VIOLET = "\033[95m",
    .YELLOW = "\033[93m",
    .CYAN = "\033[96m",
    .DBLUE = "\033[34m",
    .DGREEN = "\033[32m",
    .DRED = "\033[31m",
    .DVIOLET = "\033[35m",
    .DYELLOW = "\033[33m",
    .DCYAN = "\033[36m",
    .WHITE = "\033[97m",
    .DGRAY = "\033[90m",
    .GRAY = "\033[37m",
    .BLACK = "\033[30m",
    .BOLD = "\033[1m",
    .UNDERLINE = "\033[4m",
    .CRITIC = "\033[41;1m",
    .ENDC = "\033[0m",
    .CURSORH = "\033[?25l",
    .CURSORS = "\033[?25h",
    .chyba = "\033[91mChyba: \033[0m"
};

#else

struct bcolors bc =
{
    .BLUE = "",
    .GREEN = "",
    .RED = "",
    .VIOLET = "",
    .YELLOW = "",
    .CYAN = "",
    .DBLUE = "",
    .DGREEN = "",
    .DRED = "",
    .DVIOLET = "",
    .DYELLOW = "",
    .DCYAN = "",
    .WHITE = "",
    .DGRAY = "",
    .GRAY = "",
    .BLACK = "",
    .BOLD = "",
    .UNDERLINE = "",
    .CRITIC = "",
    .ENDC = "",
    .CURSORH = "",
    .CURSORS = "",
    .chyba = "Chyba: "
};

#endif

static std::mutex master_lock;

bool printif(bool p)
{
    #ifndef ZAKLAD_NO_OUTPUT
    masterlock();
    if (p)
    {
        fprintf(OUTPUT, "%s", bc.GREEN);
    }
    else
    {
        fprintf(OUTPUT, "%s", bc.RED);
    }
    flush_output();
    masterunlock();
    #endif
    return p;
}
bool printif(bool p, const char * data, ...)
{
    #ifndef ZAKLAD_NO_OUTPUT
    masterlock();
    va_list args;
    va_start(args, data);
    if (p)
    {
        fprintf(OUTPUT, "%s", bc.GREEN);
    }
    else
    {
        fprintf(OUTPUT, "%s", bc.RED);
    }
    vfprintf(OUTPUT, data, args);
    fprintf(OUTPUT, "%s", bc.ENDC);
    va_end(args);
    flush_output();
    masterunlock();
    #endif
    return p;
}
void printc(const char * color)
{
    #ifndef ZAKLAD_NO_OUTPUT
    masterlock();
    fprintf(OUTPUT, "%s", color);
    flush_output();
    masterunlock();
    #endif
}
void printc(const char * color, const char * data, ...)
{
    #ifndef ZAKLAD_NO_OUTPUT
    masterlock();
    va_list args;
    va_start(args, data);
    fprintf(OUTPUT, "%s", color);
    vfprintf(OUTPUT, data, args);
    fprintf(OUTPUT, "%s", bc.ENDC);
    va_end(args);
    flush_output();
    masterunlock();
    #endif
}

void printendc()
{
    #ifndef ZAKLAD_NO_OUTPUT
    masterlock();
    fprintf(OUTPUT, "%s", bc.ENDC);
    flush_output();
    masterunlock();
    #endif
}
int voidfun()
{
    return 0;
}
