/**
 * @file      floatV.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Implementuje self testy pro strukturu FloatV.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include "TypeV.h"

int floatVSelfTest()
{
    printc(bc.YELLOW, "Self Test: floatV\t");
    int ret = 0;
    bool test;

    {
        test = sizeof(FloatV) == 12;
        ret += !test;
        printif(test, " sizeof ");
    }

    {
        FloatV a;
        test = a.mX == 0 && a.mY == 0 && a.mZ == 0;
        ret += !test;
        printif(test, " zero ");
    }

    {
        FloatV a(1, 2, 3);
        test = a.mX == 1.0 && a.mY == 2.0 && a.mZ == 3.0;
        ret += !test;
        printif(test, " init ");
    }

    {
        FloatV a(1, 2, 3);
        FloatV b(a);
        test = b.mX == 1.0 && b.mY == 2.0 && b.mZ == 3.0;
        ret += !test;
        printif(test, " cpy ");
    }

    {
        FloatV a(1, 2, 3);
        FloatV c;
        c = a;
        test = c.mX == 1.0 && c.mY == 2.0 && c.mZ == 3.0;
        ret += !test;
        printif(test, " = ");
    }

    {
        FloatV a(1, 2, 3);
        FloatV c(1, 2, 3);
        test = c == a;
        ret += !test;
        printif(test, " == ");
    }

    {
        FloatV a(1, 2, 3);
        FloatV b(1, 2, 3);
        FloatV c;
        FloatV R(3, 6, 9);
        c = a + b;
        c += a;
        test = c == R;
        ret += !test;
        printif(test, " ++= ");
    }

    {
        FloatV a(1, 2, 3);
        FloatV b(1, 2, 3);
        FloatV c(3, 6, 9);
        FloatV R(-1, -2, -3);
        c = a - b;
        c -= a;
        test = c == R;
        ret += !test;
        printif(test, " --= ");
    }

    {
        FloatV a(1, 2, 3);
        FloatV c;
        FloatV R(1 * 8, 2 * 8, 3 * 8);
        c = a * 2;
        c += 2 * a;
        c *= 2;
        test = c == R;
        ret += !test;
        printif(test, " ***= ");
    }

    {
        FloatV a(1 * 8, 2 * 8, 3 * 8);
        FloatV c;
        FloatV R(1, 2, 3);
        c = a / 8;
        test = c == R;
        ret += !test;
        printif(test, " / ");
    }

    {
        FloatV a(1, 2, 3);
        FloatV b(2, 1, -1);
        FloatV c;
        FloatV R(-5, 7, -3);
        c = a ^ b;
        test = c == R;
        ret += !test;
        printif(test, " cross ");
    }

    {
        FloatV a(1, 2, 1);
        FloatV b(2, 1, -1);
        float c;
        float R = 3;
        c = a & b;
        test = c == R;
        ret += !test;
        printif(test, " scalar ");
    }

    {
        FloatV a(1, 2, 3);
        float R1 = 3.741;
        float R2 = 3.742;
        test = a.len() > R1 && a.len() < R2;
        ret += !test;
        printif(test, " len ");
    }

    {
        FloatV a(1, 2, 3);
        float R1 = 14;
        test = a.lenSq() == R1;
        ret += !test;
        printif(test, " lenSq ");
    }

    {
        FloatV a(1, 2, 3);
        FloatV b(2, 1, -1);
        FloatV c;
        FloatV R(-5, 7, -3);
        c = (a + b) ^ (b * (a & b));
        test = c == R;
        ret += !test;
        printif(test, " FINAL ");
    }

    printf("\n");
    return ret;
}
