/**
 * @file      Storage.h
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Hlavičkový soubor definující třídu Storage.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef STORAGE_H
#define STORAGE_H

#include <mutex>
#include <tuple>
#include <H5Cpp.h>

#include "Particle.h"
#include "ParticleArray.h"

#include "SimSettings.h"

class Storage
{
    public:
        struct Frame
        {
            int mFrameNum = 0;
            float mSimTime = 0;
            float mStepMS = 0;
            int mArraySize = 0;
            float mEnergy = 0;
            float mVolume = 0;
        };
        using PackedFrame = std::tuple<Frame&, std::vector<FloatV>&, std::vector<FloatV>&>;

        /// Konstruktor
        Storage(std::string name);
        Storage(std::string name, bool readFlag, bool benchFlag, std::string fileName, bool specialFlag = false);
        /// Destruktor
        ~Storage();
        /// Kopírovací konstruktor. Není povolen.
        Storage(const Storage& other) = delete;
        /// Přesouvací konstruktor. Není povolen.
        Storage(Storage&& other) noexcept = delete;
        /// Opeátor přiřazení. Není povolen.
        Storage& operator=(const Storage& other) = delete;
        /// Operátor přiřazení pomocí přesunutí. Není povolen.
        Storage& operator=(Storage&& other) noexcept = delete;

        void storeSettings(LiveSettings live, InitSettings init);
        void storeFrame(Frame f, FloatV* posArray, FloatV* colorsArray);

        void loadSettings(LiveSettings& live, InitSettings& init);
        Frame loadFrame(int frameNum);

        PackedFrame lockLatestFrame();
        void unLockLatestFrame();

        PackedFrame lockFrame(int frameNum);
        void unLockFrame();

        std::vector<float> getStats();

        void save();
        std::vector<std::string> listGroups();

    protected:

        void openHDF5File(bool specialFlag = false);
        void closeHDF5File();

        void createDataset(const std::string& datasetName);

        void writeDataset(const std::string& datasetName, unsigned long size, FloatV* data);
        void readDataset(const std::string& datasetName, unsigned long  size, FloatV* data);

        void writeInitSettingsAttribute(const InitSettings& attributeValue);
        void writeLiveSettingsAttribute(const LiveSettings& attributeValue);
        void writeFrameAttribute(const std::string& datasetName, const Frame& attributeValue);

        void readLiveSettingsAttribute(LiveSettings& attributeValue);
        void readInitSettingsAttribute(InitSettings& attributeValue);
        void readFrameAttribute(const std::string& datasetName, Frame& attributeValue);

        std::string mName;
        bool mReadFlag = false;
        bool mBenchFlag = false;
        std::string mFileName = "";

        bool mFileOpenFlag = false;
        std::mutex latestFrameLock;
        H5::H5File mFile;

        Frame mLastFrame;
        std::vector<FloatV> mLastPosArray;
        std::vector<FloatV> mLastColorsArray;

        H5::ArrayType mFloatVType;
        H5::CompType mLiveSettingsType;
        H5::CompType mInitSettingsType;
        H5::CompType mFrameType;

        std::string mGroupName;
        H5::Group mGroup;

        std::vector<float> mStats;
};

#endif
