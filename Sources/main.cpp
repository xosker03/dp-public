/**
 * @file      main.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Hlavní soubor programu BS4GC (Benchmark Suite for Graphic Cards).
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC
 * If not, see <https://www.gnu.org/licenses/>.
 */

/*! \mainpage Benchmark Suite for Graphic Cards (BS4GC)
 *
 * \section intro_sec Introduction
 *
 * This is the introduction.
 *
 * \section build_sec Build
 *
 * see Readme.md for now
 *
 * \subsection step1 Step 1: Opening the box
 *
 * etc...
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#include <omp.h>

#include <iostream>
#include <string>
#include <tuple>
#include <vector>
#include <thread>

#include "zaklad/zaklad.h"
#include "GUI/GLEngine.h"

#include "Storage.h"
#include "SimSettings.h"

#include "Registrator.h"

using namespace std;

/**
 * Hlavní funkce programu BS4GC.
 *
 * @param [in] argc - Počet vstupních parametrů.
 * @param [in] argv - Vstupní parametry.
 * @return 0 - Pokud program skončil správně.
 */
int main(int argc, char** argv)
{
    printc(bc.BLUE, "Jader: %d, dynamic: %d, max vláken %d, vláken %d\n", omp_get_num_procs(), omp_get_dynamic(), omp_get_max_threads(), omp_get_num_threads());
    omp_set_num_threads(omp_get_num_procs());

    printblue("FloatV velikost: %d bajtů\n", sizeof(FloatV));

    int ret = floatVSelfTest();
    if (ret)
    {
        printc(bc.CRITIC, "Testy selhaly%s\n", bc.ENDC);
    }

    Registrator::initModules();
    for (auto& m : Registrator::getModules())
    {
        printc(bc.GREEN, "Modul: %s\n", m.mName.c_str());
    }

    printc(bc.CYAN, "Inicializace simulace\n");

    // Hlavní část programu
    // Inicializace grafického enginu
    GLEngine gE;
    // Spuštění hlavní smyčky grafického enginu, který aktuálně ovládá běh simulace.
    gE.mainLoop();

    printf("KONEC\n");
    return 0;
}
