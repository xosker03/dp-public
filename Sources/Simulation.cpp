/**
 * @file      Simulation.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Soubor implementující třídu Simulation.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include "Simulation.h"

#include <unistd.h>

#include <iostream>
#include <algorithm>

#include "zaklad/zaklad.h"

#include "Registrator.h"

/**
 * @brief Konstruktor.
 */
Simulation::Simulation()
{
    mStorage = new Storage("NoFile");
}

/**
 * @brief Destruktor.
 */
Simulation::~Simulation()
{
    if (mThreadIsRunning)
    {
        asyncStop();
    }
    if (mParticles)
    {
        delete mParticles;
        mParticles = NULL;
    }
    if (mStorage)
    {
        delete mStorage;
    }
}

/**
 * @brief Nastaví živé parametry simulace.
 * @param liveSett - Nastavení simulace.
 */
void Simulation::set(LiveSettings liveSett)
{
    mLiveSett = liveSett;
}

/**
 * @brief Inicializace a nastaví počátečních parametů simulace.
 * @param initSett - Počáteční nastavení simulace.
 */
void Simulation::init(InitSettings initSett, LiveSettings liveSett)
{
    mInitSett = initSett;
    mLiveSett = liveSett;
    init();
}

void Simulation::init(InitSettings initSett, LiveSettings liveSett, std::string name, bool readFlag, bool benchFlag, std::string fileName)
{
    if (fileName != "")
    {
        delete mStorage;
        mStorage = new Storage(simName() + (name != "" ? "_" + name : ""), false, benchFlag, fileName);
    }
    init(initSett, liveSett);
}

/**
 * @brief Inicializace simulace.
 */
void Simulation::init()
{
    mCubeSize = mInitSett.mCubeSide * mInitSett.mCubeSide * mInitSett.mCubeSide;
    mCenter = (mLiveSett.mWallL + mLiveSett.mWallR) / 2;

    if (mParticles)
    {
        delete mParticles;
    }
    mParticles = new ParticleArray(mCubeSize);

    FloatV pos = FloatV(0.4f, 0.4f, 0.2f);
    float mass = 1;
    int idx = 0;

    for (int k = 0; k < mInitSett.mCubeSide; ++k)
    {
        for (int j = 0; j < mInitSett.mCubeSide; ++j)
        {
            for (int i = 0; i < mInitSett.mCubeSide; ++i)
            {
                Particle p(
                    pos + FloatV(i * mLiveSett.mSpacing, j * mLiveSett.mSpacing, k * mLiveSett.mSpacing),
                    mInitSett.mInitialSpeed,// + FloatV(i/5.0,j/5.0,k/5.0),
                    mass
                );
                mParticles->set(idx++, p);
                mAltParticles.push_back(p);
            }
        }
    }
    mTime = mInitSett.mTime;

    mStorage->storeSettings(mLiveSett, mInitSett);
}

void Simulation::asyncStart(bool catchAll)
{
    if (mSimThread)
    {
        throw QTError("Simulation::asyncStart: Simulace již běží");
    }

    // mThreadIsRunning = true;
    mSimIsRunning = true;

    if (catchAll)
    {
        mSimThread = new std::thread(&Simulation::userNoExceptRun, this);
    }
    else
    {
        mSimThread = new std::thread(&Simulation::userRun, this);
    }
}

void Simulation::asyncPause()
{
    (void) mPauseMutex.try_lock();
}

void Simulation::asyncResume()
{
    mPauseMutex.unlock();
}

void Simulation::asyncStop()
{
    mPauseEveryFrame = false;
    mPauseOnFrame = -1;

    mStopRequestedFlag = true;
    asyncResume();
    mSimThread->join();
    delete mSimThread;
    mSimThread = NULL;
    mStopRequestedFlag = false;
}

void Simulation::pauseIfRequested()
{
    if (mFrame == mPauseOnFrame)
    {
        (void) mPauseMutex.try_lock();
    }
    if (mPauseEveryFrame)
    {
        (void) mPauseMutex.try_lock();
    }
    mSimIsRunning = false;
    mPauseMutex.lock();
    mPauseMutex.unlock();
    mSimIsRunning = true;
}

bool Simulation::userStopRequested()
{
    if (mStopRequestedFlag)
    {
        return true;
    }
    return false;
}

void Simulation::userNoExceptRun()
{
    try
    {
        userRun();
    }
    catch (const std::exception& e)
    {
        mThreadIsRunning = false;
        std::cerr << "Simulation::userRun: Vyjímka: " << e.what() << std::endl;
    }
    catch (...)
    {
        mThreadIsRunning = false;
        std::cerr << "Simulation::userRun: Neznámá vyjímka" << std::endl;
    }
}

void Simulation::storeFrame()
{
    Storage::Frame f;
    f.mArraySize = mCubeSize;
    f.mEnergy = mEnergy;
    f.mFrameNum = mFrame;
    f.mSimTime = mTime;
    f.mStepMS = mStepTimeMS;
    f.mVolume = mVolume;
    mStorage->storeFrame(f, mParticles->mPosArray, mParticles->mColorsArray);
}

void Simulation::loadFrame(int frameNum)
{
    // if(mFrame == frameNum)
    //     return;
    Storage::Frame f = mStorage->loadFrame(frameNum);
    mCubeSize = f.mArraySize;
    mEnergy = f.mEnergy;
    mFrame = f.mFrameNum;
    mTime = f.mSimTime;
    mVolume = f.mVolume;
}

void Simulation::closeStorage()
{
    mStorage->save();
}


void Simulation::userSignalStart()
{
    mSimIsRunning = true;
    mThreadIsRunning = true;
}

void Simulation::userSignalStepBegin()
{
    if(mFirstStep)
    {
        mSimTimer.start();
        mFirstStep = false;
    }
}

void Simulation::userSignalStepEnd()
{
    mSimTimer.stop();
    mStepTimeMS = mSimTimer.ms();
    mSimTimer.start();
    storeFrame();
    pauseIfRequested();
    mFrame += 1;
    if(!mBenchFlag)
    {
        mSimTimer.start();
    }
}

void Simulation::userSignalFinish()
{
    closeStorage();
    mSimIsRunning = false;
    mThreadIsRunning = false;
}


REGISTER_SIMULATIUON(Simulation, "SimEmpty", Simulation::eSimEmpty, "Nic nedělající simulace.");
