/**
 * @file      SimCPU.h
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Hlavičkový soubor definující třídu SimCPU.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef SIMCPU_H
#define SIMCPU_H

#include <vector>
#include <tuple>

#include "../../zaklad/Timer.h"

#include "../../Particle.h"
#include "../../ParticleArray.h"

#include "../../SimSettings.h"
#include "../../Simulation.h"

/**
 * @class SimCPU
 * @brief Třída reprezentující simulaci.
 *
 * Tato třída simuluje měkou kostku tvořenou částicemi pomocí gradientního sestupu.
 */
class SimCPU : public Simulation
{
    public:
        /// Konstruktor.
        SimCPU();
        /// Destruktor.
        ~SimCPU();

        /// Kopírovací konstruktor. Není povolen.
        SimCPU(const SimCPU& other) = delete;
        /// Přesouvací konstruktor. Není povolen.
        SimCPU(SimCPU&& other) noexcept = delete;

        /// Opeátor přiřazení. Není povolen.
        SimCPU& operator=(const SimCPU& other) = delete;
        /// Operátor přiřazení pomocí přesunutí. Není povolen.
        SimCPU& operator=(SimCPU&& other) noexcept = delete;

        //spuštění simulace
        void userRun() override;
        //krok simulace
        bool userStep() override;
};

#endif // SIMULATION_H
