/**
 * @file      SimCPU.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Soubor implementující třídu SimCPU.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include "SimCPU.h"

#include <unistd.h>

#include <iostream>
#include <algorithm>

#include "../../zaklad/zaklad.h"

#include "../../Registrator.h"

/**
 * @brief Konstruktor.
 */
SimCPU::SimCPU() : Simulation()
{

}

/**
 * @brief Destruktor.
 */
SimCPU::~SimCPU()
{

}

/**
 * @brief Samostnaný běh simulace.
 */
void SimCPU::userRun()
{
    userSignalStart();
    while (mTime < mInitSett.mEndTime)
    {
        userSignalStepBegin();
        userStep();
        userSignalStepEnd();
        if (userStopRequested())
        {
            break;
        }
    }
    userSignalFinish();
}

/**
 * @brief Vypočítá vzdálenost mezi dvěma částicemi. Po odečtení očekávané vzdálenosti částic získáme chybu omezení.
 * @param X - Pozice první částice.
 * @param Y - Pozice druhé částice.
 * @return Vzdálenost mezi částicemi.
 */
static inline float computeConstrain(FloatV X, FloatV Y)
{
    float len = (Y - X).len();
    return len;
}

/**
 * @brief Vypočítá gradient chyby polohy částice, tzn. kterým směrem když se posune částice se chyba omezení zvětší nejvíce.
 * @param X - Pozice první částice.
 * @param Y - Pozice druhé částice.
 * @return směr největší chyby.
 */
static inline FloatV computeConstrainGradient(FloatV X, FloatV Y)
{
    float lnSq = (Y - X).lenSq();
    return (lnSq != 0.0) ? ((Y - X) / sqrt(lnSq)) : FloatV();
}

/**
 * @brief Převede globální index na lokální souřadnice.
 * @param i - Globální index.
 * @param size - Velikost hrany prostoru.
 * @return Lokální souřadnice.
 */
static inline std::tuple<int, int, int> fromGId(int i, int size)
{
    int z = i / (size * size);
    int y = (i % (size * size)) / size;
    int x = i % size;
    return std::tuple<int, int, int>(x, y, z);
}

/**
 * @brief Převede lokální souřadnice na globální index.
 * @param x - Lokální souřadnice v ose X.
 * @param y - Lokální souřadnice v ose Y.
 * @param z - Lokální souřadnice v ose Z.
 * @param size - Velikost hrany prostoru.
 * @return Globální index.
 */
static inline int toGId(int x, int y, int z, int size)
{
    return x + y * size + z * size * size;
}

/**
 * @brief Provede jeden krok simulace.
 * @return True pokud je možné udělat další krok, tzn. aktuální čas simulace je menší než konečný.
 */
bool SimCPU::userStep()
{
    //krok Eulerovy metody pro posun částice v prostoru
    for (int i = 0 ; i < mParticles->mSize; ++i)
    {
        mParticles->mVelocityArray[i] += mLiveSett.mDT * mLiveSett.mExternalAcceleration + mLiveSett.mDT * mLiveSett.mGravityAccell * mLiveSett.mGravityEnabled;
        mParticles->mPosNewArray[i] = mParticles->mPosArray[i] + mLiveSett.mDT * mParticles->mVelocityArray[i];
    }
    //prohození vstupního a výstupního pole poloh
    mParticles->swapPos();

    //spočtení relaxačního parametru pro aktuální delta t
    float alpha2 = mLiveSett.mAlpha / ((mLiveSett.mDT / mLiveSett.mSubsteps) * (mLiveSett.mDT / mLiveSett.mSubsteps));

    //optimalizace (zjednodušeně vyřešení) omezujících podmínek pro všechny částice pomocí gradientního sestupu
    //optimalizace se provádí v "substep" iteracích
    for (int substep = 0; substep < mLiveSett.mSubsteps; ++substep)
    {
        //omezující podmínka je vzdálenost 2 částic
        //zde se vybere 1 částice pro kterou se vypočítá korekce polohy
        for (int pIdx = 0 ; pIdx < mParticles->mSize; ++pIdx)
        {
            //reálný počet sousedů
            int neighbors = 0;
            //korekce polohy částice
            FloatV correction;
            //poloha částice ve 3 osách
            auto [x, y, z] = fromGId(pIdx, mInitSett.mCubeSide);

            //zde se vybírá sousedská částice figurující v omezující podmínce
            //sousedská částice je v okolí ve tvaru kostky o hraně délky "mParticleNeighbours".
            for (int dx = -mLiveSett.mParticleNeighbours; dx <= mLiveSett.mParticleNeighbours; ++dx)
            {
                for (int dy = -mLiveSett.mParticleNeighbours; dy <= mLiveSett.mParticleNeighbours; ++dy)
                {
                    for (int dz = -mLiveSett.mParticleNeighbours; dz <= mLiveSett.mParticleNeighbours; ++dz)
                    {
                        //pokud soused je mimo pole existujících částit, tzn. neexistuje, tak se přeskočí
                        if (x + dx < 0 || x + dx >= mInitSett.mCubeSide || y + dy < 0 || y + dy >= mInitSett.mCubeSide || z + dz < 0 || z + dz >= mInitSett.mCubeSide)
                        {
                            continue;
                        }
                        //vybraná částice a soused je totožná
                        if (dx == 0 && dy == 0 && dz == 0)
                        {
                            continue;
                        }

                        ++neighbors;

                        int nIdx = toGId(x + dx, y + dy, z + dz, mInitSett.mCubeSide);

                        //chyba omezení
                        float constrainError = 0;
                        //suma čtverců délek gradientů chyby omezení
                        float sumCEGDS = 0;
                        float lambda = 0;

                        //očekáváná vzdálenost 2 částic
                        float distance = (FloatV(dx, dy, dz) * mLiveSett.mSpacing).len();

                        //výpočet chyby omezení
                        constrainError = computeConstrain(mParticles->mPosArray[pIdx], mParticles->mPosArray[nIdx]) - distance;
                        //výpočet gradientu chyby omezení
                        sumCEGDS = 2 + alpha2;
                        lambda = -constrainError / sumCEGDS;
                        //spočítání korekce polohy
                        correction += lambda * computeConstrainGradient(mParticles->mPosArray[pIdx], mParticles->mPosArray[nIdx]);
                    }
                }
            }
            //výpočet nové polohy podle korekce
            //jelikož se původní algoritmus stal lehce nestabilní, tak se porekce dělí počtem sousedů a tím se stabilizuje, ale zpomaluje konvergence.
            mParticles->mPosNewArray[pIdx] = mParticles->mPosArray[pIdx] - (correction * (1.0 / (neighbors)));
        }
        //prohození vstupního a výstupního pole poloh
        mParticles->swapPos();
    }

    //korekce rychlosti částic po korekci polohy
    //TODO popsat proč
    for (int i = 0 ; i < mParticles->mSize; ++i)
    {
        mParticles->mVelocityArray[i] = mParticles->mPosArray[i] - mParticles->mPosStartArray[i];
        mParticles->mVelocityArray[i] *= 1.0 / mLiveSett.mDT;
        mParticles->mPosStartArray[i] = mParticles->mPosArray[i];
    }

    //ošetření hranic simulačního prostoru
    for (int gid = 0 ; gid < mParticles->mSize; ++gid)
    {
        //ošetření hranic simulačního prostoru
        if (mParticles->mPosArray[gid].mX < mLiveSett.mWallL.mX)
        {
            mParticles->mPosArray[gid].mX = mLiveSett.mWallL.mX;
            mParticles->mVelocityArray[gid].mX *= -mLiveSett.mDumping;
            mParticles->mVelocityArray[gid] *= mLiveSett.mDumpingPerpendicular;
        }
        if (mParticles->mPosArray[gid].mX > mLiveSett.mWallR.mX)
        {
            mParticles->mPosArray[gid].mX = mLiveSett.mWallR.mX;
            mParticles->mVelocityArray[gid].mX *= -mLiveSett.mDumping;
            mParticles->mVelocityArray[gid] *= mLiveSett.mDumpingPerpendicular;
        }
        if (mParticles->mPosArray[gid].mY < mLiveSett.mWallL.mY)
        {
            mParticles->mPosArray[gid].mY = mLiveSett.mWallL.mY;
            mParticles->mVelocityArray[gid].mY *= -mLiveSett.mDumping;
            mParticles->mVelocityArray[gid] *= mLiveSett.mDumpingPerpendicular;
        }
        if (mParticles->mPosArray[gid].mY > mLiveSett.mWallR.mY)
        {
            mParticles->mPosArray[gid].mY = mLiveSett.mWallR.mY;
            mParticles->mVelocityArray[gid].mY *= -mLiveSett.mDumping;
            mParticles->mVelocityArray[gid] *= mLiveSett.mDumpingPerpendicular;
        }
        if (mParticles->mPosArray[gid].mZ < mLiveSett.mWallL.mZ)
        {
            mParticles->mPosArray[gid].mZ = mLiveSett.mWallL.mZ;
            mParticles->mVelocityArray[gid].mZ *= -mLiveSett.mDumping;
            mParticles->mVelocityArray[gid] *= mLiveSett.mDumpingPerpendicular;
        }
        if (mParticles->mPosArray[gid].mZ > mLiveSett.mWallR.mZ)
        {
            mParticles->mPosArray[gid].mZ = mLiveSett.mWallR.mZ;
            mParticles->mVelocityArray[gid].mZ *= -mLiveSett.mDumping;
            mParticles->mVelocityArray[gid] *= mLiveSett.mDumpingPerpendicular;
        }
    }

    //spočítání kinetické a potenciální energie systému
    mEnergy = 0;
    for (int i = 0 ; i < mParticles->mSize; ++i)
    {
        float mass = 1;
        mEnergy += mParticles->mVelocityArray[i].len() * mass;
        mEnergy += mass * -mLiveSett.mGravityAccell.mY * mParticles->mPosArray[i].mY;
    }

    //TODO spočítání objemu krychle
    mVolume = 0;

    mTime += mLiveSett.mDT;
    return mTime < mInitSett.mEndTime;
}


REGISTER_SIMULATIUON(SimCPU, "SimCPU", Simulation::eSimCPU, "Referenční procesorová implementace.");
