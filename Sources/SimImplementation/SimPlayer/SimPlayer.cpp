/**
 * @file      SimPlayer.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Soubor implementující třídu SimPlayer.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#include "SimPlayer.h"

#include <unistd.h>

#include <iostream>
#include <algorithm>

#include "../../zaklad/zaklad.h"

#include "../../Registrator.h"

/**
 * @brief Konstruktor.
 */
SimPlayer::SimPlayer() : Simulation()
{

}

/**
 * @brief Destruktor.
 */
SimPlayer::~SimPlayer()
{

}

void SimPlayer::init()
{
    mStorage->loadSettings(mLiveSett, mInitSett);
    loadFrame(0);
}

void SimPlayer::init(InitSettings initSett, LiveSettings liveSett, std::string name, bool readFlag, bool benchFlag, std::string fileName)
{
    if (fileName != "")
    {
        delete mStorage;
        mStorage = new Storage(name, true, benchFlag, fileName);
        // mStorage = new Storage(simName() + (name != "" ? "_" + name : ""), true, benchFlag, fileName);
    }
    init();
}

/**
 * @brief Samostnaný běh simulace.
 */
void SimPlayer::userRun()
{
    bool endThis = false;

    mThreadIsRunning = true;
    while (!endThis)
    {

        try
        {
            loadFrame(mFrame);
        }
        catch (TError t)
        {
            endThis = true;
            // break;
        }
        // userStoreFrame();

        pauseIfRequested();
        mFrame += 1;

        if (userStopRequested() || mTime >= mInitSett.mEndTime)
        {
            endThis = true;
            break;
        }
    }

    userSignalFinish();
}

REGISTER_SIMULATIUON(SimPlayer, "SimPlayer", Simulation::eSimPlayer, "Přehrávač simulací ze souboru.");
