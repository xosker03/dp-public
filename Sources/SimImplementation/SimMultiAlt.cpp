/**
 * @file      SimMultiAlt.cpp
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Soubor implementující třídu Simulation.
 *
 * @version   BS4GC 0.1
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */


#include "../Simulation.h"
#include "../Registrator.h"


//FIXME: Volitelné: Pokud nechcete používat jmenný prostor, tak musíte přejmenovat SimClass na něco unikátního. Jednoduše třeba takto #define SimClass MyAwesomeSimulation.
namespace { // NAMESPACE_BEGIN

//FIXME: Volitelné: Pojmenuj tuto implementaci v GUI: __FILE_NAME__ nahraď za jakýkoliv řetězec, třeba "MojeTřída"
#define SimClassStr __FILE_NAME__



/**
 * @class SimClass
 * @brief Třída reprezentující simulaci.
 *
 * Tato třída simuluje měkou kostku tvořenou částicemi pomocí gradientního sestupu.
 */
class SimClass : public Simulation
{
    public:
        /// Konstruktor.
        SimClass() : Simulation() {};
        /// Destruktor.
        ~SimClass() {};

        /// Kopírovací konstruktor. Není povolen.
        SimClass(const SimClass& other) = delete;
        /// Přesouvací konstruktor. Není povolen.
        SimClass(SimClass&& other) noexcept = delete;

        /// Opeátor přiřazení. Není povolen.
        SimClass& operator=(const SimClass& other) = delete;
        /// Operátor přiřazení pomocí přesunutí. Není povolen.
        SimClass& operator=(SimClass&& other) noexcept = delete;

        //spuštění simulace
        virtual void userRun() override;
        //krok simulace
        virtual bool userStep() override;
};

////////////////////////////////////////////////////////////////////////////////////////



static __always_inline float computeConstrain(FloatV X, FloatV Y)
{
    float len = (Y - X).len();
    return len;
}
static __always_inline FloatV computeConstrainGradient(FloatV X, FloatV Y)
{
    float lnSq = (Y - X).lenSq();
    return (lnSq != 0.0) ? ((Y - X) / sqrt(lnSq)) : FloatV();
}


static __always_inline std::tuple<int, int, int> fromGId(int i, int size)
{
    int z = i / (size * size);
    int y = (i % (size * size)) / size;
    int x = i % size;
    return std::tuple<int, int, int>(x, y, z);
}
static __always_inline int toGId(int x, int y, int z, int size)
{
    return x + y * size + z * size * size;
}




/**
 * @brief Samostnaný běh simulace.
 */
void SimClass::userRun()
{
    /// Singalizuje začátek simulace.
    userSignalStart();

    // Zde je možné inicializovat simulaci. Alolovat paměť atd.
    omp_set_num_threads(omp_get_num_procs());

    while (mTime < mInitSett.mEndTime)
    {
        /// Singalizuje začátek kroku simulace.
        userSignalStepBegin(); 
        
        // Zde je také možné provést nějaké operace před každým krokem simulace.

        userStep();

        // Zde je také možné provést nějaké operace po každém kroku simulace.
        #pragma omp parallel for
        for(int i = 0; i < mParticles->mSize; ++i)
        {
            // mParticles->set(i, mAltParticles[i]);
            mParticles->mPosArray[i] = mAltParticles[i].mPos;
        }

        /// Singalizuje konec kroku simulace.
        userSignalStepEnd();
        /// Kontrola zda-li GUI nežádalo o zastavení simulace.
        if(userStopRequested())
        {
            break;
        }
    }
    /// Singalizuje konec simulace.
    userSignalFinish();
}


/**
 * @brief Provede jeden krok simulace.
 * @return True pokud je možné udělat další krok, tzn. aktuální čas simulace je menší než konečný.
 */
bool SimClass::userStep()
{
    //krok Eulerovy metody pro posun částice v prostoru
    #pragma omp parallel for
    for (int i = 0 ; i < mParticles->mSize; ++i)
    {
        mAltParticles[i].mVelocity += mLiveSett.mDT * mLiveSett.mExternalAcceleration + mLiveSett.mDT * mLiveSett.mGravityAccell * mLiveSett.mGravityEnabled;
        mAltParticles[i].mPosNew = mAltParticles[i].mPos + mLiveSett.mDT * mAltParticles[i].mVelocity;
    }
    //prohození vstupního a výstupního pole poloh
    // mParticles->swapPos();
    for(auto & a : mAltParticles)
    {
        a.mPos = a.mPosNew;
    }

    //spočtení relaxačního parametru pro aktuální delta t
    float alpha2 = mLiveSett.mAlpha / ((mLiveSett.mDT / mLiveSett.mSubsteps) * (mLiveSett.mDT / mLiveSett.mSubsteps));

    //optimalizace (zjednodušeně vyřešení) omezujících podmínek pro všechny částice pomocí gradientního sestupu
    //optimalizace se provádí v "substep" iteracích
    for (int substep = 0; substep < mLiveSett.mSubsteps; ++substep)
    {
        //omezující podmínka je vzdálenost 2 částic
        //zde se vybere 1 částice pro kterou se vypočítá korekce polohy
        #pragma omp parallel for
        for (int pIdx = 0 ; pIdx < mParticles->mSize; ++pIdx)
        {
            //reálný počet sousedů
            int neighbors = 0;
            //korekce polohy částice
            FloatV correction;
            //poloha částice ve 3 osách
            auto [x, y, z] = fromGId(pIdx, mInitSett.mCubeSide);

            //zde se vybírá sousedská částice figurující v omezující podmínce
            //sousedská částice je v okolí ve tvaru kostky o hraně délky "mParticleNeighbours".
            for (int dx = -mLiveSett.mParticleNeighbours; dx <= mLiveSett.mParticleNeighbours; ++dx)
            {
                for (int dy = -mLiveSett.mParticleNeighbours; dy <= mLiveSett.mParticleNeighbours; ++dy)
                {
                    for (int dz = -mLiveSett.mParticleNeighbours; dz <= mLiveSett.mParticleNeighbours; ++dz)
                    {
                        //pokud soused je mimo pole existujících částit, tzn. neexistuje, tak se přeskočí
                        if (x + dx < 0 || x + dx >= mInitSett.mCubeSide || y + dy < 0 || y + dy >= mInitSett.mCubeSide || z + dz < 0 || z + dz >= mInitSett.mCubeSide)
                        {
                            continue;
                        }
                        //vybraná částice a soused je totožná
                        if (dx == 0 && dy == 0 && dz == 0)
                        {
                            continue;
                        }

                        ++neighbors;

                        int nIdx = toGId(x + dx, y + dy, z + dz, mInitSett.mCubeSide);

                        //chyba omezení
                        float constrainError = 0;
                        //suma čtverců délek gradientů chyby omezení
                        float sumCEGDS = 0;
                        float lambda = 0;

                        //očekáváná vzdálenost 2 částic
                        float distance = (FloatV(dx, dy, dz) * mLiveSett.mSpacing).len();

                        //výpočet chyby omezení
                        constrainError = computeConstrain(mAltParticles[pIdx].mPos, mAltParticles[nIdx].mPos) - distance;
                        //výpočet gradientu chyby omezení
                        sumCEGDS = 2 + alpha2;
                        lambda = -constrainError / sumCEGDS;
                        //spočítání korekce polohy
                        correction += lambda * computeConstrainGradient(mAltParticles[pIdx].mPos, mAltParticles[nIdx].mPos);
                    }
                }
            }
            //výpočet nové polohy podle korekce
            //jelikož se původní algoritmus stal lehce nestabilní, tak se porekce dělí počtem sousedů a tím se stabilizuje, ale zpomaluje konvergence.
            mAltParticles[pIdx].mPosNew = mAltParticles[pIdx].mPos - (correction * (1.0 / (neighbors)));
        }
        //prohození vstupního a výstupního pole poloh
        // mParticles->swapPos();
        for(auto & a : mAltParticles)
        {
            a.mPos = a.mPosNew;
        }
    }

    //korekce rychlosti částic po korekci polohy
    //TODO popsat proč
    #pragma omp parallel for
    for (int i = 0 ; i < mParticles->mSize; ++i)
    {
        mAltParticles[i].mVelocity = mAltParticles[i].mPos - mAltParticles[i].mPosStart;
        mAltParticles[i].mVelocity *= 1.0 / mLiveSett.mDT;
        mAltParticles[i].mPosStart = mAltParticles[i].mPos;
    }

    //ošetření hranic simulačního prostoru
    #pragma omp parallel for
    for (int gid = 0 ; gid < mParticles->mSize; ++gid)
    {
        //ošetření hranic simulačního prostoru
        if (mAltParticles[gid].mPos.mX < mLiveSett.mWallL.mX)
        {
            mAltParticles[gid].mPos.mX = mLiveSett.mWallL.mX;
            mAltParticles[gid].mVelocity.mX *= -mLiveSett.mDumping;
            mAltParticles[gid].mVelocity *= mLiveSett.mDumpingPerpendicular;
        }
        if (mAltParticles[gid].mPos.mX > mLiveSett.mWallR.mX)
        {
            mAltParticles[gid].mPos.mX = mLiveSett.mWallR.mX;
            mAltParticles[gid].mVelocity.mX *= -mLiveSett.mDumping;
            mAltParticles[gid].mVelocity *= mLiveSett.mDumpingPerpendicular;
        }
        if (mAltParticles[gid].mPos.mY < mLiveSett.mWallL.mY)
        {
            mAltParticles[gid].mPos.mY = mLiveSett.mWallL.mY;
            mAltParticles[gid].mVelocity.mY *= -mLiveSett.mDumping;
            mAltParticles[gid].mVelocity *= mLiveSett.mDumpingPerpendicular;
        }
        if (mAltParticles[gid].mPos.mY > mLiveSett.mWallR.mY)
        {
            mAltParticles[gid].mPos.mY = mLiveSett.mWallR.mY;
            mAltParticles[gid].mVelocity.mY *= -mLiveSett.mDumping;
            mAltParticles[gid].mVelocity *= mLiveSett.mDumpingPerpendicular;
        }
        if (mAltParticles[gid].mPos.mZ < mLiveSett.mWallL.mZ)
        {
            mAltParticles[gid].mPos.mZ = mLiveSett.mWallL.mZ;
            mAltParticles[gid].mVelocity.mZ *= -mLiveSett.mDumping;
            mAltParticles[gid].mVelocity *= mLiveSett.mDumpingPerpendicular;
        }
        if (mAltParticles[gid].mPos.mZ > mLiveSett.mWallR.mZ)
        {
            mAltParticles[gid].mPos.mZ = mLiveSett.mWallR.mZ;
            mAltParticles[gid].mVelocity.mZ *= -mLiveSett.mDumping;
            mAltParticles[gid].mVelocity *= mLiveSett.mDumpingPerpendicular;
        }
    }

    //spočítání kinetické a potenciální energie systému
    mEnergy = 0;
    #pragma omp parallel for reduction(+:mEnergy)
    for (int i = 0 ; i < mParticles->mSize; ++i)
    {
        float mass = 1.0;
        mEnergy += mAltParticles[i].mVelocity.len() * mass;
        mEnergy += mass * -mLiveSett.mGravityAccell.mY * mAltParticles[i].mPos.mY;
    }

    //TODO spočítání objemu krychle
    mVolume = 0;

    mTime += mLiveSett.mDT;
    return mTime < mInitSett.mEndTime;
}








//FIXME: Pokud se změnil název třídy ze SimClass jinak něž pomocí #define, tak je třeba upravit toto "volání" makra.
//FIXME: Volitelné: Pokud chcete skrýt tuto implementaci v GUI, tak zakomentujte REGISTER_USER_SIMULATIUON().
REGISTER_USER_SIMULATIUON(SimClass, SimClassStr, "");
} //NAMESPACE_END
