/**
 * @file      Particle.h
 *
 * @author    Josef Oškera \n
 *            josef.oskera@seznam.cz
 *
 * @brief     Hlavičkový soubor definující strukturu Particle.
 *
 * @version   BS4GC 1.0
 *
 * @date      08 Feb      2024, 17:07 (created) \n
 *            08 Feb      2024, 17:07 (revised)
 *
 * @copyright Copyright (C) 2024 Josef Oškera.
 *
 * @section License
 * This file is part of BS4GC.
 *
 * BS4GC is free software: you can redistribute it and/or modify it under the terms of
 * the GNU General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or any later version.
 *
 * BS4GC program is distributed in the hope that it will be useful, but WITHOUT ANY
 * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
 * A PARTICULAR PURPOSE. See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with BS4GC.
 * If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef PARTICLE_H
#define PARTICLE_H

#include <stdio.h>
#include <math.h>
#include <stdint.h>
#include <omp.h>

#include <vector>
#include <tuple>

#include "TypeV.h"

/**
 * @struct Particle
 * @brief Struktura reprezentující částici v simulaci.
 *
 * Tato struktura obsahuje informace o pozici, rychlosti a hmotnosti částice.
 * Je určená pro jednoduchou práci s částicemi v simulaci.
 */
struct Particle
{
    /// Konstruktor. Inicializuje na 0.
    Particle();
    /// Konstruktor.
    Particle(FloatV pos, FloatV speed = FloatV(), float mass = 1.0e-3);
    /// Destruktor.
    ~Particle();

    /// Kopírovací konstruktor.
    Particle(const Particle& other);
    /// Přesouvací konstruktor. Výchozí implementace.
    Particle(Particle&& other) noexcept = default;

    /// Opeátor přiřazení.
    Particle& operator=(Particle& other);
    /// Operátor přiřazení pomocí přesunutí. Není povolen.
    Particle& operator=(Particle&& other) noexcept = delete;

    void print();

    /// Pozice částice v prostoru.
    FloatV mPos;
    /// Pomocná proměnná pozice pro výpočet nové polohy.
    FloatV mPosNew;
    /// Pomocná proměnná se stavem ze začátku kroku simulace pro korekci rychlosti na konci kroku.
    FloatV mPosStart;

    /// Rychlost částice.
    FloatV mVelocity;
};

#endif // PARTICLE_H
