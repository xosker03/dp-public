# Benchmark Suite for Graphics Cards

## Introduction

This repository contains a master's thesis focusing on development of benchmark suite for Nvidia and AMD GPUs that could be used for teaching purposes and GPU evaluation.

## Repository structure

    .
    +--Data       - Example data, Measured results, etc.
    +--Literature - Publications, references, manuals, etc.
    +--ForUsers   - Examples, templates, example data and results, etc.
    +--Libs       - External libs / sources.
    +--Sources    - Root folder for the sources.
    +--Thesis     - Latex / MS Word sources of the thesis.
    +--Misc       - Other auxiliary materials.
    Makefile      - Makefile
    Readme.md     - Read me file


## Build instruction

    Potřebné moduly na stroji IT4I:Barbora (Verze se mohou lišit, není striktně dáno která verze je třeba)
        HDF5/1.14.0-iimpi-2022b
        CUDA/12.2.0
        CMake/3.24.3-GCCcore-12.2.0
        X11/20221110-GCCcore-12.2.0
        Mesa/22.2.4-GCCcore-12.2.0

    Alternativní možnost potřebných knihoven
        HDF5            - hdf5 / hdf5-openmpi (Arch)
        CUDA            - cuda (Arch)
        OpenMP          - openmp (Arch)
        GNU C Library   - glibc (Arch)
        GCC             - gcc (Arch)
        Make            - make (Arch)
        Cmake           - cmake (Arch)
        OpenGL          - mesa (Arch)

    Kompilace:
        $ make

    Kompilace s ladícími symboly
        $ make debug

    Doxygen:
        $ make doxygen


## Usage instruction

    Spuštění:
        $ ./prog
        
    Alternativní spuštění:
        $ make run

## Other instruction

    Pokud v používáte Wayland místo X11 a X11 v distribuci nemáte, tak bude nejspíše třeba v Makefile vyměnit řetězec "-D GLFW_BUILD_WAYLAND=0" za "-D GLFW_BUILD_X11=0" (více viz https://www.glfw.org/docs/latest/compile_guide.html#compile_deps)

    Volitelné knihovny:
        Artistic Style - astyle (Arch)
        Git            - git (Arch)

    Dalsí možnosti Makefile, které by se mohly hodit:
        $ make clean   - ostraní všechny soubory vygenerované kompilaci, nebo generováním doxygen
        $ make format - pomocí programu astyle reformátuje kód do podoby blízké SC@FIT Coding Style, ale s odsazením 4 mezer.
        $ make format2 - pomocí programu astyle reformátuje kód do podoby blízké SC@FIT Coding Style.

    Dalsí možnosti Makefile, které nejspíš nebudou potřeba:
        $ make fullclean - stejné jako clean, ale odstraní i externí knihovny

## Author information

 * Name: Josef Oškera
 * Email: josef.oskera@seznam.cz
 * Data: 2023/2024

